#include "Animation.h"

std::shared_ptr<Animation> Animation::CreateAnimation(unsigned int num_of_frames, unsigned int resource_id, glm::vec2 texture_size, glm::vec2 starting_parametrized_coords, bool traverse_by_x,  glm::vec2 sprite_size, float display_time) {
	std::shared_ptr<Animation> animation = std::make_shared<Animation>(Direction::Right);
	for (unsigned int i = 0; i < num_of_frames; i++) {
		animation->AddFrame(resource_id, texture_size, starting_parametrized_coords, sprite_size, display_time);
		traverse_by_x ? ++starting_parametrized_coords.x: ++starting_parametrized_coords.y;
	}
	return animation;
}
Animation::Animation(Direction dir): m_currentFrameIndex(0), m_frameTime(0), m_dir(dir), m_looped(false) {};

void Animation::AddFrame(unsigned int resource_id,glm::vec2 tex_size, glm::vec2 coords, glm::vec2 size, float frametime) {
	std::shared_ptr<Frame> frame = std::make_shared<Frame>(resource_id,tex_size, coords, size, frametime);
	m_frames.push_back(frame);
};

const std::shared_ptr<Frame> Animation::GetCurrentFrame() const{
#ifdef DEBUG 
	if(m_frames.size() > 0)
#endif
		return m_frames[m_currentFrameIndex];
#ifdef DEBUG 
	return nullptr;
#endif
}

bool Animation::UpdateFrame(float dt) {
	if(m_frames.size() > 0){ 
		m_frameTime += dt;
		if (m_frameTime >= m_frames[m_currentFrameIndex]->displayTime) {
			m_frameTime = 0.0f;
			RunActionForCurrentFrame(dt);
			IncrementFrame();
			return true;
		}
	}
		return false;
};

unsigned int Animation::GetCurrentFrameIndex()const {
	return m_currentFrameIndex;
}

void Animation::AddFrameAction(unsigned int frame, Action action) {
	if (frame < m_frames.size()) {
		auto actionkey = m_actions.find(frame);

		if (actionkey == m_actions.end()) {
			m_mask.Set_Bit(frame);
			m_actions.insert(std::make_pair(frame, std::vector<Action>{action}));
		}
		else
			actionkey->second.push_back(action);			
	}
}

void Animation::SetDirection(Direction dir) {
	if (m_dir != dir) {
		m_dir = dir;
		for (auto& f : m_frames)
			f->Change_Direction();
	}
}

void Animation::RunActionForCurrentFrame(float dt){
	if (m_actions.size() > 0){
		if(m_mask.Get_Bit(m_currentFrameIndex)){
			auto actionkey = m_actions.find(m_currentFrameIndex);
			if (actionkey != m_actions.end())
				for (auto a : actionkey->second)
					a(dt);
		}
	}
}

void Animation::ResetAnimation() {
	m_currentFrameIndex = 0;
	m_frameTime = 0.0f;
};

void Animation::IncrementFrame() {
	if (m_currentFrameIndex == m_frames.size() - 1 && !m_looped)
		return;
	m_currentFrameIndex = (m_currentFrameIndex + 1) % m_frames.size();
}