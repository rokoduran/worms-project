#pragma once
#include "../engine/graphics/drawables/sprite/Texture.h"
#include "util/defines.h"
#include <functional>
#include <map>
#include "math/glm/glm.hpp"
#include <vector>



struct Frame {
	//Resource id of the texture
	unsigned int resourceId;
	//info for tex coords
	glm::vec2 min,max,spriteSize;
	glm::vec2 texCoordinates[4];
	//the starting direction
	bool direction;
	//how long will the frame be displayed
	float displayTime;

	Frame(unsigned int resource_id,const glm::vec2& texture_size,const glm::vec2& coords,const glm::vec2& sprite_size, float displaytime) : resourceId(resource_id),spriteSize(sprite_size), displayTime(displaytime), direction(true) {
		this->min = { (coords.x*sprite_size.x )/ texture_size.x, (coords.y*sprite_size.y) / texture_size.y };
		this->max = { ((coords.x+1) * sprite_size.x) / texture_size.x, ((coords.y+1) * sprite_size.y) / texture_size.y };
		texCoordinates[0] = { min.x,min.y };
		texCoordinates[1] = { max.x,min.y };
		texCoordinates[2] = { max.x,max.y };
		texCoordinates[3] = { min.x,max.y };
	}
	void Change_Direction() {
		direction = !direction;
		if (direction) {
			texCoordinates[0] = { min.x,min.y };
			texCoordinates[1] = { max.x,min.y };
			texCoordinates[2] = { max.x,max.y };
			texCoordinates[3] = { min.x,max.y };
		}
		else {
			texCoordinates[1] = { min.x,min.y };
			texCoordinates[0] = { max.x,min.y };
			texCoordinates[3] = { max.x,max.y };
			texCoordinates[2] = { min.x,max.y };
		}
	}

};

enum class Direction
{
	None = 0,
	Left,
	Right
};

typedef std::function<void(float)> Action;

class Animation{
	private:
		Direction m_dir;
		std::vector<std::shared_ptr<Frame>> m_frames;
		std::map<unsigned int, std::vector<Action>> m_actions;
		//We use the mask to know if the frame has any actions to begin with
		Bitmask m_mask;
		bool m_looped;
		// Current frame.
		unsigned int m_currentFrameIndex;

		// We use this to decide when to transition to the next frame.
		float m_frameTime;
	public:
		Animation(Direction);
		static std::shared_ptr<Animation> CreateAnimation(unsigned int num_of_frames, unsigned int resource_id, glm::vec2 texture_size, glm::vec2 starting_parametrized_coords,
			bool traverse_by_x, glm::vec2 sprite_size, float display_time);
		void SetDirection(Direction);
		void AddFrame(unsigned int resource_id,glm::vec2 tex_size,glm::vec2 xycoords, glm::vec2 sprite_size, float frametime);
		void AddFrameAction(unsigned int frame, Action action);
		const std::shared_ptr<Frame> GetCurrentFrame() const;
		unsigned int GetCurrentFrameIndex()const;
		bool UpdateFrame(float deltaTime);
		void ResetAnimation();
		std::shared_ptr<Frame>& GetFrameAtIndex(int index) {
			return m_frames[index];
		}
		void SetLooped(bool looped) {
			m_looped = looped;
		}

		bool GetLooped()const {
			return m_looped;
		}
		Direction GetDirection()const { return m_dir; };
	private:
		void IncrementFrame();
		void RunActionForCurrentFrame(float dt);

};

