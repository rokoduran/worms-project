#include "AnimationComponent.h"
#include "../Worms/Objects/Entity.h"

AnimationComponent::AnimationComponent(Entity* entity) : Component(entity), m_currentAnimation(nullptr), m_sprite(nullptr){}

void AnimationComponent::Start() {
		m_sprite = m_master->GetComponent<SpriteComponent>();
}

void AnimationComponent::Update(float deltaTime){
		UpdateFrame(deltaTime);	
}

void AnimationComponent::UpdateFrame(float time) {
	bool newFrame = m_currentAnimation->UpdateFrame(time);

	if (newFrame) {
		std::shared_ptr<Frame> data = m_currentAnimation->GetCurrentFrame();

		m_sprite->Load(data->resourceId);

		m_sprite->SetFrameCoordinates(data->texCoordinates, data->spriteSize);
	}
}

void AnimationComponent::SetAnimation(std::shared_ptr<Animation> animation){
	m_currentAnimation = animation;
}

