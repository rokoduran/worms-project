#pragma once
#include "../SpriteComponent/SpriteComponent.h"
#include "../Worms/Animation/Animation.h"
#include "../DirectionComponent/DirectionComponent.h"


class AnimationComponent : public Component{
	private:
		std::shared_ptr<SpriteComponent> m_sprite; 
		std::shared_ptr<Animation> m_currentAnimation;
	public:
		AnimationComponent(Entity* owner);
		void Start()override;
		void Update(float deltaTime) override;
		void SetAnimation(std::shared_ptr<Animation> animation);
		void UpdateFrame(float dt);
		// Returns current animation state.
		COMPONENT_CLASS_TYPE(eAnimation);
};