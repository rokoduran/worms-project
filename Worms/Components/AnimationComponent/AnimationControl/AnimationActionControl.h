#pragma once
#include "../WormAnimationComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Worms/Systems/InputSystem/InputSystem.h"
#include "../engine/graphics/window/Input.h"

//Function namespace for functions used for actions for animation frames

namespace AnimationControl {
static	float movespeed = 150.0f;

static void Move(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			auto direction = worm->GetComponent<DirectionComponent>();
			std::string path = "assets/sounds/walk.wav";
			worm->sharedData.soundManager.Get(worm->sharedData.soundManager.Add(path))->Play();
			direction->GetDirection() == Direction::Left ? worm->transform->Move({ -3 * movespeed*dt, 0.0f }) : worm->transform->Move({ 3 * movespeed*dt,0.0f });
		}

static	void End(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			velocity->SetGrounded(true);
		}

static	void Jump(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			std::string path = "assets/sounds/jump.wav";
			worm->sharedData.soundManager.Get(worm->sharedData.soundManager.Add(path))->Play();
			velocity->SetGrounded(false);
			auto direction = worm->GetComponent<DirectionComponent>();
			direction->GetDirection() == Direction::Left ? velocity->SetVelocity({ -movespeed, 2 * movespeed }) : velocity->SetVelocity({ movespeed,2 * movespeed });
		}

static	void Backflip(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			std::string path = "assets/sounds/backflip.wav";
			worm->sharedData.soundManager.Get(worm->sharedData.soundManager.Add(path))->Play();
			velocity->SetGrounded(false);
			auto direction = worm->GetComponent<DirectionComponent>();
			direction->GetDirection() == Direction::Left ? velocity->SetVelocity({ movespeed / 3, 2 * movespeed }) : velocity->SetVelocity({ -movespeed / 3, 2 * movespeed });
		}

static	void Fall(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto animation = worm->GetComponent<WormAnimationComponent>();
			Falling type;
			animation->SetExplicitAnimationType(type);
		}

static	void BackflipFall(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto animation = worm->GetComponent<WormAnimationComponent>();
			BackflipFalling type;
			animation->SetExplicitAnimationType(type);
		}

static	void Land(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			velocity->SetGrounded(true);
		}

static	void Up(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto sprite = worm->GetComponent<WormSpriteComponent>();
			float angle = sprite->GetCrosshairAngle()*180.0f / M_PI;
			if (angle > 90.0f) {
				if (angle - 1.0f > 90.0f)
					sprite->SetCrosshairAngle(angle - 1.0f);
				return;
			}
			if (angle + 1.0f <= 90.0f)
				sprite->SetCrosshairAngle(angle + 1.0f);
		};
static	void Down(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto sprite = worm->GetComponent<WormSpriteComponent>();
			float angle = sprite->GetCrosshairAngle()*180.0f / M_PI;
			if (angle > 90.0f) {
				if (angle + 1.0f < 270.0F)
					sprite->SetCrosshairAngle(angle + 1.0f);
				return;
			}
			if (angle - 1.0f > -90.0f)
				sprite->SetCrosshairAngle(angle - 1.0f);
		};
};