#pragma once
#include "../Worms/Objects/Entity.h"
#include "../Worms/Systems/InputSystem/InputSystem.h"
#include "../Worms/Components/AnimationComponent/WormAnimationComponent.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../Worms/Components/InventoryComponent/InventoryComponent.h"
#include "../Worms/Structures/Weapon/Grenade/Grenade.h"

//Namespace with functions that are called in inputsystem for worm animations

namespace AnimationInputControl {

static	void Backflip(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			bool grounded = velocity->GetGrounded();
			if (grounded) {
				auto animation = worm->GetComponent<WormAnimationComponent>();
				Backflipping flip;
				animation->SetExplicitAnimationType(flip);
			}
		}

static	void Go_Left(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			bool grounded = velocity->GetGrounded();
			if (grounded) {
				auto direction = worm->GetComponent<DirectionComponent>();
				direction->SetDirection(Direction::Left);
				auto animation = worm->GetComponent<WormAnimationComponent>();
				if (animation->GetCurrentCategory() != AnimationCategory::WalkingAnimation) {
					animation->SetAnimationCategory(WalkingAnimation);
					velocity->SetGrounded(false);
				}
			}
		};
static	void Go_Right(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			bool grounded = velocity->GetGrounded();
			if (grounded) {
				auto direction = worm->GetComponent<DirectionComponent>();
				direction->SetDirection(Direction::Right);
				auto animation = worm->GetComponent<WormAnimationComponent>();
				if (animation->GetCurrentCategory() != AnimationCategory::WalkingAnimation) {
					animation->SetAnimationCategory(WalkingAnimation);
					velocity->SetGrounded(false);
				}
			}
		};

static	void Jump(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto velocity = worm->GetComponent<VelocityComponent>();
			bool grounded = velocity->GetGrounded();
			if (grounded) {
				auto animation = worm->GetComponent<WormAnimationComponent>();
				Jumping jumptype;
				animation->SetExplicitAnimationType(jumptype);
			}
		}

static	void Equip_Bazooka(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto inventory = worm->GetComponent<InventoryComponent>();
			inventory->EquipWeapon(eBazooka);
	};

static	void Set_Grenade_Timer1(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			std::shared_ptr<Grenade> wep = std::static_pointer_cast<Grenade>(worm->GetComponent<InventoryComponent>()->GetCurrentWeapon());
			std::string announcement = "Grenade fuse: " + NumberToString(1);
			worm->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
			wep->Set_Fuse(1);
		};
static	void Set_Grenade_Timer2(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			std::shared_ptr<Grenade> wep = std::static_pointer_cast<Grenade>(worm->GetComponent<InventoryComponent>()->GetCurrentWeapon());
			std::string announcement = "Grenade fuse: " + NumberToString(2);
			worm->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
			wep->Set_Fuse(2);
		};
static	void Set_Grenade_Timer3(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			std::shared_ptr<Grenade> wep = std::static_pointer_cast<Grenade>(worm->GetComponent<InventoryComponent>()->GetCurrentWeapon());
			std::string announcement = "Grenade fuse: " + NumberToString(3);
			worm->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
			wep->Set_Fuse(3);
		};
static	void Set_Grenade_Timer4(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			std::shared_ptr<Grenade> wep = std::static_pointer_cast<Grenade>(worm->GetComponent<InventoryComponent>()->GetCurrentWeapon());
			std::string announcement = "Grenade fuse: " + NumberToString(4);
			worm->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
			wep->Set_Fuse(4);
		};
static	void Set_Grenade_Timer5(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			std::shared_ptr<Grenade> wep = std::static_pointer_cast<Grenade>(worm->GetComponent<InventoryComponent>()->GetCurrentWeapon());
			std::string announcement = "Grenade fuse: " + NumberToString(5);
			worm->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
			wep->Set_Fuse(5);
		};

static	void Equip_Grenade(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto inventory = worm->GetComponent<InventoryComponent>();
			inventory->EquipWeapon(eGrenade);
			InputSystem& sys = worm->sharedData.turnSystem.GetInputSys();
			sys.AttachCallback(KEYBOARD_1, AnimationInputControl::Set_Grenade_Timer1);
			sys.AttachCallback(KEYBOARD_2, AnimationInputControl::Set_Grenade_Timer2);
			sys.AttachCallback(KEYBOARD_3, AnimationInputControl::Set_Grenade_Timer3);
			sys.AttachCallback(KEYBOARD_4, AnimationInputControl::Set_Grenade_Timer4);
	    	sys.AttachCallback(KEYBOARD_5, AnimationInputControl::Set_Grenade_Timer5);
	};
static	void Equip_Mortar(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto inventory = worm->GetComponent<InventoryComponent>();
			inventory->EquipWeapon(eMortar);
		};
static	void Equip_Dynamite(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto inventory = worm->GetComponent<InventoryComponent>();
			inventory->EquipWeapon(eDynamite);
		};
static	void Equip_SkipGo(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto inventory = worm->GetComponent<InventoryComponent>();
			inventory->EquipWeapon(eSkipGo);
		};

static	void Fire(float dt) {
			std::shared_ptr<Entity>& worm = InputSystem::GetActiveWorm();
			auto inventory = worm->GetComponent<InventoryComponent>();
			inventory->Fire();
		}
}