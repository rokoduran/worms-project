#include "WormAnimationComponent.h"
#include "../Worms/Objects/Entity.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

std::vector<AnimationCategory> WormAnimationComponent::categories ={NoneCategory, IdleAnimation, WalkingAnimation, JumpingAnimation, FallingAnimation};

WormAnimationComponent::WormAnimationComponent(Entity* entity) : Component(entity), m_currentAnimation(AnimationType::AnimeNone, nullptr), m_sprite(nullptr), m_dir(nullptr), m_vel(nullptr), m_dmg(nullptr), m_heavilyDamaged(false),
 m_idleSwitcher(true), m_switchTimer(0.0f), m_currentCategory(NoneCategory), m_revertTimer(2.0f){
	m_switchTimer = glm::linearRand(10.0f, 25.0f);
}

void WormAnimationComponent::Start() {
	m_inventory = m_master->GetComponent<InventoryComponent>();
	m_sprite = m_master->GetComponent<WormSpriteComponent>();
	m_dir = m_master->GetComponent<DirectionComponent>();
	m_dmg = m_master->GetComponent<DamageComponent>();
	m_vel = m_master->GetComponent<VelocityComponent>();
	if (m_dmg->GetHP() < 26) {
		m_heavilyDamaged = true;
		IdleDamaged dmgd;
		SetDefaultForCategory(AnimationCategory::IdleAnimation, dmgd);
	}
	SetAnimationCategory(AnimationCategory::IdleAnimation);
}

void WormAnimationComponent::ProcessEvent(Event e) {
	switch (e) {

	case(Event::Victory): {
		IdleCelebrating celebrate;
		SetExplicitAnimationType(celebrate);
		break;
	}
		case(Event::VelocityGrounded): {
			if (m_vel->GetGrounded() && m_currentCategory != JumpingAnimation && m_currentCategory > GetCurrentCategory() != IdleAnimation) {
				SetAnimationCategory(IdleAnimation);
			}
			break;
		}
		case(Event::WormHPChanged): {
			int hp = m_dmg->GetHP();
			if (m_heavilyDamaged) {
				if (hp > 25) {
					IdleDefault defaultidle;
					m_heavilyDamaged = false;
					SetDefaultForCategory(IdleAnimation, defaultidle);
				}
			}
			else {
				if (hp < 26) {
					IdleDamaged idledamaged;
					m_heavilyDamaged = true;
					SetDefaultForCategory(IdleAnimation, idledamaged);
				}
			}
		}
		case(Event::DirectionChanged): {
			Direction dir = m_dir->GetDirection();
			for (auto& i : m_animations)
				for (auto& j : i.second)
					j.second->SetDirection(dir);
			break;
		}
		case(Event::NewTurn): {
			m_idleSwitcher = false;
			std::string path = "assets/sounds/new_turn.wav";
			m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path))->Play();
			break; }
		case(Event::TurnOver): {
			if (m_dmg->GetHP() < 26) {
				m_heavilyDamaged = true;
				IdleDamaged dmgd;
				SetDefaultForCategory(AnimationCategory::IdleAnimation, dmgd);
			}
			else {
				IdleDefault default_idle;
				SetDefaultForCategory(AnimationCategory::IdleAnimation, default_idle);
			}
			if (m_vel->GetGrounded())
				SetAnimationCategory(IdleAnimation);
			m_idleSwitcher = true;
			m_switchTimer = 23.0f;
			m_revertTimer = 2.0f;
			break; 
		}

		case(Event::WeaponEquipped): {
			switch (m_inventory->GetCurrentWeaponType()) {
				case(WeaponTypes::eBazooka): {
					AimingBazooka bazooka;
					SetDefaultForCategory(IdleAnimation, bazooka);
					break;
				}
				case(WeaponTypes::eMortar): {
					AimingMortar mortar;
					SetDefaultForCategory(IdleAnimation, mortar);
					break;
				}
				case(WeaponTypes::eGrenade): {
					AimingGrenade grenade;
					SetDefaultForCategory(IdleAnimation, grenade);
					break;
				}
				case(WeaponTypes::eDynamite): {
					AimingDynamite dynamite;
					SetDefaultForCategory(IdleAnimation, dynamite);
					break;
				}
				case(WeaponTypes::eSkipGo): {
					AimingSkipGo skipgo;
					SetDefaultForCategory(IdleAnimation, skipgo);
					break;
				}
			}
			if (m_currentCategory == IdleAnimation)
				SetAnimationCategory(IdleAnimation);
		}
	}
}

void WormAnimationComponent::Update(float deltaTime) {
	if (!m_heavilyDamaged && m_idleSwitcher) {
		if (m_switchTimer <= 0.0f) {
			m_revertTimer -= deltaTime;
			if (m_revertTimer <= 0.0f) {
				m_revertTimer = 2.0f;
				m_switchTimer = glm::linearRand(14.0f, 25.0f);
				SetAnimationCategory(IdleAnimation);
				return;
			}
		}
		else {
			m_switchTimer -= deltaTime;
			if (m_switchTimer <= 0.0f) {
				AnimationType random_idle = (AnimationType)glm::linearRand((int)eIdle1, (int)eIdle7);
				//The switch is ugly to see, but it's necessary.
				switch (random_idle) {
					case(eIdle1): {
						Idle1 anime;
						SetExplicitAnimationType(anime);
						break;
					}
					case(eIdle2): {
						Idle2 anime;
						SetExplicitAnimationType(anime);
						break;
					}
					case(eIdle3): {
						Idle3 anime;
						SetExplicitAnimationType(anime);
						break;
					}
					case(eIdle4): {
						Idle4 anime;
						SetExplicitAnimationType(anime);
						break;
					}
					case(eIdle5): {
						Idle5 anime;
						SetExplicitAnimationType(anime);
						break;
					}
					case(eIdle6): {
						Idle6 anime;
						SetExplicitAnimationType(anime);
						break;
					}
					case(eIdle7): {
						Idle7 anime;
						SetExplicitAnimationType(anime);
						break;
					}
				}
				return;
			}
		}
	}
		UpdateFrame(deltaTime);
}

void WormAnimationComponent::AddAnimation(AnimeType& type, std::shared_ptr<Animation> animation) {
	for(auto& i : categories)
		if (type.Is_In_Category(i)) {
			m_animations[i][type.Get_Animation_Type()] = animation;
			break;
		}
}

void WormAnimationComponent::SetAnimationCategory(AnimationCategory category) {
	m_currentAnimation = std::pair<AnimationType, std::shared_ptr<Animation>>(m_categoryDefaultAnimations[category], 
		m_animations[category][m_categoryDefaultAnimations[category]]);
	m_currentAnimation.second->ResetAnimation();
	if (category != m_currentCategory){
		m_currentCategory = category;
	}
};

void WormAnimationComponent::SetExplicitAnimationType(AnimeType& type) {
	if (m_currentAnimation.first == type.Get_Animation_Type())
		return;

	for (auto& i : categories)
		if (type.Is_In_Category(i)) {
			m_currentAnimation.first = type.Get_Animation_Type();
			m_currentAnimation.second = m_animations[i][type.Get_Animation_Type()];
			m_currentAnimation.second->ResetAnimation();
			if (m_currentCategory != i) {
				m_currentCategory = i;
			}
			break;
		}

}
void WormAnimationComponent::SetDefaultForCategory(AnimationCategory category, AnimeType& type) {
	m_categoryDefaultAnimations[category] = type.Get_Animation_Type();
};


void WormAnimationComponent::UpdateFrame(float dt){
	bool newFrame = m_currentAnimation.second->UpdateFrame(dt);

	if (newFrame) {
		std::shared_ptr<Frame> data = m_currentAnimation.second->GetCurrentFrame();

		m_sprite->Load(data->resourceId);

		m_sprite->SetFrameCoordinates(data->texCoordinates, data->spriteSize);
	}
};
