#pragma once
#pragma once
#include "../SpriteComponent/WormSpriteComponent.h"
#include "../Worms/Animation/Animation.h"
#include "../DirectionComponent/DirectionComponent.h"
#include "../DamageComponent/DamageComponent.h"
#include "../InventoryComponent/InventoryComponent.h"
#include "WormAnimationTypes.h"

typedef std::unordered_map <AnimationType, std::shared_ptr<Animation>> AnimationTypeMap;

class WormAnimationComponent : public Component {
	private:
		float m_switchTimer, m_revertTimer;
		bool m_heavilyDamaged, m_idleSwitcher;
		std::shared_ptr<InventoryComponent> m_inventory;
		std::shared_ptr<WormSpriteComponent> m_sprite;
		std::shared_ptr<DirectionComponent> m_dir;
		std::shared_ptr<DamageComponent> m_dmg;
		std::shared_ptr<VelocityComponent> m_vel;
		//Map of animation categories and animation types which are assigned to those categories, along with animations
		std::unordered_map<AnimationCategory,AnimationTypeMap> m_animations;
		std::unordered_map<AnimationCategory, AnimationType> m_categoryDefaultAnimations;
		std::pair<AnimationType,std::shared_ptr<Animation>> m_currentAnimation;
		AnimationCategory m_currentCategory;
		//Vector of all categories, used for assigning new animation types
		static std::vector<AnimationCategory> categories;
	public:
		WormAnimationComponent(Entity* owner);

		void Start()override;

		void Update(float deltaTime) override;

		//Add animation, along with the type it will be assigned to
		void AddAnimation(AnimeType& type,
			std::shared_ptr<Animation> animation);
		// Set current animation category.
		void SetAnimationCategory(AnimationCategory);
		void SetExplicitAnimationType(AnimeType&);
		void SetDefaultForCategory(AnimationCategory, AnimeType&);
		void UpdateFrame(float dt);
		// Returns current animation state.
		unsigned int GetCurrentAnimationFrameIndex() {
			return m_currentAnimation.second->GetCurrentFrameIndex();
		};
		const AnimationType GetAnimationType() const {
			return  m_currentAnimation.first;
		};
		const AnimationCategory GetCurrentCategory()const {
			return m_currentCategory;
		}
		void ProcessEvent(Event)override;
		COMPONENT_CLASS_TYPE(eWormAnimation);
};
