#pragma once
#include "util/defines.h"

enum AnimationType{
	AnimeNone,
	eIdleDefault, eIdle1, eIdle2, eIdle3, eIdle4, eIdle5, eIdle6, eIdle7, eIdleDamaged,
	eAimBazooka, eAimGrenade, eAimMortar, eAimDynamite, eAimSkipGo,
	eCelebrating,
	eWalking,
	eJump, eBackflip,
	eFalling, eBackflipFalling, eLanding
};

enum AnimationCategory {
	NoneCategory = 0,
	IdleAnimation = ShiftBit(0),
	WalkingAnimation = ShiftBit(1),
	JumpingAnimation = ShiftBit(2),
	FallingAnimation = ShiftBit(3)
};



#define ANIMATION_CLASS_TYPE(type) static AnimationType Get_Static_Type() { return AnimationType::type; }\
								 AnimationType Get_Animation_Type() const override { return Get_Static_Type(); }\

#define ANIMATION_CLASS_CATEGORY(category) virtual int Get_Category_Flags() const override { return category; }

struct AnimeType{
	virtual AnimationType Get_Animation_Type() const { return AnimationType::AnimeNone; };
	virtual int Get_Category_Flags() const { return 0; };

		bool Is_In_Category(AnimationCategory category){
			return Get_Category_Flags() & category;
		}
	};

struct IdleType : public AnimeType {
	ANIMATION_CLASS_CATEGORY(IdleAnimation);
};

struct WalkType : public AnimeType {
	ANIMATION_CLASS_CATEGORY(WalkingAnimation);
};

struct JumpingType : public AnimeType {
	ANIMATION_CLASS_CATEGORY(JumpingAnimation)
};

struct FallingType : public AnimeType {
	ANIMATION_CLASS_CATEGORY(FallingAnimation)
};

struct IdleDefault : public IdleType {
	ANIMATION_CLASS_TYPE(eIdleDefault)
};

struct Idle1 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle1)
};
struct Idle2 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle2)
};
struct Idle3 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle3)
};
struct Idle4 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle4)
};
struct Idle5 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle5)
};
struct Idle6 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle6)
};
struct Idle7 : public IdleType {
	ANIMATION_CLASS_TYPE(eIdle7)
};

struct IdleCelebrating : public IdleType {
	ANIMATION_CLASS_TYPE(eCelebrating)
};


struct IdleDamaged : public IdleType {
	ANIMATION_CLASS_TYPE(eIdleDamaged)
};
struct AimingBazooka : public IdleType {
	ANIMATION_CLASS_TYPE(eAimBazooka)
};
struct AimingGrenade : public IdleType {
	ANIMATION_CLASS_TYPE(eAimGrenade)
};
struct AimingMortar : public IdleType {
	ANIMATION_CLASS_TYPE(eAimMortar)
};
struct AimingDynamite : public IdleType {
	ANIMATION_CLASS_TYPE(eAimDynamite)
};
struct AimingSkipGo : public IdleType {
	ANIMATION_CLASS_TYPE(eAimSkipGo)
};
struct Walking : public WalkType {
	ANIMATION_CLASS_TYPE(eWalking)
};
struct Jumping : public JumpingType {
	ANIMATION_CLASS_TYPE(eJump)
};
struct Backflipping : public JumpingType {
	ANIMATION_CLASS_TYPE(eBackflip)
};
struct Falling : public FallingType {
	ANIMATION_CLASS_TYPE(eFalling)
};
struct BackflipFalling : public FallingType {
	ANIMATION_CLASS_TYPE(eBackflipFalling)
};
struct Landing : public FallingType {
	ANIMATION_CLASS_TYPE(eLanding)
};
