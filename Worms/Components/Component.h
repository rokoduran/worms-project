#pragma once
#include "graphics/window/Window.h"
#include "../Structures/GameEvent/GameEvent.h"

class Entity;
//Components are bound to entities. Entities have vectors of components.
//Components are created by invoking their creation on the entity.

enum class ComponentType {
	eBase = 0,
	eAnimation,
	eWormAnimation,
	eDirection,
	ePhysics,
	eDraw,
	eTransform,
	eVelocity,
	eInventory,
	eFloat,
	eDamage,
	eVelocityController,
	eTimer
};
//Macro which ensures we have only one type of each component in an entities' vector of components.
#define COMPONENT_CLASS_TYPE(type) static ComponentType GetStaticType() { return ComponentType::type; }\
								ComponentType GetType() const override { return GetStaticType();}

class Component{
	protected:
		Entity* m_master;
	public:
		Component(Entity* master) : m_master(master) {}
		virtual ~Component() {};
		virtual void Start() {};
		virtual void ProcessEvent(Event) {};
		virtual void Update(float dt) {};
		virtual void LateUpdate(float dt) {};
		virtual ComponentType GetType()const { return ComponentType::eBase; }
		Entity* GetMaster() { return m_master; }
};
