#include "DamageComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"

void DamageComponent::Start() {
	m_sprite = m_master->GetComponent<WormSpriteComponent>();
	m_sprite->SetHP(m_hp);
}

void DamageComponent::Damage(int dmg) {
	m_hp - dmg <= 0 ? m_hp = 0 : m_hp -= dmg;
	m_sprite->SetHP(m_hp);
	if (m_hp == 0)
		m_master->sharedData.turnSystem.WormDied(m_master->GetID());
	m_master->RaiseEvent(Event::WormHPChanged);
}

void DamageComponent::Heal(int amount) {
	if(m_hp!=0)
		m_hp += amount;
	m_sprite->SetHP(m_hp);
	m_master->RaiseEvent(Event::WormHPChanged);
}

bool DamageComponent::IsDead() {
	return m_hp == 0 ? true : false;
}