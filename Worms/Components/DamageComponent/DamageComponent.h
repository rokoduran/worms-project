#pragma once
#include "../Component.h"
#include "../SpriteComponent/WormSpriteComponent.h"

class DamageComponent : public Component {
	private:
		int m_hp;
		std::shared_ptr<WormSpriteComponent> m_sprite;
	public:
		DamageComponent(Entity* master) : Component(master), m_hp(100) {};
		void Start()override;
		void Damage(int);
		void Heal(int);
		bool IsDead();
		int GetHP() {
			return m_hp;
		}
		COMPONENT_CLASS_TYPE(eDamage);
};