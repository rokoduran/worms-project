#include "DirectionComponent.h"
#include "../Worms/Objects/Entity.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

void DirectionComponent::Start() {
	SetDirection((Direction)glm::linearRand((int)Direction::Left, (int)Direction::Right));
}

Direction DirectionComponent::GetDirection() {
	return m_dir;
}

void DirectionComponent::SetDirection(Direction dir) {
	if (dir != m_dir){
		m_master->RaiseEvent(Event::DirectionChanged);
		m_dir = dir;
	}
}