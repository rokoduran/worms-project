#pragma once
#include "../Component.h"
#include "../Worms/Animation/Animation.h"
#include "../VelocityComponent/VelocityComponent.h"

class DirectionComponent : public Component {
	private:
		Direction m_dir;
	public:
		DirectionComponent(Entity* master) : Component(master), m_dir(Direction::Right) {
		};
		void Start()override;
		void SetDirection(Direction);
		Direction GetDirection();
		COMPONENT_CLASS_TYPE(eDirection);
};
