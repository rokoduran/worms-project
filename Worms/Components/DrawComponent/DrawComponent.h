#pragma once
#include "../Component.h"

enum class DrawLayer {
	None = 0,
	Sky,
	Floating,
	Background,
	TerrainCollision,
	Entity,
	Water
};

//Base class for draw component, which will be invoked for drawing to the window
class DrawComponent : public Component {
	private:
		DrawLayer m_layer;
		int m_draworderID;
	public:
		DrawComponent(Entity* master) : Component(master), m_draworderID(0), m_layer(DrawLayer::None) {};
		virtual bool ShouldStopDrawing()const = 0;
		virtual void Draw(Window&) = 0;
		DrawLayer GetDrawLayer()const {
			return m_layer;
		}
		void SetDrawLayer(DrawLayer layer) {
			m_layer = layer;
		}
		void SetOrder(int id) { m_draworderID = id; }
		int GetOrder()const { return m_draworderID; }
		COMPONENT_CLASS_TYPE(eDraw);
};