#pragma once
#include "math/glm/gtc/random.hpp"
#include "../VelocityComponent/VelocityComponent.h"
#include "../Worms/Objects/Entity.h"

//Float component is only used for the floating elements in the background, clouds, stars etc.
class FloatComponent : public Component {
	private:
		//Assumption that the game bounds will not change during the game itself
		std::shared_ptr<VelocityComponent> m_vel;
		Rect& m_gameBounds;
		float m_spawnLeftLocation, m_spawnRightLocation, m_spawnTopLocation;
		glm::vec2 m_xVelocityLimits, m_yVelocityLimits;
		bool m_rotating, m_randomHorizontalSpawn;
	public:
		FloatComponent(Entity* master) : Component(master), m_rotating(false), m_randomHorizontalSpawn(false), m_vel(nullptr), m_spawnLeftLocation(m_gameBounds.GetOrigin().x), m_spawnRightLocation(m_gameBounds.GetOrigin().x + m_gameBounds.GetSize().x - 1),
			m_gameBounds(master->sharedData.gameBounds), m_spawnTopLocation(0.0f), m_xVelocityLimits(0.0f,1.0f), m_yVelocityLimits(0.0f,1.0f) {}

		void Start()override {
			m_vel = m_master->GetComponent<VelocityComponent>();
			m_vel->SetVelocity({ glm::linearRand(m_xVelocityLimits.x,m_xVelocityLimits.y),glm::linearRand(m_yVelocityLimits.x,m_yVelocityLimits.y) });
			if(m_rotating)
				m_master->transform->SetRotation(glm::linearRand(-360.0f,360.0f));
		}

		void SetRandomHorizontalSpawn(bool random) {
			m_randomHorizontalSpawn = random;
		}
		void SetTopLocation(float t) {
			m_spawnTopLocation = t;
		}

		void SetVelocityLimits(const glm::vec2& x_limit, const glm::vec2& y_limit) {
			m_xVelocityLimits = x_limit;
			m_yVelocityLimits = y_limit;
		}

		void SetRotating(bool b) {
			m_rotating = b;
		}

		void Update(float dt)override {
			const glm::vec2& pos = m_master->transform->GetPosition();
			if(m_rotating)
				m_master->transform->Rotate(VelocityComponent::wind.x*10*dt);
			if (!m_gameBounds.Contains(pos)) {
				float x;
				if (m_randomHorizontalSpawn)
					x = glm::linearRand(m_spawnLeftLocation, m_spawnRightLocation);
				else 
				    x =	pos.x > (m_gameBounds.GetOrigin().x + m_gameBounds.GetSize().x) ? m_spawnLeftLocation : m_spawnRightLocation;
				
				m_master->transform->SetPosition(x, m_spawnTopLocation);
				m_vel->SetVelocity({ glm::linearRand(m_xVelocityLimits.x,m_xVelocityLimits.y),glm::linearRand(m_yVelocityLimits.x,m_yVelocityLimits.y) });
			}


		}
		COMPONENT_CLASS_TYPE(eFloat);
};