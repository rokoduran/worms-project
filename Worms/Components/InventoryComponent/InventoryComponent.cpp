#include "InventoryComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../AnimationComponent/AnimationControl/AnimationInputControl.h"

InventoryComponent::InventoryComponent(Entity* master) : Component(master) {};

void InventoryComponent::Start(){
#ifdef DEBUG
	if (m_inventory == nullptr)
		ASSERT(0);
#endif
	m_sprite = m_master->GetComponent<WormSpriteComponent>();
	m_currentlyEquipped = m_inventory->GetNextAvailableWeapon();
}
void InventoryComponent::ProcessEvent(Event e){
	switch (e) {
		case(Event::NewTurn) :{
			if (m_inventory->GetAmmo(m_currentlyEquipped) == 0) {
				m_currentlyEquipped = m_inventory->GetNextAvailableWeapon();
			}
			InputSystem& sys = m_master->sharedData.turnSystem.GetInputSys();
			if (m_currentlyEquipped == eGrenade) {
				sys.AttachCallback(KEYBOARD_1,AnimationInputControl::Set_Grenade_Timer1);
				sys.AttachCallback(KEYBOARD_2,AnimationInputControl::Set_Grenade_Timer2);
				sys.AttachCallback(KEYBOARD_3,AnimationInputControl::Set_Grenade_Timer3);
				sys.AttachCallback(KEYBOARD_4,AnimationInputControl::Set_Grenade_Timer4);
				sys.AttachCallback(KEYBOARD_5,AnimationInputControl::Set_Grenade_Timer5);
			}
			sys.AttachCallback(KEYBOARD_F1,AnimationInputControl::Equip_Bazooka);
			sys.AttachCallback(KEYBOARD_F2,AnimationInputControl::Equip_Grenade);
			sys.AttachCallback(KEYBOARD_F3,AnimationInputControl::Equip_Mortar);
			sys.AttachCallback(KEYBOARD_F4,AnimationInputControl::Equip_Dynamite);
			sys.AttachCallback(KEYBOARD_F5,AnimationInputControl::Equip_SkipGo);
			sys.AttachCallback(KEYBOARD_SPACE, AnimationInputControl::Fire);
			m_master->RaiseEvent(Event::WeaponEquipped);
			break;
		}
		case(Event::TurnOver): {
			ClearInputActions();
			break;
		}
	}
}
void InventoryComponent::SetInventory(std::shared_ptr<Inventory> inventory) {
	m_inventory = inventory;
}

void InventoryComponent::AddWeapon(std::shared_ptr<Weapon> weapon) {
	m_inventory->Add_Weapon(weapon);
}

void InventoryComponent::EquipWeapon(WeaponTypes wep) {
	int ammo = m_inventory->GetAmmo(wep);
	if (ammo!=0) {
		if (wep != eGrenade)
			ClearNumberKeys();
		m_currentlyEquipped = wep;
	}
	else 
		m_currentlyEquipped = m_inventory->GetNextAvailableWeapon();
	std::shared_ptr<Weapon> current_wep = GetCurrentWeapon();
	std::string current_ammo = current_wep->GetAmmo() == -1 ? "Infinite" : NumberToString(current_wep->GetAmmo());
	std::string announcement = current_wep->GetWeaponName() + " " + "Ammo: " + current_ammo;
	m_master->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
	m_master->RaiseEvent(Event::WeaponEquipped);
}
void InventoryComponent::Fire() {
	m_inventory->Fire(m_currentlyEquipped,m_sprite->GetCrosshairAngle(),16.0f, m_sprite->Get_Center(), m_master->sharedData);

}

void InventoryComponent::ClearNumberKeys() {
	InputSystem& sys = m_master->sharedData.turnSystem.GetInputSys();
	sys.RemoveKeyAction(KEYBOARD_1);
	sys.RemoveKeyAction(KEYBOARD_2);
	sys.RemoveKeyAction(KEYBOARD_3);
	sys.RemoveKeyAction(KEYBOARD_4);
	sys.RemoveKeyAction(KEYBOARD_5);
}

void InventoryComponent::ClearInputActions() {
	InputSystem& sys = m_master->sharedData.turnSystem.GetInputSys();
	sys.RemoveKeyAction(KEYBOARD_1);
	sys.RemoveKeyAction(KEYBOARD_2);
	sys.RemoveKeyAction(KEYBOARD_3);
	sys.RemoveKeyAction(KEYBOARD_4);
	sys.RemoveKeyAction(KEYBOARD_5);
	sys.RemoveKeyAction(KEYBOARD_1);
	sys.RemoveKeyAction(KEYBOARD_F1);
	sys.RemoveKeyAction(KEYBOARD_F2);
	sys.RemoveKeyAction(KEYBOARD_F3);
	sys.RemoveKeyAction(KEYBOARD_F4);
	sys.RemoveKeyAction(KEYBOARD_F5);
	sys.RemoveKeyAction(KEYBOARD_SPACE);
}