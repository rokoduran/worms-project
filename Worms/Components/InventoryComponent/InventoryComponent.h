#pragma once
#include "../Worms/Structures/Inventory/Inventory.h"
#include "../SpriteComponent/WormSpriteComponent.h"

class InventoryComponent : public Component {
	private:
		std::shared_ptr<WormSpriteComponent> m_sprite;
		WeaponTypes m_currentlyEquipped = WeaponTypes::eSkipGo;
		std::shared_ptr<Inventory> m_inventory = nullptr;
	public:
		InventoryComponent(Entity* master);
		void Start();
		void ProcessEvent(Event e);
		void SetInventory(std::shared_ptr<Inventory> inventory);
		void AddWeapon(std::shared_ptr<Weapon> weapon);
		void EquipWeapon(WeaponTypes wep);
		void Fire();

		WeaponTypes GetCurrentWeaponType() {
			return m_currentlyEquipped;
		}
		std::shared_ptr<Weapon> GetCurrentWeapon() {
			return m_inventory->GetWeapon(m_currentlyEquipped);
		}
		COMPONENT_CLASS_TYPE(eInventory);
	private:
		void ClearNumberKeys();
		void ClearInputActions();


};
