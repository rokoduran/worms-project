#include "CollisionMapComponent.h"
#include "../Worms/Objects/Entity.h"
const CollisionMap& CollisionMapComponent::GetMap(){
	return m_map;
}

void CollisionMapComponent::Damage(const Rect& area) {
	m_map.Damage(area);
	m_damaged = true;
}

bool CollisionMapComponent::ShouldStopDrawing()const {
	return m_master->ShouldBeRemoved();
}

void CollisionMapComponent::SetNoise(unsigned char* data, const std::string& file) {
	m_map.SetNoiseData(data,file);
}

void CollisionMapComponent::SetImage(const std::string& name) {
	m_map.SetImageData(name);
}

void CollisionMapComponent::Draw(Window& window) {
	if (m_damaged)
		m_map.UpdateTexture();
	m_map.Draw(window);
}