#pragma once
#include "../DrawComponent/DrawComponent.h"
#include "../Worms/MapGen/CollisionMap.h"
class CollisionMapComponent : public DrawComponent {
	private:
		CollisionMap m_map;
		bool m_damaged;
	public:
		CollisionMapComponent(Entity* master) : DrawComponent(master), m_damaged(false) {};
		const CollisionMap& GetMap();
		void SetNoise(unsigned char*, const std::string& file);
		void SetImage(const std::string&);
		void Damage(const Rect&);
		bool ShouldStopDrawing()const override;
		float MapTopLocation() {
			const Rect& area = m_map.GetArea();
			return area.GetOrigin().y + area.GetSize().y;
		}
		void Draw(Window&)override;

};