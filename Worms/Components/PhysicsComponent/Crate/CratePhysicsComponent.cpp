#include "CratePhysicsComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Destruction/DestructionPhysicsComponent.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

void CratePhysicsComponent::Start(){
	m_vel = m_master->GetComponent<VelocityComponent>();
}

void CratePhysicsComponent::Action() {
	std::shared_ptr<Entity> entity = std::make_shared<Entity>(m_master->sharedData);
	auto destro = entity->AddComponent<DestructionPhysicsComponent>();
	destro->SetDamage(40);
	const glm::vec2& size = m_rect.GetSize();
	const glm::vec2& origin = m_rect.GetOrigin();
	entity->transform->SetPosition( origin.x - 64.0f / 2, origin.y - 64.0f / 2 );
	destro->SetRect( {64.0f, 64.0f});
	m_master->sharedData.entities.Add(entity);
	std::string sound_path = "assets/sounds/Explosion" + NumberToString(glm::linearRand(1, 3)) + ".wav";
	std::shared_ptr<Sound> effect = m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(sound_path));
	effect->Play();
	m_master->SetForRemoval();
};
void CratePhysicsComponent::Move(const glm::vec2& resolution) {
	glm::vec2 move_amount;
	if (resolution.y >= 0 && resolution.y <= 4) {
		move_amount = { 0.0f,resolution.y };
		m_vel->SetGrounded(true);
	}
	else {
		move_amount = resolution;
		m_vel->SetVelocity(move_amount);
	}
	m_master->transform->Move(move_amount);
	SetPosition();

};

void CratePhysicsComponent::ResolveOverlap(std::shared_ptr<PhysicsComponent>& other) {
	switch (other->GetLayer()) {
		case(CollisionLayer::Destruction): {
			Action();
			break;
		}
		case(CollisionLayer::Water): {
			if (other->Get_Rect().Contains(m_rect.GetCenter())){
				std::string path = "assets/sounds/splash.wav";
				m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path))->Play();
				m_master->SetForRemoval();
			}
			break;
		}
	}
};

void CratePhysicsComponent::ResolveActive(){
	SetPosition();
	m_active = !m_vel->GetGrounded();
}