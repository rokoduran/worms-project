#pragma once
#include "../PhysicsComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
enum class CrateType {
	Weapon,
	Health
};

class CratePhysicsComponent : public PhysicsComponent {
	protected:
		std::shared_ptr<VelocityComponent> m_vel;
		CrateType m_type;
		bool m_usedUp;
	public:
		CratePhysicsComponent(Entity* master, CrateType type) : PhysicsComponent(master, CollisionLayer::Crate), m_type(type), m_usedUp(false) {};
		CrateType Get_Crate_Type() {
			return m_type;
		}
		void Start()override;
		void ResolveActive()override;
		void Action()override;
		void Move(const glm::vec2&)override;
		void ResolveOverlap(std::shared_ptr<PhysicsComponent>&)override;
		bool CrateUsed() {
			return m_usedUp;
		}
};