#include "HealthCratePhysicsComponent.h"


HealthCratePhysicsComponent::HealthCratePhysicsComponent(Entity* master):CratePhysicsComponent(master,CrateType::Health),m_hpAmount(25) {};
void HealthCratePhysicsComponent::SetHP(int hp) {
	m_hpAmount = hp;
};
int HealthCratePhysicsComponent::GetHP() {
	m_usedUp = true;
	return m_hpAmount;
};