#pragma once
#include "../CratePhysicsComponent.h"

class HealthCratePhysicsComponent : public CratePhysicsComponent {
	private:
		int m_hpAmount;
	public:
		HealthCratePhysicsComponent(Entity* master);
		void SetHP(int hp);
		int GetHP();
};