#include "WeaponCratePhysicsComponent.h"
#include "../Worms/Objects/Entity.h"

WeaponCratePhysicsComponent::WeaponCratePhysicsComponent(Entity* master) : CratePhysicsComponent(master, CrateType::Weapon), m_wep(nullptr) {};
void WeaponCratePhysicsComponent::SetWeapon(std::shared_ptr<Weapon> wep) {
	m_wep = wep;
};
std::shared_ptr<Weapon> WeaponCratePhysicsComponent::GetWeapon() {
	m_usedUp = true;
	return m_wep;
};
