#pragma once
#include "../CratePhysicsComponent.h"
#include "../Worms/Structures/Weapon/Weapon.h"

class WeaponCratePhysicsComponent : public CratePhysicsComponent {
	private:
		std::shared_ptr<Weapon> m_wep;
	public:
		WeaponCratePhysicsComponent(Entity* master);
		void SetWeapon(std::shared_ptr<Weapon>);
		std::shared_ptr<Weapon> GetWeapon();
};