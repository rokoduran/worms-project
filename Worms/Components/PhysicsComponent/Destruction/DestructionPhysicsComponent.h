#pragma once
#include "../PhysicsComponent.h"
#include "../Worms/Objects/Entity.h"

class DestructionPhysicsComponent : public PhysicsComponent {
	private:
		int m_damage;
	public:
		DestructionPhysicsComponent(Entity* master) : PhysicsComponent(master,CollisionLayer::Destruction), m_damage(0) {
			SetActivity(true);
		};
		~DestructionPhysicsComponent() {
		}
		void Start()override {
			SetPosition();
		}
		void SetDamage(int dmg) {
			m_damage = dmg;
		}
		void ResolveActive() override{
			m_master->SetForRemoval();
		}
		void ResolveOverlap(std::shared_ptr<PhysicsComponent>& other)override {
		};
		int GetDamage() {
			return m_damage;
		}
};