#include "MapPhysicsComponent.h"
#include "../Worm/WormPhysicsComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Destruction/DestructionPhysicsComponent.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"

void MapPhysicsComponent::Set_Map(std::shared_ptr<CollisionMapComponent> draw, unsigned int part_size) {
	m_map = draw;
	const CollisionMap& map = draw->GetMap();
	const Rect& area = map.GetArea();
	const glm::vec2& size = area.GetSize();
	m_rect.SetSize(size);
#ifdef DEBUG
	if ((int)(size.x*size.y) % (part_size*part_size)){
		std::cout << "Part sizes aren't compatible with map resolution!" << std::endl;
		ASSERT(0);
	}
#endif
	m_partSize = part_size;
		
	const unsigned char* data = map.Get_Data();
//Every part_size*part_size we create a new rectangle.As soon as one pixel is found, a rectangle can be made. If none of the part_size*part_size
//pixels have a value of 1, it's "air", and it's not counted as part of the terrain.
	bool solid_pixel_found = false;
	unsigned int x_counter = 0, y_counter = 0;

	for(unsigned int a = 0; a < part_size; a++){
		for(unsigned int b = 0; b < part_size; b++){
			if(a==0 && b == 0)
				if (data[3] != 0) {
					solid_pixel_found = true;
					break;
				}
		}
		if (solid_pixel_found) {
			m_parts.push_back(Rect(x_counter, y_counter, part_size, part_size));
			solid_pixel_found = false;
			break;
		}
	}
	x_counter += part_size;


	while(y_counter < size.y){
		for(unsigned int i = 0; i < part_size; i++){
			for (unsigned int j = 0; j < part_size; j++) {
				if (data[((y_counter+i)*(int)size.x + x_counter + j)*4 -1] != 0){
					solid_pixel_found = true;
					break;
				}
			}
			if (solid_pixel_found) {
				m_parts.push_back(Rect((float)x_counter, (float)y_counter, part_size, part_size));
				solid_pixel_found = false;
				break;
			}
		}
		x_counter += part_size;
		if (x_counter >= size.x){
			y_counter += part_size;
			x_counter = 0;
		}
	}

}

//The mapcomponents' intersects checks if the other physcomp is active, if it is, places it in the active vector
bool MapPhysicsComponent::Intersects(std::shared_ptr<PhysicsComponent>& other) {
	if(other->GetActivity()){
		m_activeComponents.push_back(other);
		return true;
	}
	return false;
};

void MapPhysicsComponent::ResolveActive() {
	if (m_activeComponents.empty())
		return;
	bool part_erased = false;
	auto it = m_parts.begin();
	while (it != m_parts.end()){
		auto& p = *it;
		//Now we go over the active components, if there are any left
		auto second_it = m_activeComponents.begin();
		while (second_it != m_activeComponents.end()) {
			auto& a = *second_it;
			if (p.Intersects(a->Get_Rect())) {
				switch (a->GetLayer()) {
					//Move the entity depending on the overlap between their rects
					case (CollisionLayer::Entity): {
						a->Move(a->Get_Rect().SolveOverlap(p));
						second_it = m_activeComponents.erase(second_it);
						break;
					}
					case(CollisionLayer::Crate): {
						a->Move(a->Get_Rect().SolveOverlap(p));
						second_it = m_activeComponents.erase(second_it);
						break;
					}
					//Trigger the projectile action
					case(CollisionLayer::Projectile): {
						a->Action();
						second_it = m_activeComponents.erase(second_it);
						break;
					}
					case(CollisionLayer::Thrown): {
						a->Move(a->Get_Rect().SolveOverlap(p));
						++second_it;
						break;
					}
					//Chip away the maps chunks for the given rectangle
					case (CollisionLayer::Destruction): {
						m_map->Damage(p);
						it = m_parts.erase(it);
						part_erased = true;
					}
					default: ++second_it;
				}
			}
			else
				++second_it;
		}
		if (m_activeComponents.empty())
			return;
		if (part_erased) {
			part_erased = false;
		}
		else
			++it;		
	}
	m_activeComponents.clear();
}

std::vector<glm::vec2> MapPhysicsComponent::Find_Placemenet_Points(glm::vec2 given_size) {
	std::vector<glm::vec2> placement_points;
	float current_x_size = 0.0f, current_y_size = 0.0f, starting_x = 0.0f, starting_y = 0.0f;
	bool j_break = false, k_break = false;
	//First loop searches for potential candidates
	for (unsigned int i = 0; i < m_parts.size(); i++) {
		glm::vec2 candidate_point = { m_parts[i].GetOrigin().x, m_parts[i].GetOrigin().y + m_partSize };
		current_x_size = 0.0f;
//Second loop we check if the object can fit on the horizontal line, if there is enough space for the width of the object
		for (unsigned int j = i; j < m_parts.size(); j++) {
			if (j == m_parts.size()-1 || m_parts[j].GetOrigin().y != m_parts[i].GetOrigin().y) 
				break;
			current_x_size += m_partSize;
			if(current_x_size >= given_size.x){
//In the third loop we check if the object can fit on the vertical axis, does it have enough space for its height
				for (unsigned int k = j; k < m_parts.size(); k++) {
					if (k == m_parts.size() - 1) {
						placement_points.push_back(candidate_point);
						i += j - i - 1;
						break;
					}
					const glm::vec2& origin = m_parts[k].GetOrigin();
					if (origin.y - candidate_point.y <= given_size.y &&
						origin.x >= candidate_point.x && origin.x < candidate_point.x + given_size.x) {
						break;
					}
					else if (origin.y - candidate_point.y > given_size.y && origin.x > candidate_point.x + given_size.x){
						placement_points.push_back(candidate_point);
						i += j - i - 1;
						break;
					}
				}
				break;
			}
		}
	}
	return placement_points;
}