#pragma once
#include "../PhysicsComponent.h"
#include "../Worms/Components/MapComponent/CollisionMapComponent.h"

class MapPhysicsComponent : public PhysicsComponent {
	private:
		std::vector<std::shared_ptr<PhysicsComponent>> m_activeComponents;
		std::shared_ptr<CollisionMapComponent> m_map;
		std::vector<Rect> m_parts;
		unsigned int m_partSize;
	public:
		MapPhysicsComponent(Entity* master) : PhysicsComponent(master,CollisionLayer::Map), m_map(nullptr), m_partSize(0){};
		void Set_Map(std::shared_ptr<CollisionMapComponent> map, unsigned int part_size);

		void ResolveActive()override;
		bool Intersects(std::shared_ptr<PhysicsComponent>&)override;
		std::vector<glm::vec2> Find_Placemenet_Points(glm::vec2 size);
};

