#include "PhysicsComponent.h"
#include "../Worms/Objects/Entity.h"

//Has to be in a cpp because it fetches from the master,  Entity.h has to be included
void PhysicsComponent::SetPosition() {
	const glm::vec2& pos = m_master->transform->GetPosition();
	m_rect.SetOrigin({ pos.x + m_offset.x, pos.y + m_offset.y });
}