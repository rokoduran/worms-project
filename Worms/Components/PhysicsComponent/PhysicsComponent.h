#pragma once
#include "util/Shapes/Rectangle.h"
#include "../Component.h"
#include <memory>
enum class CollisionLayer{
	None = 1,    
	Entity = 2,
	Water = 3,
	Projectile = 4,
	Crate = 5,
	Thrown = 6,
	Destruction = 7,
	Map = 8
};

class PhysicsComponent: public Component {
	protected:
		glm::vec2 m_offset;
		Rect m_rect;
		bool m_active;
		CollisionLayer m_layer;
	public:
		PhysicsComponent(Entity* master, CollisionLayer layer) : Component(master), m_layer(layer), m_active(false), m_rect(0.0f,0.0f,0.0f,0.0f), m_offset(0.0f,0.0f){};
		virtual bool Intersects(std::shared_ptr<PhysicsComponent>& other) {
			return m_rect.Intersects(other->Get_Rect());
		};
		virtual void ResolveOverlap(std::shared_ptr<PhysicsComponent>& other) {};
		const Rect& Get_Rect() {
			return m_rect;
		};
		CollisionLayer GetLayer() const {
			return m_layer;
		};
		bool GetActivity()const {
			return m_active;
		}
		void SetActivity(bool set) {
			m_active = set;
		}
		void SetRect(const glm::vec2& size) {
			m_rect.SetSize(size);
		};
		void SetOffset(const glm::vec2& offset) {
			m_offset = offset;
		}
		virtual void ResolveActive() {};
		virtual void Action() {};
		virtual void Move(const glm::vec2&){}
		COMPONENT_CLASS_TYPE(ePhysics);
	protected:
		void SetPosition();
};