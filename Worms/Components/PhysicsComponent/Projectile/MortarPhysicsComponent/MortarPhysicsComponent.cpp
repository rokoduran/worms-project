#include "MortarPhysicsComponent.h"
#include "../Worms/Components/PhysicsComponent/Destruction/DestructionPhysicsComponent.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

void MortarPhysicsComponent::Action() {
	if (!m_actionTriggered) {
		SharedAccess& sa = m_master->sharedData;
		std::shared_ptr<Entity> entity = std::make_shared<Entity>(sa);
		auto destro = entity->AddComponent<DestructionPhysicsComponent>();
		destro->SetDamage(m_damage);
		const glm::vec2& size = m_rect.GetSize();
		const glm::vec2& origin = m_rect.GetOrigin();
		entity->transform->SetPosition(origin.x - m_destructionDimensions.x / 2, origin.y - m_destructionDimensions.y / 2);
		destro->SetRect(m_destructionDimensions);
		m_master->sharedData.entities.Add(entity);
		std::string sound_path = "assets/sounds/Explosion" + NumberToString(glm::linearRand(1, 3)) + ".wav";
		std::shared_ptr<Sound> effect = m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(sound_path));
		effect->Play();
		m_master->SetForRemoval();
		m_actionTriggered = true;
		const glm::vec2& current_position = m_rect.GetCenter();
		const glm::vec2& velocity = m_master->GetComponent<VelocityComponent>()->GetVelocity();
		std::string path = "assets/Weapons/Mortar/Mortar_cluster.png";

		for(unsigned int i = 0; i < 3; i++){
			std::shared_ptr<Entity> cluster = std::make_shared<Entity>(sa);
			auto sprite = cluster->AddComponent<SpriteComponent>();
			sprite->Load(path);
			sprite->SetDrawLayer(DrawLayer::Entity);
			auto vel_comp = cluster->AddComponent<VelocityComponent>();
			glm::vec2 vel_value{ -glm::linearRand(0.7f,1.0f)* velocity.x , -glm::linearRand(0.7f,1.0f)* velocity.y/10 };
			vel_comp->SetVelocity(vel_value);
			auto phys = cluster->AddComponent<ProjectilePhysicsComponent>();
			
			cluster->transform->SetPosition(current_position.x,current_position.y);
			phys->SetDamage(30);
			phys->SetRect({ 2.0f,2.0f });
			phys->SetOffset({ 14.0f,14.0f });
			phys->SetDestructionDimensions({ 64.0f,64.0f });
			m_master->sharedData.entities.Add(cluster);
		}
	}
}