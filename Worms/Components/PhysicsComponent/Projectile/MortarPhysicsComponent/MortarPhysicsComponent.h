#pragma once
#include "../ProjectilePhysicsComponent.h"

class MortarPhysicsComponent : public ProjectilePhysicsComponent {
	public:
		MortarPhysicsComponent(Entity* master) : ProjectilePhysicsComponent(master) {};
		void Action() override;
};