#include "ProjectilePhysicsComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Destruction/DestructionPhysicsComponent.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

ProjectilePhysicsComponent::ProjectilePhysicsComponent(Entity* master) : PhysicsComponent(master, CollisionLayer::Projectile), m_damage(0), m_destructionDimensions(0.0f, 0.0f) {
	SetActivity(true);
}

void ProjectilePhysicsComponent::ResolveOverlap(std::shared_ptr<PhysicsComponent>& other) {
	switch (other->GetLayer()) {
		case(CollisionLayer::Entity): {
			Action();
			break;
		}
		case(CollisionLayer::Crate): {
			Action();
			break;
		}
		case(CollisionLayer::Water): {
			std::string path = "assets/sounds/splash.wav";
			m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path))->Play();
			m_actionTriggered = true;
			m_master->SetForRemoval();
			break;
		}
	}
}

void ProjectilePhysicsComponent::Start() {
	SetPosition();
}

void ProjectilePhysicsComponent::ResolveActive() {
	SetPosition();
	if (!m_master->sharedData.gameBounds.Contains(m_rect.GetCenter()))
		m_master->SetForRemoval();
}

void ProjectilePhysicsComponent::Action() {
	if(!m_actionTriggered){
		std::shared_ptr<Entity> entity = std::make_shared<Entity>(m_master->sharedData);
		auto destro = entity->AddComponent<DestructionPhysicsComponent>();
		destro->SetDamage(m_damage);
		const glm::vec2& size = m_rect.GetSize();
		const glm::vec2& origin = m_rect.GetOrigin();
		entity->transform->SetPosition(origin.x - m_destructionDimensions.x / 2, origin.y - m_destructionDimensions.y / 2);
		destro->SetRect(m_destructionDimensions);
		m_master->sharedData.entities.Add(entity);
		std::string path = "assets/sounds/Explosion" + NumberToString(glm::linearRand(1, 3)) + ".wav";
		std::shared_ptr<Sound> effect = m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path));
		effect->Play();
		m_master->SetForRemoval();
		m_actionTriggered = true;
	}
}