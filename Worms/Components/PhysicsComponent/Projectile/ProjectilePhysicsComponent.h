#pragma once
#include "../PhysicsComponent.h"

class ProjectilePhysicsComponent : public PhysicsComponent {
	protected:
		bool m_actionTriggered = false;
		int m_damage;
		glm::vec2 m_destructionDimensions;
	public:
		ProjectilePhysicsComponent(Entity* master);
		void Start()override;
		void ResolveActive()override;
		void SetDamage(int dmg) {
			m_damage = dmg;
		}
		void SetDestructionDimensions(const glm::vec2& dim) {
			m_destructionDimensions = dim;
		}
		void ResolveOverlap(std::shared_ptr<PhysicsComponent>& other)override;
		void Action()override;
};