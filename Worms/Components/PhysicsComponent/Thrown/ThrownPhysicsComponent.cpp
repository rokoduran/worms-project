#include "ThrownPhysicsComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Destruction/DestructionPhysicsComponent.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

void ThrownPhysicsComponent::ProcessEvent(Event e) {
	switch (e) {
		case(Event::TimerRanOut): 
			Action();
	}
};

void ThrownPhysicsComponent::Start() {
	m_vel = m_master->GetComponent<VelocityComponent>();
	SetPosition();
}

ThrownPhysicsComponent::ThrownPhysicsComponent(Entity* master) : PhysicsComponent(master, CollisionLayer::Thrown), m_damage(0), m_destructionParameters(0.0f, 0.0f), m_bouncinessFactor(0.0f) {
	SetActivity(true);
};
void ThrownPhysicsComponent::Move(const glm::vec2& resolution) {
	glm::vec2 velocity = m_vel->GetVelocity();
	if(!m_vel->GetGrounded())
		m_master->transform->Move(resolution);
	if (fabs(velocity.y) < 20.0f && fabs(velocity.x) < 20.0f){
		velocity.y = 0.0f;
		velocity.x = 0.0f;
		m_vel->SetGrounded(true);
	}
	if (velocity.x > 0) {
		if (resolution.x > 0)
			velocity.x *= m_bouncinessFactor;
		else if (resolution.x < 0)
			velocity.x *= -m_bouncinessFactor;
	}
	else if (velocity.x < 0) {
		if (resolution.x > 0)
			velocity.x *= -m_bouncinessFactor;
		else if (resolution.x < 0)
			velocity.x *= m_bouncinessFactor;
	}
	if (velocity.y > 0) {
		if (resolution.y > 0)
			velocity.y *= m_bouncinessFactor;
		else if (resolution.y < 0)
			velocity.y *= -m_bouncinessFactor;
	}
	else if (velocity.y < 0) {
		if (resolution.y > 0)
			velocity.y *= -m_bouncinessFactor;
		else if (resolution.x < 0)
			velocity.y *= m_bouncinessFactor;
	}
	m_vel->SetVelocity(velocity);
	
	SetPosition();
};

void ThrownPhysicsComponent::ResolveActive() {
	SetPosition();
	if (!m_master->sharedData.gameBounds.Contains(m_rect.GetCenter()))
		m_master->SetForRemoval();
}

void ThrownPhysicsComponent::ResolveOverlap(std::shared_ptr<PhysicsComponent>& other) {
	switch (other->GetLayer()) {
		case(CollisionLayer::Entity): {
			glm::vec2 result = m_rect.SolveOverlap(other->Get_Rect());
			Move(result);
			break;
		}
		case(CollisionLayer::Crate): {
			glm::vec2 result = m_rect.SolveOverlap(other->Get_Rect());
			Move(result);
			break;
		}
		case(CollisionLayer::Water): {
			std::string path = "assets/sounds/splash.wav";
			m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path))->Play();
			m_master->SetForRemoval();
			break;
		}
	}
};

void ThrownPhysicsComponent::Action() {
	std::shared_ptr<Entity> entity = std::make_shared<Entity>(m_master->sharedData);
	auto destro = entity->AddComponent<DestructionPhysicsComponent>();
	destro->SetDamage(m_damage);
	const glm::vec2& size = m_rect.GetSize();
	const glm::vec2& origin = m_rect.GetOrigin();
	entity->transform->SetPosition(origin.x - m_destructionParameters.x / 2, origin.y - m_destructionParameters.y / 2);
	destro->SetRect(m_destructionParameters);
	m_master->sharedData.entities.Add(entity);
	std::string path = "assets/sounds/Explosion" + NumberToString(glm::linearRand(1, 3))+ ".wav";
	std::shared_ptr<Sound> effect = m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path));
	effect->Play();
	m_master->SetForRemoval();
}