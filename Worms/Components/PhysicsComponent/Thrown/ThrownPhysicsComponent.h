#pragma once
#include "../PhysicsComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"

class ThrownPhysicsComponent : public PhysicsComponent {
	private:
		int m_damage;
		glm::vec2 m_destructionParameters;
		float m_bouncinessFactor;
		std::shared_ptr<VelocityComponent> m_vel;
	public:
		void ProcessEvent(Event)override;
		void Start()override;
		ThrownPhysicsComponent(Entity* master);
		void ResolveActive()override;
		void SetDamage(int dmg) {
			m_damage = dmg;
		}
		void SetDestructionDimensions(const glm::vec2& dim) {
			m_destructionParameters = dim;
		}
		void SetBounce(float bounce) {
			m_bouncinessFactor = bounce;
		};
		void Action()override;
		void Move(const glm::vec2&)override;;
		void ResolveOverlap(std::shared_ptr<PhysicsComponent>&)override;
};