#pragma once
#include "../PhysicsComponent.h"

class WaterPhysicsComponent : public PhysicsComponent {
	public:
		WaterPhysicsComponent(Entity* master) : PhysicsComponent(master,CollisionLayer::Water){};
		void Start()override {
			SetPosition();
		}
};