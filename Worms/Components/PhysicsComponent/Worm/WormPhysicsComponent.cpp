#include "WormPhysicsComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../Destruction/DestructionPhysicsComponent.h"
#include "../Crate/HealthCratePhysicsComponent/HealthCratePhysicsComponent.h"
#include "../Crate/WeaponCratePhysicsComponent/WeaponCratePhysicsComponent.h"
#include "../Worms/Components/DamageComponent/DamageComponent.h"
#include "../Worms/Components/InventoryComponent/InventoryComponent.h"

void WormsPhysicsComponent::Start() {
	m_velocity = m_master->GetComponent<VelocityComponent>();
	m_animation = m_master->GetComponent<WormAnimationComponent>();
}

void WormsPhysicsComponent::Move(const glm::vec2& pos) {
	glm::vec2 move_amount;
  if (pos.y >= 0 && pos.y <= 8){
		if (m_animation->GetCurrentCategory() != AnimationCategory::WalkingAnimation){
			if (m_master->sharedData.turnSystem.GetInputSys().GetActiveWorm()->GetID() == m_master->GetID()) {
				Landing land;
				m_animation->SetExplicitAnimationType(land);
			}
			else
				m_velocity->SetGrounded(true);
		}
		//NEEDED if we want to stop the falling animation after walking
		else if (m_animation->GetCurrentAnimationFrameIndex() == 4)
			m_velocity->SetGrounded(true);
		m_velocity->SetVelocity({ 0.0f,0.0f });
		move_amount = { 0.0f,pos.y };
	}
	else {
		move_amount = { pos.x, pos.y };
		
		if(!m_velocity->GetGrounded())
			m_velocity->SetVelocity(move_amount*4.0f);
	}
	m_master->transform->Move(move_amount);
	SetPosition();
}

void WormsPhysicsComponent::ResolveActive(){
	SetPosition();
	m_active = !m_velocity->GetGrounded();
	if (!m_master->sharedData.gameBounds.Contains(m_rect.GetCenter())){
		m_master->sharedData.turnSystem.WormDied(m_master->GetID());
		m_master->SetForRemoval();
	}
}

void WormsPhysicsComponent::ResolveOverlap(std::shared_ptr<PhysicsComponent>& other) {
	switch (other->GetLayer()) {
		case(CollisionLayer::Crate): {
			std::shared_ptr<CratePhysicsComponent> crate = std::static_pointer_cast<CratePhysicsComponent>(other);
			crate->GetMaster()->SetForRemoval();
			if(!crate->CrateUsed()){
				std::string announcement = "Got ";
				switch (crate->Get_Crate_Type()) {
					case(CrateType::Health): {
						std::shared_ptr<HealthCratePhysicsComponent> hp_crate = std::static_pointer_cast<HealthCratePhysicsComponent>(crate);
						m_master->GetComponent<DamageComponent>()->Heal(hp_crate->GetHP());
						announcement = announcement + NumberToString(hp_crate->GetHP()) + " HP.";
						break;
						}
					case(CrateType::Weapon): {
						std::shared_ptr<WeaponCratePhysicsComponent> wep_crate = std::static_pointer_cast<WeaponCratePhysicsComponent>(crate);
						std::shared_ptr<Weapon> weapon = wep_crate->GetWeapon();
						m_master->GetComponent<InventoryComponent>()->AddWeapon(weapon);
						announcement = announcement + NumberToString(weapon->GetAmmo())+ " " + weapon->GetWeaponName() + " ammo.";
						break;
					}
				}
				m_master->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
			}
			break;
		}

		case(CollisionLayer::Destruction): {
			const glm::vec2& this_center = m_rect.GetCenter();
			const glm::vec2& other_center = other->Get_Rect().GetCenter();
			const glm::vec2& other_size = other->Get_Rect().GetSize();
			float distance = sqrt((this_center.x - other_center.x)*(this_center.x - other_center.x) + (this_center.y - other_center.y)*(this_center.y - other_center.y));
			float reach = (other_size.x + other_size.y) / 2;
			float damage_ratio = 1 - (distance / reach);
			std::shared_ptr<DestructionPhysicsComponent> destro = std::static_pointer_cast<DestructionPhysicsComponent>(other);
			m_master->GetComponent<DamageComponent>()->Damage((int)destro->GetDamage()*(damage_ratio));
			m_velocity->SetVelocity(m_rect.SolveOverlap(other->Get_Rect())*damage_ratio*5.0f);
			m_velocity->SetGrounded(false);
			Falling fall;
			m_animation->SetExplicitAnimationType(fall);
			break;
		}
		case (CollisionLayer::Water): {
			if (other->Get_Rect().Contains(m_rect.GetCenter())) {
				std::string path = "assets/sounds/splash.wav";
				m_master->sharedData.soundManager.Get(m_master->sharedData.soundManager.Add(path))->Play();
				std::string announcement = m_master->GetComponent<WormSpriteComponent>()->Get_Worm_Name() + " went to marry a mermaid.";
				m_master->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(announcement);
				m_master->sharedData.turnSystem.WormDrowned(m_master->GetID());
			}
			break;
		}
	}
}
