#pragma once
#include "../PhysicsComponent.h"
#include "../Worms/Components/AnimationComponent/WormAnimationComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"

class WormsPhysicsComponent : public PhysicsComponent {
	private:
		std::shared_ptr<WormAnimationComponent> m_animation;
		std::shared_ptr<VelocityComponent> m_velocity;

	public:
		WormsPhysicsComponent(Entity* master) : PhysicsComponent(master,CollisionLayer::Entity), m_velocity(nullptr) {};
		
		void Start()override;
		void Move(const glm::vec2&)override;
		void ResolveActive()override;
		void ResolveOverlap(std::shared_ptr<PhysicsComponent>&)override;
};