#pragma once
#include "../Worms/Managers/ResourceManager.h"

//Circular dependency entitymanager & turnsystem included
class Texture;
class Font;
class Sound;
class Window;
class EntityManager;
class TurnSystem;

struct SharedAccess {
	Rect& gameBounds;
	EntityManager& entities;
	ResourceManager<Texture>& texManager;
	ResourceManager<Sound>& soundManager;
	ResourceManager<Font>& fontManager;
	TurnSystem& turnSystem;
	Window& window;
	SharedAccess(Rect& rect, EntityManager& ent, ResourceManager<Texture>& tex, ResourceManager<Sound>& sound, ResourceManager<Font>&fm, TurnSystem& ts, Window& window) : gameBounds(rect), entities(ent),
		texManager(tex), soundManager(sound), fontManager(fm), turnSystem(ts), window(window) {};
};
