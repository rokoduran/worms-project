#include "SpriteComponent.h"
#include "../Worms/Objects/Entity.h"

void SpriteComponent::Load(unsigned int id) {
	m_sprite.SetTexture(*m_master->sharedData.texManager.Get(id));
}

void SpriteComponent::Start() {
	const glm::vec2& pos = m_master->transform->GetPosition();;
	m_sprite.SetPosition(pos);
	float angle = m_master->transform->GetRotation();
	m_sprite.SetRotation(angle);
}

void SpriteComponent::ProcessEvent(Event e) {
	switch (e) {
		case(Event::TransformationOccured): {
			const glm::vec2& pos = m_master->transform->GetPosition();;
			m_sprite.SetPosition(pos);
			float angle = m_master->transform->GetRotation();
			m_sprite.SetRotation(angle);
		}
	}
}

void SpriteComponent::Update(float dt) {
//It doesnt really matter if the spritecomponents fetches the transforms position every frame,
//because it's used by objects which are constantly moving. But for consistency's sake, it'll be an event
}

bool SpriteComponent::ShouldStopDrawing()const{
	return m_master->ShouldBeRemoved();
}

void SpriteComponent::SetRotatingAxes(const glm::vec3& axes) {
	m_sprite.SetRotatingAxes(axes);
}

void SpriteComponent::Load(const std::string& path) {
	ResourceManager<Texture>& manager = m_master->sharedData.texManager;
	m_sprite.SetTexture(*manager.Get(manager.Add(path)));
}

void SpriteComponent::Draw(Window& win) {
	win.DrawQuad(m_sprite);
}

void SpriteComponent::SetFrameCoordinates(const glm::vec2* tex_coords, const glm::vec2& size) {
	m_sprite.SetTexCoords(tex_coords, size);
}