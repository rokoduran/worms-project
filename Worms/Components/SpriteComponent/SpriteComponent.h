#pragma once
#include "../DrawComponent/DrawComponent.h"
#include "../engine/graphics/drawables/sprite/Sprite.h"
#include "../Worms/Managers/ResourceManager.h"


class SpriteComponent : public DrawComponent{
	private:
		Sprite m_sprite;
	public:
		SpriteComponent(Entity* entity) : DrawComponent(entity) {};
		void Start()override;
		void ProcessEvent(Event)override;
		void Update(float)override;
		void Load(unsigned int id);
		void Load(const std::string& filepath);
		bool ShouldStopDrawing()const override;
		const glm::vec2& GetSpriteCenter() {
			return m_sprite.GetCenter();
		}
		void SetRotatingAxes(const glm::vec3&);
		void Draw(Window&) override;
		void SetFrameCoordinates(const glm::vec2* tex_coords, const glm::vec2& size);
};