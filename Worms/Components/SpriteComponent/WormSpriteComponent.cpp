#include "WormSpriteComponent.h"
#include "../Worms/Objects/Entity.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"

WormSpriteComponent::WormSpriteComponent(Entity* master) : DrawComponent(master), m_sprite(), m_crosshair(*m_master->sharedData.texManager.Get(m_master->sharedData.texManager.Add("assets/worm_sprites/crosshair.png"))), 
m_wormName(*m_master->sharedData.fontManager.Get(m_master->sharedData.fontManager.Add("assets/fonts/BRITANIC.TTF")), m_sprite.GetPosition()),
m_teamName(*m_master->sharedData.fontManager.Get(m_master->sharedData.fontManager.Add("assets/fonts/BRITANIC.TTF")), m_sprite.GetPosition()), 
m_hp(*m_master->sharedData.fontManager.Get(m_master->sharedData.fontManager.Add("assets/fonts/BRITANIC.TTF")), m_sprite.GetPosition()) {
	m_wormName.SetCameraAffected(1);
	m_teamName.SetCameraAffected(1);
	m_hp.SetCameraAffected(1);
	m_crosshairEnabled = false;
};

void WormSpriteComponent::Start() {
	const glm::vec2 pos = m_master->transform->GetPosition();
	glm::vec2 diff = pos - m_sprite.GetPosition();
	m_sprite.SetPosition(pos);
	m_sprite.SetRotation(m_master->transform->GetRotation());
	m_wormName.Move(diff);
	m_teamName.Move(diff);
	m_hp.Move(diff);
	m_crosshair.Move(diff);

}

void WormSpriteComponent::ProcessEvent(Event e) {
	switch (e) {
		case(Event::DirectionChanged): {
			SetCrosshairAngle(180.0f - GetCrosshairAngle()*180.0f / M_PI); 
			break;
			}

		case(Event::TransformationOccured): {
			float angle = m_master->transform->GetRotation();
			const glm::vec2& pos = m_master->transform->GetPosition();;
			glm::vec2 diff = pos - m_sprite.GetPosition();
			m_sprite.SetPosition(pos);
			m_wormName.Move(diff);
			m_teamName.Move(diff);
			m_hp.Move(diff);
			m_crosshair.Move(diff);
			m_sprite.SetRotation(m_master->transform->GetRotation());
			break;
			}
		case(Event::NewTurn): {
			std::string worm_up = m_wormName.GetLabelText() + " from " + m_teamName.GetLabelText() + " is up!";
			m_master->sharedData.turnSystem.GetBroadcastSystem().PlayAnnouncement(worm_up);
				m_crosshairEnabled = true; 
				break;
			}
		case(Event::TurnOver): {
			m_crosshairEnabled = false;
			break;
			}
	
	}
}

void WormSpriteComponent::Load(unsigned int id) {
	m_sprite.SetTexture(*m_master->sharedData.texManager.Get(id));
}

void WormSpriteComponent::Update(float dt) {
}

bool WormSpriteComponent::ShouldStopDrawing()const {
	return m_master->ShouldBeRemoved();
}

void WormSpriteComponent::SetRotatingAxes(const glm::vec3& axes) {
	m_sprite.SetRotatingAxes(axes);
}

void WormSpriteComponent::Load(const std::string& path) {
	ResourceManager<Texture>& manager = m_master->sharedData.texManager;
	m_sprite.SetTexture(*manager.Get(manager.Add(path)));
}

void WormSpriteComponent::Draw(Window& win) {
	win.DrawQuad(m_sprite);
	m_wormName.Draw(win);
	m_teamName.Draw(win);
	m_hp.Draw(win);
	if(m_crosshairEnabled)
		win.DrawQuad(m_crosshair);
}

void WormSpriteComponent::SetFrameCoordinates(const glm::vec2* tex_coords, const glm::vec2& size) {
	m_sprite.SetTexCoords(tex_coords, size);
}

void WormSpriteComponent::SetTeamName(const std::string& teamname) {
	m_teamName.UpdateText(teamname);
	const glm::vec2& m_sprite_pos = m_sprite.GetPosition();
	m_teamName.SetPosition({ m_sprite_pos.x, m_sprite_pos.y + m_sprite.GetSize().y + 2* m_teamName.GetFontSize() + 7 });
	float center_diff = m_sprite.GetCenter().x - m_teamName.GetBoxCenter().x;
	m_crosshair.SetCenter(m_sprite.GetCenter());
	m_teamName.Move({ center_diff,0.0f });
}

void WormSpriteComponent::SetWormName(const std::string& name) {
	m_wormName.UpdateText(name);
	const glm::vec2& m_sprite_pos = m_sprite.GetPosition();
	m_wormName.SetPosition({ m_sprite_pos.x, m_sprite_pos.y + m_sprite.GetSize().y + m_wormName.GetFontSize() + 7 });
	float center_diff = m_sprite.GetCenter().x - m_wormName.GetBoxCenter().x;
	m_wormName.Move({ center_diff,0.0f });
}

void WormSpriteComponent::SetHP(unsigned int hitpoints) {
	std::string str_hp = NumberToString(hitpoints);
	m_hp.UpdateText(str_hp);
	const glm::vec2& m_sprite_pos = m_sprite.GetPosition();
	m_hp.SetPosition({ m_sprite_pos.x, m_sprite_pos.y + m_sprite.GetSize().y + 5 });
	float center_diff = m_sprite.GetCenter().x - m_hp.GetBoxCenter().x;
	m_hp.Move({ center_diff,0.0f });
}

void WormSpriteComponent::SetBoxesColor(unsigned int boxcolor) {
	unsigned int white = 0xffffffff;
	m_wormName.SetBoxColor(boxcolor);
	m_wormName.SetLabelColor(white);
	m_teamName.SetBoxColor(boxcolor);
	m_teamName.SetLabelColor(white);
	m_hp.SetBoxColor(boxcolor);
	m_hp.SetLabelColor(white);
}