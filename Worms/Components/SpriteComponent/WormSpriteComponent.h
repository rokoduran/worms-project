#pragma once
#include "../DrawComponent/DrawComponent.h"
#include "../engine/graphics/drawables/sprite/Sprite.h"
#include "../Worms/Managers/ResourceManager.h"
#include "../Worms/Structures/BoxLabel/BoxLabel.h"

class WormSpriteComponent : public DrawComponent {
private:
	Sprite m_sprite, m_crosshair;
	BoxLabel m_wormName, m_teamName, m_hp;
	bool m_crosshairEnabled;
public:
	WormSpriteComponent(Entity* entity);
	void Start()override;
	void Update(float)override;
	void ProcessEvent(Event)override;
	void Load(unsigned int id);
	void Load(const std::string& filepath);
	bool ShouldStopDrawing()const override;
	void SetRotatingAxes(const glm::vec3&);
	void SetWormName(const std::string& wormname);
	void SetTeamName(const std::string& teamname);
	void SetHP(unsigned int hp);
	void SetBoxesColor(unsigned int);
	void Draw(Window&) override;
	void SetFrameCoordinates(const glm::vec2* tex_coords, const glm::vec2& size);
	const std::string& Get_Worm_Name()const{
		return m_wormName.GetLabelText();
	}
	const glm::vec2& Get_Center()const {
		return m_sprite.GetCenter();
	}
	float GetCrosshairAngle() {
		return m_crosshair.GetRotation();
	}
	void SetCrosshairAngle(float angle) {
		m_crosshair.SetRotation(angle);
	}
};