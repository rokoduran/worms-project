#include "TimerComponent.h"
#include "../Worms/Objects/Entity.h"

TimerComponent::TimerComponent(Entity* master): Component(master), m_maxTime(0.0f), m_currentTime(0.0f) {}

void TimerComponent::SetMaxTime(float maxtime) {
	m_maxTime = maxtime;
}

void TimerComponent::Update(float dt) {
	m_currentTime += dt;
	if (m_currentTime > m_maxTime)
		m_master->RaiseEvent(Event::TimerRanOut);
}