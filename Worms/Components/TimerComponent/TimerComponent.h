#pragma once
#include "../Component.h"


class TimerComponent : public Component {
	private:
		float m_maxTime, m_currentTime;
	public:
		TimerComponent(Entity* master);
		void SetMaxTime(float max);
		void Update(float dt);

		COMPONENT_CLASS_TYPE(eTimer)
};