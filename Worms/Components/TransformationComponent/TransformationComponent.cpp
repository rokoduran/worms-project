#include "TransformationComponent.h"
#include "../Worms/Objects/Entity.h"

TransformationComponent::TransformationComponent(Entity* master) : Component(master), m_position(0.0f, 0.0f), m_rotation(0.0f) {};

void TransformationComponent::Move(float x, float y) {
	m_position.x += x;
	m_position.y += y;
	m_master->RaiseEvent(Event::TransformationOccured);
}

void TransformationComponent::SetPosition(float x, float y) {
	m_position.x = x;
	m_position.y = y;
}

void TransformationComponent::Move(const glm::vec2& pos) {
	m_position.x += pos.x;
	m_position.y += pos.y;
	m_master->RaiseEvent(Event::TransformationOccured);
}

void TransformationComponent::SetRotation(float angle) {
	m_rotation = angle;
	while (fabs(m_rotation) > 360.0f)
		m_rotation = m_rotation < 0? m_rotation + 360.0f :m_rotation - 360.0f;
	m_master->RaiseEvent(Event::TransformationOccured);
}

void TransformationComponent::Rotate(float angle) {
	m_rotation += angle;
	while (fabs(m_rotation) > 360.0f)
		m_rotation = m_rotation < 0 ? m_rotation + 360.0f : m_rotation - 360.0f;
	m_master->RaiseEvent(Event::TransformationOccured);
}

void TransformationComponent::ResetRotation() {
	m_rotation = 0;
	m_master->RaiseEvent(Event::TransformationOccured);
}
