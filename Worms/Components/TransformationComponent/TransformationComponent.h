#pragma once
#include "../Component.h"

//Automatically included in every entity. Can be fetched by any other component to induce transformations on the entity
class TransformationComponent : public Component{
	private:
		glm::vec2 m_position;
		float m_rotation;
	public:
		TransformationComponent(Entity* master);

		void Move(float x, float y);

		void SetPosition(float x, float y);

		void Move(const glm::vec2& pos);

		void SetRotation(float angle);

		void ResetRotation();

		void Rotate(float angle);

		float GetRotation()const {
			return m_rotation;
		}
		const glm::vec2& GetPosition()const{
			return m_position;
		}
		COMPONENT_CLASS_TYPE(eTransform);
};