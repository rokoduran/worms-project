#include "VelocityComponent.h"
#include "../Worms/Objects/Entity.h"

glm::vec2 VelocityComponent::gravity = { 0.0f,-500.8f };
glm::vec2 VelocityComponent::wind = { -50.0f,0.0f };

//For now, max vel is set for 300 in both directions. Upon testing set to something else if the current value still causes clipping
VelocityComponent::VelocityComponent(Entity* master) : Component(master),m_velocity(0.0f,0.0f), m_maxVelocity(1000.0f, 1000.0f), m_windAffected(false), m_gravityAffected(true), m_grounded(false) {};

void VelocityComponent::Update(float dt) {
	if(!m_grounded){
		m_master->transform->Move(m_velocity*dt);
		if (m_windAffected)
			m_velocity += wind * dt;
		if (m_gravityAffected)
			m_velocity += gravity * dt;
	}
}

void VelocityComponent::SetGrounded(bool set) {
	m_grounded = set;
	m_master->RaiseEvent(Event::VelocityGrounded);
}

void VelocityComponent::SetVelocity(const glm::vec2& vec) {
	m_velocity = vec;
	FixVelocity();
}

void VelocityComponent::SetXVelocity(float x) {
	m_velocity.x = x;
	FixVelocity();
}

void VelocityComponent::SetYVelocity(float y) {
	m_velocity.y = y;
	FixVelocity();
}

void VelocityComponent::FixVelocity() {
	if (fabs(m_velocity.x) > m_maxVelocity.x)
		m_velocity.x = m_velocity.x > 0 ? m_maxVelocity.x : -m_maxVelocity.x;
	if (fabs(m_velocity.y) > m_maxVelocity.y)
		m_velocity.y = m_velocity.y > 0 ? m_maxVelocity.y : -m_maxVelocity.y;
}