#pragma once
#include "../Component.h"

class VelocityComponent : public Component {
	private:
		bool m_grounded;
		bool m_windAffected,m_gravityAffected;
		glm::vec2 m_velocity;
		glm::vec2 m_maxVelocity;
	public:	
		static glm::vec2 gravity;
		static glm::vec2 wind;
		VelocityComponent(Entity* master);
		void Update(float) override;
		void SetVelocity(const glm::vec2&);
		void SetGrounded(bool);
		bool GetGrounded()const {
			return m_grounded;
		};
		void SetXVelocity(float);
		void SetYVelocity(float);
		void SetWindAffected(bool set) {
			m_windAffected = set;
		}
		void SetGravityAffected(bool set) {
			m_gravityAffected = set;
		}
		inline const glm::vec2& GetVelocity()const {
			return m_velocity;
		}
		COMPONENT_CLASS_TYPE(eVelocity);
	private:
		void FixVelocity();
};