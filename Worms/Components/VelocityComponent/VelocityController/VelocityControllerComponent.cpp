#include "VelocityControllerComponent.h"
#include "../Worms/Objects/Entity.h"
#include <cmath>

VelocityControllerComponent::VelocityControllerComponent(Entity* master): Component(master) {};

void VelocityControllerComponent::Start() {
	m_vel = m_master->GetComponent<VelocityComponent>();
}

void VelocityControllerComponent::Update(float dt) {
	const glm::vec2& vel = m_vel->GetVelocity();
	float value = atanf(vel.y / vel.x)*180.0f / M_PI;
	if (vel.x < 0.0f && vel.y > 0.0f)
		value = 180.0f + value;
	else if (vel.x < 0.0f && vel.y < 0.0f)
		value = 180.0f + value;
	else if (vel.x > 0.0f && vel.y < 0.0f)
		value = 360.0f + value;
	m_master->transform->SetRotation(value);
}