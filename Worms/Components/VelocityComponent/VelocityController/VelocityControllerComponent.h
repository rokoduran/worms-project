#pragma once
#include "../VelocityComponent.h"

class VelocityControllerComponent : public Component {
	private:
		std::shared_ptr<VelocityComponent> m_vel;
	public:
		VelocityControllerComponent(Entity* master);
		void Start()override;
		void Update(float dt)override;
		COMPONENT_CLASS_TYPE(eVelocityController);
};