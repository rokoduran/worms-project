#include "Game.h"
#include "../Objects/Entity.h"
#include "../Managers/ResourceManager.h"
#include "../Scenes/IntroductionScene/IntroductionScene.h"
#include "../Scenes/TipsScene/TipsScene.h"
#include "../Scenes/GameScene/GameScene.h"
#include "../Scenes/PostGameScene/PostGameScene.h"

Game::Game() : m_window(Window::CreateNewWindow()), m_bounds({ -2400, -200 , 6600, 3300 }), m_sm(*m_window) {
	Init();
}

void Game::CalculateDeltaTime() {
	m_deltaTime = (float)m_clock.Restart();
	if (m_deltaTime > (1 / 60.0f))
		m_deltaTime = 1 / 60.0f;

	if (Input::IsKeyPressed(KeyboardKeys::KEYBOARD_ESC))
		m_running = 0;
}

void Game::End() {
	m_sm.End();
	m_texManager.End();
	m_soundManager.End();
	m_fontManager.End();
}

void Game::Draw() {
	m_sm.Draw();
	m_window->EndBatch();
	m_window->FlushRenderer();
	m_window->OnUpdate();
}

void Game::ProcessInput() {
	m_window->BeginBatch();
	m_window->Clear();
	m_sm.ProcessInput();
}

void Game::Update() {
	m_sm.Update(m_deltaTime);
	Sound::Update();
}

void Game::LateUpdate() {
	m_sm.LateUpdate(m_deltaTime);
}

void Game::Init() {
#ifndef DEBUG
	m_window->SetFullscreen(true);
#endif

	m_window->Set_Icon("assets/worm_sprites/idle5_2.png");
	m_clock.Restart();
	m_deltaTime = (float)m_clock.Get_Seconds();
	m_window->InitRenderer();
	std::shared_ptr<IntroductionScene> intro = std::make_shared<IntroductionScene>(m_sm, m_fontManager, *m_window);
	unsigned int introID = m_sm.Add(intro);
	std::shared_ptr<TipsScene> tips = std::make_shared<TipsScene>(m_sm, m_texManager, *m_window);
	unsigned int tipsID = m_sm.Add(tips);
	std::shared_ptr<GameScene> gamescene = std::make_shared<GameScene>(m_sm,m_texManager,m_soundManager,m_fontManager,*m_window,m_bounds); 
	unsigned int game_sceneid = m_sm.Add(gamescene); 
	std::shared_ptr<PostGameScene> post = std::make_shared<PostGameScene>(m_sm, m_fontManager, *m_window);
	unsigned int postID = m_sm.Add(post);
	m_sm.SwitchTo(introID);
}