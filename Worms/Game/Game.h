#pragma once
#include "util/Timer.h"
#include "../Managers/SceneManager/SceneManager.h"
#include "graphics/window/Window.h"
class Game {
	private:
		std::unique_ptr<Window> m_window;
		SceneManager m_sm;
		Clock m_clock;
		float m_deltaTime;
		bool m_running =  1;
		ResourceManager<Texture> m_texManager;
		ResourceManager<Sound> m_soundManager;
		ResourceManager<Font> m_fontManager;
		Rect m_bounds;
	public:
		Game();
		void ProcessInput();
		void CalculateDeltaTime();
		void Update();
		void LateUpdate();
		void Draw();
		bool IsRunning() {
			return m_running;
		}
		void End();
	private:
		void Init();
};
