#pragma once
#include "graphics/window/Input.h"
#include "graphics/window/Window.h"

class CameraManager {
	private:
		Rect& m_bounds;
		Window& m_window;
		View& m_view;
		glm::vec2 m_previousMouseCoords;
		glm::vec2 m_actualCameraLocation;
		float m_cameraVelocity;
	public:
		CameraManager(Rect& bounds,Window& window) : m_bounds(bounds), m_window(window), m_view(window.Get_View()), m_cameraVelocity(7.0f), m_previousMouseCoords(Input::GetMousePos()),m_actualCameraLocation(0.0f,0.0f) {};
		void Update() {
			glm::vec2& pos = Input::GetMousePos();
			glm::vec2 current_mouse_pos = { pos.x, m_window.GetHeight() - pos.y };
			Move_Camera(current_mouse_pos);
		}
	private:
		void Keep_Within_Bounds(float x, float y) {
			float window_width = (float)m_window.GetWidth();
			float window_height = (float)m_window.GetHeight();
			if (!m_bounds.Contains({ m_actualCameraLocation.x + 2*x, m_actualCameraLocation.y + 2*y })
				|| !m_bounds.Contains({ m_actualCameraLocation.x + 2 * x + window_width, m_actualCameraLocation.y + 2 * y + window_height })){
				m_view.Move(-x, -y);
				m_actualCameraLocation.x -= x;
				m_actualCameraLocation.y -= y;
				return;
			}
			m_actualCameraLocation.x += x;
			m_actualCameraLocation.y += y;
			m_view.Move(x, y);
			return;
		}

		void Move_Camera(glm::vec2& pos) {
			float x_movement = 0.0f,y_movement = 0.0f;
			if (pos.x <= 5)
				x_movement = -m_cameraVelocity;
			else if (pos.x >= m_window.GetWidth() -5)
				x_movement = m_cameraVelocity;
			if (pos.y <= 5)
				y_movement = -m_cameraVelocity;
			else if (pos.y >= m_window.GetHeight() - 5)
				y_movement = m_cameraVelocity;

			m_previousMouseCoords = pos;
			Keep_Within_Bounds(x_movement, y_movement);
		}
};