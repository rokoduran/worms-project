#include "EntityManager.h"

void EntityManager::Update(float deltaTime){
	m_currentFrameTime += deltaTime;
	if(m_currentFrameTime >= m_oneSixtieth){
		m_currentFrameTime -= m_oneSixtieth;
		for (auto& e : m_entities)
			e->Update(m_oneSixtieth);
		m_physsys.Update();
	}
}

void EntityManager::LateUpdate(float deltaTime){
	for (auto& e : m_entities)
		e->LateUpdate(deltaTime);
}

void EntityManager::Draw(Window& window){
	m_drawsys.Draw(window);
}

void EntityManager::Add(std::shared_ptr<Entity> object){
	m_newEntities.push_back(object);
}

void EntityManager::Deactivate() {
	m_drawsys.Deactivate();
	m_physsys.Deactivate();
	m_newEntities.clear();
	m_entities.clear();
	Entity::counter = 0;
}

void EntityManager::HandleNewEntities(){
	if (m_newEntities.size() > 0){
		for (const auto& e : m_newEntities)
			e->Start();
		m_entities.insert(m_entities.end(),m_newEntities.begin(),m_newEntities.end());
		m_physsys.Add(m_newEntities);
		m_drawsys.Add(m_newEntities);
		m_newEntities.clear();
	}
}

void EntityManager::HandleDeletion() {
	bool removal = false;

	auto it = m_entities.begin();
	while (it != m_entities.end()){
		Entity& obj = **it;
		if (obj.ShouldBeRemoved()) { 
			it = m_entities.erase(it);
			removal = true;
		} 
		else	
			++it;
	}
	if(removal){
		m_physsys.ProcessRemovals();
		m_drawsys.HandleDeletion();
	}
}