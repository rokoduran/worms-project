#pragma once
#include <memory>
#include <vector>
#include "../Worms/Systems/DrawSystem/DrawSystem.h"
#include "../Worms/Systems/PhysicsSystem/PhysicsSystem.h"
#include "../Worms/Objects/Entity.h"

class EntityManager{
	private:
		float m_oneSixtieth = 1.0f / 60.0f;
		float m_currentFrameTime = 0.0f;
		PhysicsSystem m_physsys;
		DrawSystem m_drawsys;
		std::vector<std::shared_ptr<Entity>> m_entities;
		std::vector<std::shared_ptr<Entity>> m_newEntities; 
	public:
		void Add(std::shared_ptr<Entity>);

		void Update(float deltaTime);
		void LateUpdate(float deltaTime);
		void Draw(Window& window);
		void HandleDeletion();
		void HandleNewEntities(); 
		void Deactivate();
		bool NothingActive() {
			return m_physsys.NothingActive();
		};
};