#include "SceneManager.h"


SceneManager::SceneManager(Window& window) : m_scenes(), m_curScene(nullptr), window(window), insertedSceneID(0) {};//, tex_manager(tex_manager),sound_manager(sound_manager), bounds(game_bounds) { }

void SceneManager::ProcessInput(){
	if (m_curScene)
		m_curScene->ProcessInput();
}

void SceneManager::Update(float deltaTime){
	if (m_curScene)
		m_curScene->Update(deltaTime);
}

void SceneManager::LateUpdate(float deltaTime){
	if (m_curScene)
		m_curScene->LateUpdate(deltaTime);
}

void SceneManager::End() {
	m_curScene = nullptr;
	for (auto& it : m_scenes)
		it.second->OnDelete();
	m_scenes.clear();
}

void SceneManager::Draw(){
	if (m_curScene)
		m_curScene->Draw(window);
}

unsigned int SceneManager::Add(std::shared_ptr<Scene> scene){
	auto inserted = m_scenes.insert(std::make_pair(insertedSceneID, scene));

	insertedSceneID++;

	inserted.first->second->OnCreate();

	return insertedSceneID - 1;
}

void SceneManager::Remove(unsigned int id)
{
	auto it = m_scenes.find(id);
	if (it != m_scenes.end())
	{
		if (m_curScene == it->second)
			m_curScene = nullptr;
			// If the scene we are removing is the current scene, 
			// we also want to set that to a null pointer so the scene 
			// is no longer updated.

		// We make sure to call the OnDestroy method 
		// of the scene we are removing.
		it->second->OnDelete();

		m_scenes.erase(it);
	}
}

void SceneManager::SwitchTo(unsigned int id)
{
	auto it = m_scenes.find(id);
	if (it != m_scenes.end())
	{
		if (m_curScene)
			// If we have a current scene, we call its OnDeactivate method.
			m_curScene->OnDeactivate();

		// Setting the current scene ensures that it is updated and drawn.
		m_curScene = it->second;

		m_curScene->OnActivate();
	}
}