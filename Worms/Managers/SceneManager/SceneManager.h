#pragma once

#include <memory>

#include "../Worms/Scenes/Scene.h"
#include "../Worms/Managers/ResourceManager.h"
#include <unordered_map>

class SceneManager{

private:
	// Stores all of the scenes associated with this state machine.
	std::unordered_map<unsigned int, std::shared_ptr<Scene>> m_scenes;

	// Stores a reference to the current scene. Used when drawing/updating.
	std::shared_ptr<Scene> m_curScene;

	// Stores our current scene id. This is incremented whenever 
	// a scene is added.
	Window& window;
	unsigned int insertedSceneID;
	//ResourceManager<Texture>& tex_manager;
	//ResourceManager<Sound>& sound_manager;
public:
	SceneManager(Window& window);//, ResourceManager<Texture>&, ResourceManager<Sound>&, Rect&);

	// ProcessInput, Update, LateUpdate, and Draw will simply be 
	// pass through methods. They will call the correspondingly 
	// named methods of the active scene.
	void ProcessInput();
	void Update(float deltaTime);
	void LateUpdate(float deltaTime);
	void Draw();
	void End();
	
	// Adds a scene to the state machine and returns the id of that scene.
	unsigned int Add(std::shared_ptr<Scene> scene);

	// Transitions to scene with specified id.
	void SwitchTo(unsigned int id);

	// Removes scene from state machine.
	void Remove(unsigned int id);
};