#include "CollisionMap.h"
#include "graphics/drawables/sprite/Texture.h"
#include "util/stb_image/stb_image.h"
#include "util/Log.h"

void CollisionMap::SetNoiseData(unsigned char* data, const std::string& file) {
	glm::vec2 size(1920, 696);
	m_area.SetSize(size);
	if (m_data != NULL){
		delete[] m_data;
		delete m_tex;
	}

	int width, height, channels;
	stbi_set_flip_vertically_on_load(1);
	m_data = stbi_load(file.c_str(), &width, &height, &channels, 4);

	//Postavljamo transparency
	m_data[3] = data[0] * 255;
	for (unsigned int i = 1; i < (unsigned int)width*height; i++)
		m_data[(i * 4) - 1] = data[i] * 255;

	m_tex = new Texture(m_data, 1920, 696, GL_RGBA8, GL_RGBA);
}

CollisionMap::~CollisionMap() {
	if(m_data!=NULL){
		delete[] m_data;
		delete m_tex;
	}
}

void CollisionMap::SetImageData(const std::string& file) {
	int width, height, channels;
	stbi_set_flip_vertically_on_load(1);
	if (m_data != NULL) {
		delete[] m_data;
		delete m_tex;
	}
	m_data = stbi_load(file.c_str(), &width, &height, &channels, 4);
	m_area.SetSize(glm::vec2(width, height));
	m_tex = new Texture(m_data, (unsigned int)width, (unsigned int)height, GL_RGBA8, GL_RGBA);
}

const unsigned char* CollisionMap::Get_Data() const{
	return m_data;
}

void CollisionMap::UpdateTexture() {
	const glm::vec2& size = this->m_area.GetSize();
	GLCall(glBindTexture(GL_TEXTURE_2D, m_tex->GetTexID()));
	GLCall(glTexSubImage2D(GL_TEXTURE_2D,0,0,0,(int)size.x,(int)size.y,GL_RGBA,GL_UNSIGNED_BYTE,m_data));
	GLCall(glBindTexture(GL_TEXTURE_2D, m_tex->GetTexID()));
}

void CollisionMap::Damage(const Rect& other_area) {
	const glm::vec2& _size = this->m_area.GetSize();
	int x_size = (int)_size.x;
	glm::vec2 origin = { other_area.GetOrigin().x, other_area.GetOrigin().y };
	const glm::vec2& size = other_area.GetSize();
	if (origin.y == 0.0f && origin.x == 0.0f){
		m_data[3] = 0;
		m_data[7] = 0;
		m_data[11] = 0;
		m_data[15] = 0;
		m_data[19] = 0;
		m_data[23] = 0;
		m_data[27] = 0;
		m_data[31] = 0;
		origin.y = 1.0f;
	}

	for (int i = origin.y; i < origin.y + size.y; i++)
		for (int j = origin.x; j < origin.x + size.x; j++) 
			m_data[(i*x_size + j)*4 -1] = 0;
		
}

void CollisionMap::Draw(Window& window) {
	m_tex == nullptr ? window.DrawQuad(m_area, 0xffffffff) : window.DrawQuad(m_area,0xffffffff,m_tex->GetTexID());
}