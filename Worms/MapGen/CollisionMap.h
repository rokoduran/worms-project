#pragma once
#include "util/Shapes/Rectangle.h"
#include "graphics/window/Window.h"
#include <string>

class CollisionMap{
	private:
		Rect m_area;
		unsigned char* m_data = NULL;
		Texture* m_tex = nullptr;
	public:
		CollisionMap() : m_area(0.0f, 0.0f, 0.0f, 0.0f) {};
		~CollisionMap();
		void SetNoiseData(unsigned char* data, const std::string& file);
		void SetImageData(const std::string& file);
		const unsigned char* Get_Data()const;
		const Rect& GetArea()const {
			return m_area;
		}
		void Damage(const Rect&);
		void UpdateTexture();
		void Draw(Window&);
};