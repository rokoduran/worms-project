#include "Entity.h"

unsigned int Entity::counter = 0;

void Entity::SetForRemoval() {
	m_removalFlag = true;
}

bool Entity::ShouldBeRemoved()const {
	return m_removalFlag;
}

void Entity::Start(){
	for (int i = m_components.size() - 1; i >= 0; i--)
		m_components[i]->Start();
}

void Entity::Update(float dt){
	//Go over the events on all the components
	for (auto& i : m_events) {
		for (int j = m_components.size() - 1; j >= 0; j--)
			m_components[j]->ProcessEvent(i);
	}
	m_events.clear();
	for (int i = m_components.size() - 1; i >= 0; i--)
		m_components[i]->Update(dt);
}

void Entity::LateUpdate(float dt){
	for (int i = m_components.size() - 1; i >= 0; i--)
		m_components[i]->LateUpdate(dt);
}

void Entity::RaiseEvent(Event event) {
	m_events.insert(event);
}


unsigned int Entity::GetID()const {
	return m_Id;
}
