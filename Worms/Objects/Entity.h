#pragma once
#include <iostream>
#include "../Components/TransformationComponent/TransformationComponent.h"
#include "../Worms/Components/SharedAccess.h"
#include "../Structures/GameEvent/GameEvent.h"
#include <set>

class Entity {
	protected:
		friend class EntityManager;
		static unsigned int counter;
		unsigned int m_Id;
		bool m_removalFlag;
		std::vector<std::shared_ptr<Component>> m_components;
		std::set<Event> m_events;
	public:
		SharedAccess& sharedData;
		std::shared_ptr<TransformationComponent> transform;
		Entity(SharedAccess& sa) : transform(AddComponent<TransformationComponent>()), m_Id(counter++), sharedData(sa), m_removalFlag(false)  {};
		void Start();
		//Go over every component in the vector and perform action
		void Update(float);
		void LateUpdate(float);
		bool ShouldBeRemoved()const;
		void SetForRemoval();
		void RaiseEvent(Event);
		unsigned int GetID()const;

		template <typename T> 
		std::shared_ptr<T> AddComponent(){
#ifdef DEBUG
			static_assert(std::is_base_of<Component, T>::value,
				"T isn't derived from Component");
#endif
//Only one of each type allowed atm
			for (auto& c : m_components)	
				if (c->GetType() == T::GetStaticType())
					return std::static_pointer_cast<T>(c);
				
			std::shared_ptr<T> newcomp = std::make_shared<T>(this);
			m_components.push_back(newcomp);

			return newcomp;
		};

		template <typename T> std::shared_ptr<T> GetComponent(){
// Check that we don't already have a component of this type
			for (auto& c : m_components)
				if (c->GetType() == T::GetStaticType())
					return std::static_pointer_cast<T>(c);
			return nullptr;
		};


};