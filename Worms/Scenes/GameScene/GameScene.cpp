#include "GameScene.h"
#include "graphics/window/Input.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/AnimationComponent/AnimationComponent.h"
#include "../Worms/Components/AnimationComponent/WormAnimationComponent.h"
#include "../Worms/Components/PhysicsComponent/Map/MapPhysicsComponent.h"
#include "../Worms/Components/PhysicsComponent/Worm/WormPhysicsComponent.h"
#include "../Worms/Components/MapComponent/CollisionMapComponent.h"
#include "../Worms/Components/AnimationComponent/AnimationControl/AnimationInputControl.h"
#include "../Worms/Components/AnimationComponent/AnimationControl/AnimationActionControl.h"
#include "../Worms/Components/PhysicsComponent/Water/WaterPhysicsComponent.h"
#include "../Worms/Structures/Weapon/Bazooka/Bazooka.h"
#include "../Worms/Structures/Weapon/Grenade/Grenade.h"
#include "../Worms/Structures/Weapon/Mortar/Mortar.h"
#include "../Worms/Structures/Weapon/Dynamite/Dynamite.h"
#include "../Worms/Structures/Weapon/SkipGo/SkipGo.h"
#include "../Worms/Components/FloatComponent/FloatComponent.h"
#include "util/DebugClass/Debug.h"
#include "graphics/drawables/Noise/PerlinNoise.h"

GameScene::GameScene(SceneManager& sm, ResourceManager<Texture>& tm, ResourceManager<Sound>& soundm, ResourceManager<Font>& fm, Window& wn, Rect& bounds) :m_sm(sm), m_matchOver(false), m_timeUntilSwitch(5.0f),
m_turnSystem(m_matchOver, 60.0f, 4.0f, *fm.Get(fm.Add("assets/fonts/testing.ttf")), { wn.GetWidth(),wn.GetHeight() }),
m_sharedData(bounds, m_entities, tm, soundm, fm, m_turnSystem, wn), m_cameraManager(bounds, wn), m_crateSystem(0.175f, 0.075f, m_sharedData, 30) {};

void GameScene::OnCreate() {
	std::string song_name = "assets/sounds/berserk_guitar_song.wav";
	std::shared_ptr<Sound> music = m_sharedData.soundManager.Get(m_sharedData.soundManager.Add(song_name));
	music->Loop();
};

void GameScene::ProcessInput() {
	Input::Update();
}

void GameScene::OnActivate() {
	m_matchOver = false;
	m_timeUntilSwitch = 5.0f;
	InputSystem& input_system = m_turnSystem.GetInputSys();
	input_system.AttachCallback(KeyboardKeys::KEYBOARD_LEFT, AnimationInputControl::Go_Left);
	input_system.AttachCallback(KeyboardKeys::KEYBOARD_RIGHT, AnimationInputControl::Go_Right);
	input_system.AttachCallback(KeyboardKeys::KEYBOARD_ENTER, AnimationInputControl::Jump);
	input_system.AttachCallback(KeyboardKeys::KEYBOARD_BACKSPACE, AnimationInputControl::Backflip);
	input_system.AttachCallback(KeyboardKeys::KEYBOARD_UP, AnimationControl::Up);
	input_system.AttachCallback(KeyboardKeys::KEYBOARD_DOWN, AnimationControl::Down);

	CreateMap();
	CreateBackgroundObjects();
	CreateWater();
	m_turnSystem.Start();
};

void GameScene::OnDeactivate() {
	m_turnSystem.Deactivate();
	m_entities.Deactivate();
#ifdef DEBUG
	Debug::Clear();
#endif
};

void GameScene::OnDelete()  {
	OnDeactivate();
};

void GameScene::LateUpdate(float dt)  {
	m_entities.LateUpdate(dt);
	if (m_turnSystem.IsTurnOver() && !m_matchOver)
		if (m_entities.NothingActive())
			if (m_turnSystem.UpdateTimeBetween(dt)) {
				m_turnSystem.NewTurn();
				if (!m_matchOver)
					m_crateSystem.DropCrate();
			}
	m_turnSystem.Update(dt);
	if (m_matchOver) {
		m_timeUntilSwitch -= dt;
		if (m_timeUntilSwitch <= 0)
			m_sm.SwitchTo(3);
	}
};

void GameScene::Update(float dt)  {
	m_entities.HandleDeletion();
	m_entities.HandleNewEntities();
	m_entities.Update(dt);
};

void GameScene::Draw(Window& win) {
	m_cameraManager.Update();
	m_entities.Draw(win);
	m_turnSystem.Draw(win);
#ifdef DEBUG
	Debug::DrawRect(m_sharedData.gameBounds, 2, 0x0000ffff);
	Debug::Draw(win);
#endif
}

std::shared_ptr<Team> GameScene::CreateTeam(const std::string& team_name, unsigned int color, const std::vector<std::string>& names, const glm::vec2& collision) {
	std::shared_ptr<Team> team = std::make_shared<Team>(color, team_name);
	std::shared_ptr<Inventory> inventory = std::make_shared<Inventory>();
	inventory->Add_Weapon(std::make_shared<Bazooka>(30, false));
	inventory->Add_Weapon(std::make_shared<Grenade>(10, false));
	inventory->Add_Weapon(std::make_shared<Mortar>(5, false));
	inventory->Add_Weapon(std::make_shared<Dynamite>(2, false));
	for (auto& it : names)
		team->AddWorm(CreateWorm(team_name, color, it, collision, inventory));
	m_turnSystem.AddTeam(team);
	return team;

}

std::shared_ptr<Entity> GameScene::CreateWorm(const std::string& team_name, unsigned int team_color, const std::string& worm_name, const glm::vec2& collision, std::shared_ptr<Inventory>& inventory) {
	//Dummy sprite is used to set the worms name, team and hp boxlabels properly
	std::string dummy_str = "assets/worm_sprites/idle5_2.png";
	std::shared_ptr<Entity> worm = std::make_shared<Entity>(m_sharedData);
	auto invent = worm->AddComponent<InventoryComponent>();
	invent->SetInventory(inventory);
	auto sprite = worm->AddComponent<WormSpriteComponent>();
	sprite->Load(dummy_str);
	sprite->SetTeamName(team_name);
	sprite->SetWormName(worm_name);
	sprite->SetBoxesColor(team_color);
	auto velocity_comp = worm->AddComponent<VelocityComponent>();
	velocity_comp->SetGrounded(true);
	worm->AddComponent<DamageComponent>();
	sprite->SetDrawLayer(DrawLayer::Entity);
	auto wormanim_comp = worm->AddComponent<WormAnimationComponent>();
	auto dircomp = worm->AddComponent<DirectionComponent>();

	std::string str = "assets/worm_sprites/idle_animations_atlas.png";
	std::string str2 = "assets/worm_sprites/walking_and_jumping_atlas.png";
	std::string str3 = "assets/worm_sprites/weaponanimations.png";
	auto animation = worm->AddComponent<WormAnimationComponent>();
	std::shared_ptr<Texture> tex = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str));
	std::shared_ptr<Texture> tex2 = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str2));
	std::shared_ptr<Texture> tex3 = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str3));
	glm::vec2 tex_size = glm::vec2(tex->GetWidth(), tex->GetHeight());
	glm::vec2 tex_size2 = glm::vec2(tex2->GetWidth(), tex2->GetHeight());
	glm::vec2 tex_size3 = glm::vec2(tex3->GetWidth(), tex3->GetHeight());
	unsigned int resource_id = m_sharedData.texManager.Add(str);
	unsigned int resource_id2 = m_sharedData.texManager.Add(str2);
	unsigned int resource_id3 = m_sharedData.texManager.Add(str3);
	IdleDefault idle;
	Idle1 idle1;
	Idle2 idle2;
	Idle3 idle3;
	Idle4 idle4;
	Idle5 idle5;
	Idle6 idle6;
	Idle7 idle7;
	IdleCelebrating celebrate;
	IdleDamaged idledamaged;
	AimingBazooka aimbazooka;
	AimingGrenade aimgrenade;
	AimingMortar aimingmortar;
	AimingDynamite aimingdynamite;
	AimingSkipGo aimingskipgo;
	Walking walk;
	Jumping jumpin;
	Backflipping backflipping;
	Falling falling;
	BackflipFalling backflipfalling;
	Landing landing;
	std::shared_ptr<Animation> celebration_animation = Animation::CreateAnimation(6, resource_id, tex_size, glm::vec2(0.0f, 10.0f), true, glm::vec2(32.0f, 32.0f), 0.125f);
	animation->AddAnimation(celebrate, celebration_animation);
	std::shared_ptr<Animation> idle_default = Animation::CreateAnimation(7, resource_id, tex_size, glm::vec2(0.0f, 9.0f), true, glm::vec2(32.0f, 32.0f), 0.2f);
	animation->AddAnimation(idle, idle_default);
	std::shared_ptr<Animation> idle_damaged = Animation::CreateAnimation(7, resource_id, tex_size, glm::vec2(0.0f, 1.0f), true, glm::vec2(32.0f, 32.0f), 0.2f);
	animation->AddAnimation(idledamaged, idle_damaged);
	std::shared_ptr<Animation> idle_1 = Animation::CreateAnimation(11, resource_id, tex_size, glm::vec2(0.0f, 2.0f), true, glm::vec2(32.0f, 32.0f), 0.18f);
	animation->AddAnimation(idle1, idle_1);
	std::shared_ptr<Animation> idle_2 = Animation::CreateAnimation(8, resource_id, tex_size, glm::vec2(0.0f, 3.0f), true, glm::vec2(32.0f, 32.0f), 0.2f);
	animation->AddAnimation(idle2, idle_2);
	std::shared_ptr<Animation> idle_3 = Animation::CreateAnimation(8, resource_id, tex_size, glm::vec2(0.0f, 4.0f), true, glm::vec2(32.0f, 32.0f), 0.2f);
	animation->AddAnimation(idle3, idle_3);
	std::shared_ptr<Animation> idle_4 = Animation::CreateAnimation(21, resource_id, tex_size, glm::vec2(0.0f, 5.0f), true, glm::vec2(32.0f, 32.0f), 0.1f);
	animation->AddAnimation(idle4, idle_4);
	std::shared_ptr<Animation> idle_5 = Animation::CreateAnimation(7, resource_id, tex_size, glm::vec2(0.0f, 6.0f), true, glm::vec2(32.0f, 32.0f), 0.2f);
	animation->AddAnimation(idle5, idle_5);
	std::shared_ptr<Animation> idle_6 = Animation::CreateAnimation(16, resource_id, tex_size, glm::vec2(0.0f, 7.0f), true, glm::vec2(32.0f, 32.0f), 0.125f);
	animation->AddAnimation(idle6, idle_6);
	std::shared_ptr<Animation> idle_7 = Animation::CreateAnimation(7, resource_id, tex_size, glm::vec2(0.0f, 8.0f), true, glm::vec2(32.0f, 32.0f), 0.2f);
	animation->AddAnimation(idle7, idle_7);

	std::shared_ptr<Animation> walking_test = Animation::CreateAnimation(5, resource_id2, tex_size2, glm::vec2(0.0f, 0.0f), true, glm::vec2(32.0f, 32.0f), 0.10f);
	animation->AddAnimation(walk, walking_test);
	std::shared_ptr<Animation> jumping_test = Animation::CreateAnimation(4, resource_id2, tex_size2, glm::vec2(0.0f, 4.0f), true, glm::vec2(32.0f, 32.0f), 0.15f);
	animation->AddAnimation(jumpin, jumping_test);
	std::shared_ptr<Animation> backflip_test = Animation::CreateAnimation(7, resource_id2, tex_size2, glm::vec2(0.0f, 2.0f), true, glm::vec2(32.0f, 32.0f), 0.10f);
	animation->AddAnimation(backflipping, backflip_test);
	std::shared_ptr<Animation> falling_test = Animation::CreateAnimation(1, resource_id2, tex_size2, glm::vec2(0.0f, 1.0f), true, glm::vec2(32.0f, 32.0f), 0.25f);
	animation->AddAnimation(falling, falling_test);
	std::shared_ptr<Animation> backflip_fall_test = Animation::CreateAnimation(1, resource_id2, tex_size2, glm::vec2(1.0f, 1.0f), true, glm::vec2(32.0f, 32.0f), 0.25f);
	animation->AddAnimation(backflipfalling, backflip_fall_test);
	std::shared_ptr<Animation> landing_test = Animation::CreateAnimation(4, resource_id2, tex_size2, glm::vec2(0.0f, 3.0f), true, glm::vec2(32.0f, 32.0f), 0.10f);
	animation->AddAnimation(landing, landing_test);
	std::shared_ptr<Animation> aimzooka = Animation::CreateAnimation(1, resource_id3, tex_size3, glm::vec2(0.0f, 3.0f), true, glm::vec2(32.0f, 32.0f), 0.1f);
	animation->AddAnimation(aimbazooka, aimzooka);
	std::shared_ptr<Animation> aimgrena = Animation::CreateAnimation(1, resource_id3, tex_size3, glm::vec2(0.0f, 0.0f), true, glm::vec2(32.0f, 32.0f), 0.1f);
	animation->AddAnimation(aimgrenade, aimgrena);
	std::shared_ptr<Animation> aimmort = Animation::CreateAnimation(1, resource_id3, tex_size3, glm::vec2(0.0f, 2.0f), true, glm::vec2(32.0f, 32.0f), 0.1f);
	animation->AddAnimation(aimingmortar, aimmort);
	std::shared_ptr<Animation> aimdyna = Animation::CreateAnimation(1, resource_id3, tex_size3, glm::vec2(0.0f, 1.0f), true, glm::vec2(32.0f, 32.0f), 0.1f);
	animation->AddAnimation(aimingdynamite, aimdyna);
	std::shared_ptr<Animation> aimskip = Animation::CreateAnimation(6, resource_id3, tex_size3, glm::vec2(0.0f, 4.0f), true, glm::vec2(32.0f, 32.0f), 0.1f);
	animation->AddAnimation(aimingskipgo, aimskip);

	celebration_animation->SetLooped(true);
	idle_default->SetLooped(true);
	idle_damaged->SetLooped(true);
	aimzooka->SetLooped(true);
	aimgrena->SetLooped(true);
	aimmort->SetLooped(true);
	aimdyna->SetLooped(true);
	aimskip->SetLooped(true);
	animation->SetDefaultForCategory(IdleAnimation, idle);
	animation->SetDefaultForCategory(WalkingAnimation, walk);
	backflip_test->AddFrameAction(1, AnimationControl::Backflip);
	backflip_test->AddFrameAction(6, AnimationControl::BackflipFall);
	jumping_test->AddFrameAction(1, AnimationControl::Jump);
	jumping_test->AddFrameAction(3, AnimationControl::Fall);
	walking_test->AddFrameAction(1, AnimationControl::Move);
	walking_test->AddFrameAction(4, AnimationControl::Fall);
	landing_test->AddFrameAction(3, AnimationControl::End);
	auto collide = worm->AddComponent<WormsPhysicsComponent>();
	collide->SetRect(collision);
	collide->SetOffset({ 7.0f,0.0f });
	m_entities.Add(worm);
	return worm;
}

void GameScene::CreateWater() {
	const glm::vec2& origin = m_sharedData.gameBounds.GetOrigin();
	const glm::vec2& size = m_sharedData.gameBounds.GetSize();
	std::string str = "assets/background/wateratlas2.png";
	std::shared_ptr<Texture> tex = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str));
	glm::vec2 tex_size = glm::vec2(tex->GetWidth(), tex->GetHeight());
	unsigned int resource_id = m_sharedData.texManager.Add(str);
	float frame_display_time = 0.05f;
	unsigned int water_repeats = ceil(size.x / tex_size.x);

	for (unsigned int i = 0; i < water_repeats; i++) {
		//Create the entity itself
		std::shared_ptr<Entity> water = std::make_shared<Entity>(m_sharedData);
		water->transform->SetPosition(origin.x + i * tex->GetWidth(), -5.0f);
		auto s = water->AddComponent<SpriteComponent>();
		s->SetDrawLayer(DrawLayer::Water);
		auto a = water->AddComponent<AnimationComponent>();
		auto water_animation = Animation::CreateAnimation(15, resource_id, tex_size, glm::vec2(0, 0), false, glm::vec2(240.0f, 11.0f), frame_display_time);
		water_animation->SetLooped(true);
		a->SetAnimation(water_animation);
		m_entities.Add(water);
	}

	std::string str2 = "assets/background/waterbed.png";
	std::shared_ptr<Entity> water = std::make_shared<Entity>(m_sharedData);
	water->transform->SetPosition(origin.x, origin.y);
	auto s = water->AddComponent<SpriteComponent>();
	s->SetDrawLayer(DrawLayer::Water);
	s->Load(str2);
	auto physcomp = water->AddComponent<WaterPhysicsComponent>();
	std::shared_ptr<Texture> tex2 = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str2));
	physcomp->SetRect({ tex2->GetWidth(),tex2->GetHeight() });
	m_entities.Add(water);

}

void GameScene::CreateBackgroundObjects() {
	//Firstly, the gradient
	std::string str = "assets/background/sky_gradient.png";
	const glm::vec2& origin = m_sharedData.gameBounds.GetOrigin();
	const glm::vec2& size = m_sharedData.gameBounds.GetSize();
	std::shared_ptr<Texture> tex = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str));
	unsigned int gradient_repeats = (unsigned int)ceil(size.x / tex->GetWidth());
	for (unsigned int i = 0; i < gradient_repeats; i++) {
		std::shared_ptr<Entity> gradient = std::make_shared<Entity>(m_sharedData);
		gradient->transform->SetPosition(origin.x + i * tex->GetWidth(), origin.y);
		auto s = gradient->AddComponent<SpriteComponent>();
		s->Load(str);
		s->SetDrawLayer(DrawLayer::Sky);
		m_entities.Add(gradient);
	}
	//Then, the lower background
	std::string str2 = "assets/background/forest_background.png";
	std::shared_ptr<Texture> tex2 = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str2));
	unsigned int background_repeats = (unsigned int)ceil(size.x / tex2->GetWidth());
	for (unsigned int i = 0; i < background_repeats; i++) {
		std::shared_ptr<Entity> back = std::make_shared<Entity>(m_sharedData);
		back->transform->SetPosition(origin.x + i * tex2->GetWidth(), 0.0f);
		auto s = back->AddComponent<SpriteComponent>();
		s->Load(str2);
		s->SetDrawLayer(DrawLayer::Background);
		m_entities.Add(back);
	}
}

void GameScene::CreateMap() {
	std::shared_ptr<Entity> map = std::make_shared<Entity>(m_sharedData);
	auto draw_map = map->AddComponent<CollisionMapComponent>();
	draw_map->SetDrawLayer(DrawLayer::TerrainCollision);
	PerlinNoise perlin;
	srand(time(0));
	unsigned int seed = glm::linearRand(0, 150000);
	perlin.CalculateNoise(seed, 12, 7, 0.45);
	std::string map_str = "assets/map/forest.png";
	//Toggle between SetImage and SetNoise to change is the map randomly generated, or made from a supplied image
	//draw_map->SetImage(map_str);
	draw_map->SetNoise(perlin.FlushData(), "assets/map/Cobblestone.png");
	auto phys_map = map->AddComponent<MapPhysicsComponent>();
	phys_map->Set_Map(draw_map, 8);
	m_entities.Add(map);
	CreateFloatingObjects(phys_map->Get_Rect().GetSize().y);
	m_crateSystem.Set_Map_Limits(phys_map->Get_Rect().GetSize());
	//Creating the different teams
	glm::vec2 collision = { 16.0f,22.0f };
	unsigned int num_of_worms = 0;
	std::vector<std::string> names = { "Arthur" , "Lancelot", "Gawain" };
	std::shared_ptr<Team> team1 = CreateTeam("The Knights", 0x669999ff, names, collision);
	num_of_worms += team1->GetTeamSize();

	names = { "Ragnar", "Digni", "Orek" };
	std::shared_ptr<Team> team2 = CreateTeam("The Vikings", 0x996600ff, names, collision);
	num_of_worms += team2->GetTeamSize();

	names = { "Shrek", "Fiona", "Donkey" };
	std::shared_ptr<Team> team3 = CreateTeam("Dreamworks", 0x009900ff, names, collision);
	num_of_worms += team3->GetTeamSize();

	names = { "Saber", "Rin", "Shiro" };
	std::shared_ptr<Team> team4 = CreateTeam("Fate", 0x990000ff, names, collision);
	num_of_worms += team4->GetTeamSize();

	auto vector = phys_map->Find_Placemenet_Points(collision);
#ifdef DEBUG
	if (vector.size() < num_of_worms)
		ASSERT(0);
#endif
	std::vector<std::vector<std::shared_ptr<Entity>>> all_worms;
	all_worms.push_back(team1->GetAllWormsInTeam());
	all_worms.push_back(team2->GetAllWormsInTeam());
	all_worms.push_back(team3->GetAllWormsInTeam());
	all_worms.push_back(team4->GetAllWormsInTeam());

	for (auto& i : all_worms)
		for (auto& w : i) {
			unsigned int index = glm::linearRand((unsigned int)0, (unsigned int)vector.size() - 1);
			w->transform->SetPosition(vector[index].x, vector[index].y);
			//previously move
			vector.erase(vector.begin() + index);
		}
}
void GameScene::CreateFloatingObjects(float top_location) {
	std::string str1 = "assets/background/star.png";
	std::string str2 = "assets/background/cloudatlas.png";
	const glm::vec2& origin = m_sharedData.gameBounds.GetOrigin();
	const glm::vec2& size = m_sharedData.gameBounds.GetSize();
	float shard = (origin.x + size.x) / 25;
	//Stars
	for (unsigned int i = 0; i < 25; i++) {
		std::shared_ptr<Entity> prop = std::make_shared<Entity>(m_sharedData);
		auto s = prop->AddComponent<SpriteComponent>();
		s->Load(str1);
		s->SetRotatingAxes({ 0.0f,1.0f,1.0f });
		s->SetDrawLayer(DrawLayer::Floating);
		auto v = prop->AddComponent<VelocityComponent>();
		v->SetGrounded(false);
		v->SetWindAffected(true);
		v->SetGravityAffected(false);
		auto f = prop->AddComponent<FloatComponent>();
		f->SetRandomHorizontalSpawn(true);
		f->SetTopLocation(top_location + 50);
		f->SetRotating(true);
		f->SetVelocityLimits({ -50.0f,50.0f }, { -50.0f,-20.0f });
		prop->transform->SetPosition(glm::linearRand(origin.x, origin.x + size.x), top_location + 75);
		m_entities.Add(prop);
	}
	//Clouds
	std::shared_ptr<Texture> tex = m_sharedData.texManager.Get(m_sharedData.texManager.Add(str2));
	glm::vec2 texture_size = { tex->GetWidth(), tex->GetHeight() };
	unsigned int resource_id = m_sharedData.texManager.Add(str2);
	glm::vec2  sprite_size = { 256.0f,256.0f };
	for (unsigned int i = 0; i < 25; i++) {
		std::shared_ptr<Entity> cloud = std::make_shared<Entity>(m_sharedData);
		auto s = cloud->AddComponent<SpriteComponent>();
		s->Load(str2);
		s->SetDrawLayer(DrawLayer::Floating);
		Frame frame(resource_id, texture_size, { glm::linearRand(0, 2), 0 }, sprite_size, 0.0f);
		s->SetFrameCoordinates(frame.texCoordinates, sprite_size);
		//Set the other components
		auto v = cloud->AddComponent<VelocityComponent>();
		v->SetWindAffected(true);
		v->SetGravityAffected(false);
		v->SetGrounded(false);
		auto f = cloud->AddComponent<FloatComponent>();
		f->SetTopLocation(top_location + 75);
		f->SetVelocityLimits({ -50.0f,50.0f }, { 0.0f,0.0f });
		cloud->transform->SetPosition(glm::linearRand(origin.x, origin.x + size.x), top_location + 75);
		m_entities.Add(cloud);
	}
}