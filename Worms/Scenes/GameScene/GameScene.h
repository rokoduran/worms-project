#pragma once
#include "../Scene.h"
#include "../Worms/Managers/SceneManager/SceneManager.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#include "../Worms/Managers/CameraManager/CameraManager.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../Worms/Components/DamageComponent/DamageComponent.h"
#include "../Worms/Systems/CrateDropSystem/CrateDropSystem.h"
#undef min
#undef max
#include "../Worms/Components/InventoryComponent/InventoryComponent.h"

class GameScene : public Scene {
	private:
		bool m_matchOver;
		float m_timeUntilSwitch;
		EntityManager m_entities;
		CameraManager m_cameraManager;
		TurnSystem m_turnSystem;
		CrateDropSystem m_crateSystem;
		SceneManager& m_sm;
		SharedAccess m_sharedData;

	public:
		GameScene(SceneManager& sm, ResourceManager<Texture>& tm, ResourceManager<Sound>& soundm, ResourceManager<Font>& fm, Window& wn, Rect& bounds);

		void OnCreate() override;
		void ProcessInput()override;
		void OnActivate()override;
		void OnDeactivate()override;
		void OnDelete() override;
		void LateUpdate(float dt) override;
		void Update(float dt) override;
		void Draw(Window& win)override;

	private:
		std::shared_ptr<Team> CreateTeam(const std::string& team_name, unsigned int color,
			const std::vector<std::string>& names, const glm::vec2& collision);
		std::shared_ptr<Entity> CreateWorm(const std::string& team_name, unsigned int team_color, const std::string& worm_name,
			const glm::vec2& collision, std::shared_ptr<Inventory>& inventory);
		void CreateWater();
		void CreateBackgroundObjects();
		void CreateMap();
		void CreateFloatingObjects(float top_location);
};