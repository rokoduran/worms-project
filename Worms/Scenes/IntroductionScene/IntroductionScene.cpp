#include "IntroductionScene.h"
#include "../engine/graphics/window/Input.h"

IntroductionScene::IntroductionScene(SceneManager& sm, ResourceManager<Font>& fm, Window& wn) : m_sm(sm), m_zavrsniLabel(*fm.Get(fm.Add("assets/fonts/Berry Rotunda.ttf")), { 0.0f,0.0f }),
	m_gameLabel(*fm.Get(fm.Add("assets/fonts/Berry Rotunda.ttf")), { 0.0f,0.0f }), m_nameLabel(*fm.Get(fm.Add("assets/fonts/Berry Rotunda.ttf")), { 0.0f,0.0f }) {
		Font& font = *fm.Get(fm.Add("assets/fonts/Berry Rotunda.ttf"));
		font.SetSize(28);
		std::string text1 = "Zavr�ni rad 2019/20";
		std::string text2 = "Worms clone";
		std::string text3 = "Roko Duran";
		glm::vec2 window_center = { wn.GetCenter() };
		m_zavrsniLabel.UpdateText(text1);
		m_gameLabel.UpdateText(text2);
		m_nameLabel.UpdateText(text3);
		m_zavrsniLabel.SetLabelColor(0xffffffff);
		m_gameLabel.SetLabelColor(0xffffffff);
		m_nameLabel.SetLabelColor(0xffffffff);
		m_zavrsniLabel.SetPosition({ window_center.x, wn.GetHeight()*0.66f });
		m_gameLabel.SetPosition(window_center);
		m_nameLabel.SetPosition({ window_center.x, wn.GetHeight()*0.33f });
		m_zavrsniLabel.Move({ window_center.x - m_zavrsniLabel.GetBoxCenter().x, 0.0f });
		m_gameLabel.Move({ window_center.x - m_gameLabel.GetBoxCenter().x, 0.0f });
		m_nameLabel.Move({ window_center.x - m_nameLabel.GetBoxCenter().x, 0.0f });
};

void IntroductionScene::OnActivate(){
	m_countdown = 3.0f;
};

void IntroductionScene::ProcessInput(){
	Input::Update();
};
void IntroductionScene::Update(float dt){
	m_countdown -= dt;
	if (m_countdown <= 0.0f)
		m_sm.SwitchTo(1);
}
void IntroductionScene::Draw(Window& window){
	m_zavrsniLabel.Draw(window);
	m_gameLabel.Draw(window);
	m_nameLabel.Draw(window);
}