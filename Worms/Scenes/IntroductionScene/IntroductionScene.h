#pragma once
#include "../Scene.h"
#include "../Worms/Managers/SceneManager/SceneManager.h"
#include "../Worms/Managers/ResourceManager.h"
#include "../Worms/Structures/BoxLabel/BoxLabel.h"

class IntroductionScene : public Scene {
private:
	float m_countdown = 3.0f;
	SceneManager& m_sm;
	BoxLabel m_zavrsniLabel, m_gameLabel, m_nameLabel;
public:
	IntroductionScene(SceneManager& sm, ResourceManager<Font>& fm, Window& wn);
	void OnCreate()override {};
	void OnActivate()override;
	void OnDelete()override {};

	void OnDeactivate()override {};

	// The below functions can be overridden as necessary in our scenes.
	void ProcessInput()override;

	void Update(float dt)override;
	virtual void Draw(Window& window)override;
};