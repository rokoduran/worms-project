#include "PostGameScene.h"
#include "../engine/graphics/window/Input.h"

PostGameScene::PostGameScene(SceneManager& sm, ResourceManager<Font>& fm, Window& wn) : m_sm(sm), m_label(*fm.Get(fm.Add("assets/fonts/Berry Rotunda.ttf")), { 0.0f,0.0f }) {
	std::string text = " Press space to continue to the next round or press escape to leave the game.";
	m_label.UpdateText(text);
	m_label.SetLabelColor(0xffffffff);
	glm::vec2 window_center = { wn.GetCenter() };
	m_label.SetPosition({ window_center });
	m_label.Move({ window_center.x - m_label.GetBoxCenter().x, 0.0f });
};

void PostGameScene::OnActivate(){
	m_spacePressed = false;
	m_countdown = 1.5f;
};

void PostGameScene::ProcessInput(){
	Input::Update();
	if (Input::IsKeyPressed(KeyboardKeys::KEYBOARD_SPACE))
		m_spacePressed = true;
};
void PostGameScene::Update(float dt){
	if (m_spacePressed)
		m_countdown -= dt;
	if (m_countdown <= 0.0f)
		m_sm.SwitchTo(2);
}
void PostGameScene::Draw(Window& window) {
	m_label.Draw(window);
}