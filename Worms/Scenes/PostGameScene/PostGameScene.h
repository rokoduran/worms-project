#pragma once
#include "../Scene.h"
#include "../Worms/Managers/SceneManager/SceneManager.h"
#include "../Worms/Managers/ResourceManager.h"
#include "../Worms/Structures/BoxLabel/BoxLabel.h"

class PostGameScene : public Scene {
	private:
		bool m_spacePressed = false;
		float m_countdown = 1.5f;
		SceneManager& m_sm;
		BoxLabel m_label;
	public:
		PostGameScene(SceneManager& sm, ResourceManager<Font>& fm, Window& wn);
		void OnCreate()override {}
		void OnActivate()override;
		void OnDelete()override{}
		// Called whenever a transition out of a scene occurs. 
		// Can be called many times in a typical game cycle.
		void OnDeactivate()override {};

		// The below functions can be overridden as necessary in our scenes.
		void ProcessInput()override;
		void Update(float dt)override;
		void Draw(Window& window)override;
};