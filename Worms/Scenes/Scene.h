#pragma once
#include "../engine/graphics/window/Window.h"


class Scene{
	public:
		// Called when a scene gets initially created.
		virtual void OnCreate() = 0;

		// Called when a scene is destroyed. 
		virtual void OnDelete() = 0;

		// Called whenever a scene is swapped to. Can be 
		// called many times in a typical game loop.
		virtual void OnActivate() {};

		// Called whenever a scene is switched out of. 
		// Can be called many times in a typical game loop.
		virtual void OnDeactivate() {};

		// The below functions can be overridden as necessary in our scenes.
		virtual void ProcessInput() {};
		virtual void Update(float deltaTime) {};
		virtual void LateUpdate(float deltaTime) {};
		virtual void Draw(Window&) {}
};
