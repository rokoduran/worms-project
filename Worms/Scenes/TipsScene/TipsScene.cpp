#include "TipsScene.h"

TipsScene::TipsScene(SceneManager& sm, ResourceManager<Texture>& tm, Window& win) : m_sm(sm), m_firstIntro(*tm.Get(tm.Add("assets/misc/intro1.png"))), m_secondIntro(*tm.Get(tm.Add("assets/misc/intro2.png"))) {
	const glm::vec2& win_center = win.GetCenter();
	m_firstIntro.Move({ win_center - m_firstIntro.GetCenter() });
	m_secondIntro.Move({ win_center - m_secondIntro.GetCenter() });
}

void TipsScene::ProcessInput() {
	Input::Update();
	if (Input::IsKeyPressed(KeyboardKeys::KEYBOARD_SPACE)) {
		m_firstDone = true;
		if (m_firstDelay <= 0)
			m_secondDone = true;
	}
};
void TipsScene::Update(float dt) {
	if (m_secondDone) {
		m_secondDelay -= dt;
		if (m_secondDelay <= 0.0f)
			m_sm.SwitchTo(2);
	}
	else if (m_firstDone) {
		m_firstDelay -= dt;
	}
}
void TipsScene::Draw(Window& window) {
	window.DrawQuad(m_firstDelay <= 0 ? m_secondIntro : m_firstIntro);
}