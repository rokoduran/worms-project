#pragma once
#include "../Scene.h"
#include "../Worms/Managers/SceneManager/SceneManager.h"
#include "../Worms/Managers/ResourceManager.h"
#include "../engine/graphics/window/Input.h"
#include "../engine/graphics/window/Window.h"
#include "../engine/graphics/drawables/sprite/Sprite.h"

class TipsScene : public Scene {
	private:
		SceneManager& m_sm;
		bool m_firstDone = false, m_secondDone = false;
		float m_firstDelay = 1.0f, m_secondDelay = 2.0f;
		Sprite m_firstIntro, m_secondIntro;
	public:
		TipsScene(SceneManager& sm, ResourceManager<Texture>& tm, Window& win);

		void OnCreate()override {};
		void OnActivate()override {};
		void OnDelete()override {}
		void OnDeactivate()override {};
		void ProcessInput()override;
		void Update(float dt)override;
		void Draw(Window& window)override;
};