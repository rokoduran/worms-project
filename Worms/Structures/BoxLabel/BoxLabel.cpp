#include "BoxLabel.h"

BoxLabel::BoxLabel(Font& font, const glm::vec2& pos, unsigned int color): m_label(font), m_box(color) {
	SetPosition(pos);
}

void BoxLabel::SetPosition(const glm::vec2& pos) {
	m_label.SetPosition(pos);
	m_box.SetPosition(pos);
}

void BoxLabel::SetBoxColor(unsigned int color) {
	m_box.SetColor(color);
}

void BoxLabel::SetLabelColor(unsigned int color) {
	m_label.SetColor(color);
}

void BoxLabel::SetCameraAffected(bool affected) {
	m_box.SetCameraAffected(affected);
	m_label.SetCameraAffected(affected);
}

void BoxLabel::UpdateText(const std::string& text) {
	m_label.UpdateText(text);
	unsigned int size = GetFontSize();
	m_box.SetSize(m_label.GetAdvance(), size);
}

void BoxLabel::Draw(Window& win) {
	win.DrawQuad(m_box);
	win.DrawString(m_label);
}

void BoxLabel::Move(const glm::vec2& pos) {
	m_label.Move(pos);
	m_box.Move(pos);
}