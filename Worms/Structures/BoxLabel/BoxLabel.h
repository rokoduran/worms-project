#pragma once
#include "graphics/drawables/text/Label.h"
#include "graphics/drawables/sprite/Sprite.h"
#include "graphics/window/Window.h"

class BoxLabel {
	private:
		Label m_label;
		Sprite m_box;
	public:
		BoxLabel(Font& font, const glm::vec2& pos,  unsigned int box_color = 0x000000ff);
		void UpdateText(const std::string&);
		void SetBoxColor(unsigned int);
		void SetLabelColor(unsigned int);
		void SetCameraAffected(bool);
		void Draw(Window& win);
		void SetPosition(const glm::vec2&);
		const glm::vec2& GetBoxCenter() {
			return m_box.GetCenter();
		}
		const glm::vec2& GetBoxSize() {
			return m_box.GetSize();
		}
		unsigned int GetFontSize() {
			return m_label.GetFont().GetFontSize();
		}
		const std::string& GetLabelText()const {
			return m_label.GetText();
		}
		void Move(const glm::vec2& pos);
};