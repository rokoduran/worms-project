#pragma once
#include "../Worms/Animation/Animation.h"

enum class Event {
	NoEvent = 0,
	DirectionChanged ,
	VelocityGrounded,
	WormHPChanged,
	TransformationOccured,
	NewTurn,
	TurnOver,
	WeaponEquipped,
	TimerRanOut,
	Victory
};
