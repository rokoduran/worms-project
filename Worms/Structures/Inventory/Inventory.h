#pragma once
#include <map>
#include "../Weapon/SkipGo/SkipGo.h"
#include <memory>
class Inventory {
	private:
		std::map<WeaponTypes, std::shared_ptr<Weapon>> m_weaponMap;
	public:
		Inventory() {
			std::shared_ptr<SkipGo> skipgo = std::make_shared<SkipGo>();
			m_weaponMap[eSkipGo] = skipgo;
		}
		void Add_Weapon(std::shared_ptr<Weapon> wep) {
			WeaponTypes type = wep->GetWeaponType();
			auto  find_it = m_weaponMap.find(wep->GetWeaponType());
			if(find_it == m_weaponMap.end())
				m_weaponMap[type] = wep;
			else {
				m_weaponMap[type]->AddAmmo(wep->GetAmmo());
			}
		}
		WeaponTypes GetNextAvailableWeapon() {
			for (auto& it : m_weaponMap) {
				if (it.second->GetAmmo() != 0)
					return it.second->GetWeaponType();
			}
			return WeaponTypes::eSkipGo;
		}
		int GetAmmo(WeaponTypes wep) {
			auto find_it = m_weaponMap.find(wep);
			if (find_it == m_weaponMap.end() || find_it->second->GetAmmo() == 0)
				return 0;
			return find_it->second->GetAmmo();
		}

		std::shared_ptr<Weapon> GetWeapon(WeaponTypes type) {
			auto wep = m_weaponMap.find(type);
			if (wep == m_weaponMap.end())
				return nullptr;
			return wep->second;
		}

		void Fire(WeaponTypes wep, float angle, float radius, const glm::vec2& source_center, SharedAccess& sa){
			auto  find_it = m_weaponMap.find(wep);
			if (find_it != m_weaponMap.end())
				find_it->second->Fire(angle,radius, source_center, sa);
		}
};