#include "Quadtree.h"

Rect Quadtree::_defaultbounds = { -2400, -200 , 6600, 3300 };

Quadtree::Quadtree() : Quadtree(1, 5, 0, _defaultbounds,nullptr) {}

Quadtree::Quadtree(int maxObjects, int maxLevels, int level,
	Rect bounds, Quadtree* parent)
	: m_maxObjects(maxObjects), m_maxDepth(maxLevels),
	m_depthLevel(level), m_bounds(bounds), m_parent(parent) {}
/*
1.It first needs to check if it has any children nodes. We assume if the first child node is present then all four nodes are because when we split the node we create the four children together.
2. If this node has child nodes then we call a function whose job is to return the index of the node that the object should belong to.
3. If the function returns an index to a child node and not to this node then we call the child nodes insert function and return.
4. At this point, we know that this node has no child nodes so we insert the object into this nodes collection.
5. As we�ve added a new object we need to check if we have exceeded the maximum number of objects allowed in a node.
6. If we exceed the object count limit we need initialise child nodes.
7. And lastly, once we�ve split into child nodes we need to iterate over every object in this node and calculate if it should be in a child node instead.
*/
void Quadtree::Insert(std::shared_ptr<PhysicsComponent> object){
	if (m_children[0] != nullptr) {
		int indexToPlaceObject = GetChildIndexOfObject(object->Get_Rect());

		if (indexToPlaceObject != thistree){
			m_children[indexToPlaceObject]->Insert(object);
			return;
		}
	}

	m_objects.emplace_back(object); 

	if ((m_objects.size() > m_maxObjects) && (m_depthLevel < m_maxDepth) && (m_children[0] == nullptr)) {
		Split(); 

		auto objIterator = m_objects.begin(); 
		while (objIterator != m_objects.end()){
			auto obj = *objIterator;
			int indexToPlaceObject =
				GetChildIndexOfObject(obj->Get_Rect());

			if (indexToPlaceObject != thistree){
				m_children[indexToPlaceObject]->Insert(obj);
				objIterator = m_objects.erase(objIterator);

			}
			else{
				++objIterator;
			}
		}
	}
}

void Quadtree::Remove(std::shared_ptr<PhysicsComponent> object){
	int index = GetChildIndexOfObject(object->Get_Rect());

	if (index == thistree || m_children[index] == nullptr){
		for (unsigned int i = 0; i < m_objects.size(); i++){
			if (m_objects.at(i)->GetMaster()->GetID() == object->GetMaster()->GetID()){
				m_objects.erase(m_objects.begin() + i);
				break;
			}
		}
	}
	else return m_children[index]->Remove(object);
}

std::vector<std::shared_ptr<PhysicsComponent>> Quadtree::FindColliders(const Rect& area){
	std::vector<std::shared_ptr<PhysicsComponent>> possibleOverlaps;
	Search(area, possibleOverlaps); 

	std::vector<std::shared_ptr<PhysicsComponent>> returnList;

	for (auto collider : possibleOverlaps)
		if (area.Intersects(collider->Get_Rect())) 
			returnList.emplace_back(collider); 

	return returnList;
}

void Quadtree::Search(const Rect& area, std::vector<std::shared_ptr<PhysicsComponent>>& overlappingObjects){
	overlappingObjects.insert(overlappingObjects.end(), m_objects.begin(), m_objects.end()); 

	if (m_children[0] != nullptr){
		int index = GetChildIndexOfObject(area); 

		if (index == thistree) {
			for (int i = 0; i < 4; i++)
				if (m_children[i]->GetBounds().Intersects(area))
					m_children[i]->Search(area, overlappingObjects);
		}
		else m_children[index]->Search(area, overlappingObjects);
	}
}

const Rect& Quadtree::GetBounds() const{
	return m_bounds;
}

int Quadtree::GetChildIndexOfObject(const Rect& shape){

	int index = -1;
	const glm::vec2& center = m_bounds.GetCenter();
	const glm::vec2& origin = shape.GetOrigin();
	const glm::vec2& size = shape.GetSize();
	double verticalDividingLine = center.x;
	double horizontalDividingLine = center.y;
	bool north, south, west, east;

	north = origin.y  < horizontalDividingLine && (size.y + origin.y < horizontalDividingLine);
	south = origin.y  > horizontalDividingLine;
	west  = origin.x  < verticalDividingLine && (origin.x + size.x < verticalDividingLine);
	east  = origin.x  > verticalDividingLine;

	if (east) {
		if (north)
			index = NE;
		else if (south)
			index = SE;
	}
	else if (west) {
		if (north)
			index = NW;
		else if (south)
			index = SW;
	}

	return index;
}

void Quadtree::Split(){
	const glm::vec2& size = m_bounds.GetSize();
	const glm::vec2& origin = m_bounds.GetOrigin();
	const float childWidth = size.x / 2;
	const float childHeight = size.y / 2;

	m_children[NE] = std::make_shared<Quadtree>(m_maxObjects, m_maxDepth, m_depthLevel + 1,Rect(origin.x + childWidth, origin.y, childWidth, childHeight),this);
	m_children[NW] = std::make_shared<Quadtree>(m_maxObjects, m_maxDepth, m_depthLevel + 1,Rect(origin.x, origin.y, childWidth, childHeight),this);
	m_children[SW] = std::make_shared<Quadtree>(m_maxObjects, m_maxDepth, m_depthLevel + 1,Rect(origin.x, origin.y + childHeight, childWidth, childHeight),this);
	m_children[SE] = std::make_shared<Quadtree>(m_maxObjects, m_maxDepth, m_depthLevel + 1,Rect(origin.x + childWidth, origin.y + childHeight, childWidth, childHeight),this);
}

void Quadtree::Clear(){
	m_objects.clear();

	for (int i = 0; i < 4; i++){
		if (m_children[i] != nullptr){
			m_children[i]->Clear();
			m_children[i] = nullptr;
		}
	}
}