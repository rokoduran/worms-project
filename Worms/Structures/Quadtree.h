#pragma once
#include <memory>
#include <vector>
#include "util/Shapes/Rectangle.h"
#include "../Components/PhysicsComponent/PhysicsComponent.h"
#include "../Objects/Entity.h"
#include "../engine/util/DebugClass/Debug.h"

class Quadtree{
	private:
		// Indices for the array of children.
		static const int thistree = -1;
		static const int NE = 0;
		static const int NW = 1;
		static const int SW = 2;
		static const int SE = 3;

		unsigned int m_maxObjects;
		unsigned int m_maxDepth;

		//This is the base node.
		Quadtree* m_parent;
		std::shared_ptr<Quadtree> m_children[4];

		// Stores objects in this node.
		std::vector<std::shared_ptr<PhysicsComponent>> m_objects;

		// How deep the current node is from the base node. 
		// The first node starts at 0 and then its child node 	
		// is at level 1 and so on.
		unsigned int m_depthLevel;

		Rect m_bounds;
	public:
		static Rect _defaultbounds;
		Quadtree();
		Quadtree(int maxentities, int maxdepth, int level,
			Rect bounds, Quadtree* parent);
#ifdef DEBUG
		void DrawDebug() {
			if (m_children[0] != nullptr)
				for (unsigned int i = 0; i < 4; i++)
					m_children[i]->DrawDebug();
			Debug::DrawRect(m_bounds);
		};
#endif
		void Insert(std::shared_ptr<PhysicsComponent> object);

		void Remove(std::shared_ptr<PhysicsComponent> object);

		void Clear();

		std::vector<std::shared_ptr<PhysicsComponent>> FindColliders(const Rect& area);

		const Rect& GetBounds() const;
	private:
		void Search(const Rect& area,
			std::vector<std::shared_ptr<PhysicsComponent>>&
			overlappingentities);

		// Returns the index for the node that will contain 		
		// the object. -1 is returned if it is this node.
		int GetChildIndexOfObject(const Rect& objectBounds);

		// Creates the child nodes.
		void Split();
};