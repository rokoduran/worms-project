#include "Team.h"
#include "../Worms/Objects/Entity.h"
Team::Team(unsigned int color, const std::string& teamname) : m_color(color), m_teamName(teamname), m_currentWormIndex(-1) {};

void Team::AddWorm(std::shared_ptr<Entity> worm) {
	m_worms.push_back(worm);
}

std::shared_ptr<Entity> Team::NextWorm() {
	m_currentWormIndex + 1 >= m_worms.size() ? m_currentWormIndex = 0 : ++m_currentWormIndex;
	return m_worms[m_currentWormIndex];
}

unsigned int Team::GetTeamSize() {
	return m_worms.size();
}

bool Team::WormDied(unsigned int dead_worm_id) {
	for (unsigned int i = 0; i < m_worms.size(); i++) {
		if (m_worms[i]->GetID() == dead_worm_id) {
			m_worms[i]->SetForRemoval();
			if (i <= m_currentWormIndex)
				--m_currentWormIndex;
			m_worms.erase(m_worms.begin()+i);
			return true;
		}
	}
	return false;
}