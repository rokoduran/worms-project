#pragma once
#include <vector>
#include <string>
#include <memory>

class Entity;

class Team {
	private:
		unsigned int m_color;
		std::string m_teamName;
		unsigned int m_currentWormIndex;
		std::vector<std::shared_ptr<Entity>> m_worms;
	public:
		Team(unsigned int color, const std::string& teamname);
		void AddWorm(std::shared_ptr <Entity>);
		bool WormDied(unsigned int dead_worm_id);
		unsigned int GetTeamSize();
		unsigned int GetTeamColor() {
			return m_color;
		}
		const std::string& GetTeamName() {
			return m_teamName;
		}
		std::shared_ptr<Entity> NextWorm();

		std::vector<std::shared_ptr<Entity>>& GetAllWormsInTeam() {
			return m_worms;
		}
};