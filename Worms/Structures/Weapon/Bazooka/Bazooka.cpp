#include "Bazooka.h"
#include "../Worms/Components/SharedAccess.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/PhysicsComponent/Projectile/ProjectilePhysicsComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityController/VelocityControllerComponent.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"

Bazooka::Bazooka(unsigned int ammo, bool infinite) : Weapon(eBazooka,"Bazooka", ammo, infinite) {};

void Bazooka::Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa) {
       	std::string path = "assets/Weapons/Bazooka/Bazooka_Projectile.png";
		std::shared_ptr<Entity> zooka = std::make_shared<Entity>(sa);
		auto sprite = zooka->AddComponent<SpriteComponent>();
		sprite->Load(path);
		sprite->SetDrawLayer(DrawLayer::Entity);
		auto phys = zooka->AddComponent<ProjectilePhysicsComponent>();
		phys->SetDamage(50);
		phys->SetRect({ 8.0f,8.0f });
		phys->SetOffset({ 8.0f,8.0f });
		phys->SetDestructionDimensions({ 128.0f,128.0f });
		const glm::vec2 point = { source_center.x+radius*cos(angle),source_center.y+radius*sin(angle) };
		auto velocity = zooka->AddComponent<VelocityComponent>();
		auto vel_control = zooka->AddComponent<VelocityControllerComponent>();
		glm::vec2 velocity_value;
		angle = angle * 180 / M_PI;
		if (angle < 0)
			angle = 360 + angle;
		SetupVelocityForAngle(velocity_value, angle, 1000.0f);
        velocity->SetVelocity(velocity_value);
		velocity->SetWindAffected(true);
		zooka->transform->SetPosition(point.x - 8.0f,point.y - 8.0f);
		zooka->transform->SetRotation(angle);
		sa.entities.Add(zooka);
		sa.turnSystem.EndTurn();
		if(!m_infiniteAmmo)
			m_ammo--;
		sa.soundManager.Get(sa.soundManager.Add("assets/sounds/shoot.wav"))->Play();
};