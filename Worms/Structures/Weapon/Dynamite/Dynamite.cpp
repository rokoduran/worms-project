#include "Dynamite.h"
#include "../Worms/Components/SharedAccess.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/PhysicsComponent/Projectile/MortarPhysicsComponent/MortarPhysicsComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../Worms/Components/PhysicsComponent/Thrown/ThrownPhysicsComponent.h"
#include "../Worms/Components/TimerComponent/TimerComponent.h"

Dynamite::Dynamite(int ammo, bool infinite) : Weapon(eDynamite, "Dynamite", ammo, infinite) {};
void Dynamite::Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa) {
	std::string path = "assets/Weapons/Dynamite/Dynamite_base.png";
	std::shared_ptr<Entity> grenade = std::make_shared<Entity>(sa);
	auto sprite = grenade->AddComponent<SpriteComponent>();
	sprite->Load(path);
	sprite->SetDrawLayer(DrawLayer::Entity);
	auto phys = grenade->AddComponent<ThrownPhysicsComponent>();
	phys->SetDamage(80);
	phys->SetRect({ 14.0f,14.0f });
	phys->SetOffset({ 8.0f,8.0f });
	phys->SetBounce(0.15f);
	phys->SetDestructionDimensions({ 256.0f,256.0f });
	const glm::vec2 point = { source_center.x + radius * cos(angle),source_center.y + radius * sin(angle) };
	auto velocity = grenade->AddComponent<VelocityComponent>();
	auto timer = grenade->AddComponent<TimerComponent>();
	timer->SetMaxTime((float)_fuse);
	glm::vec2 velocity_value;
	angle = angle * 180 / M_PI;
	if (angle < 0)
		angle = 360 + angle;
	SetupVelocityForAngle(velocity_value, angle, 600.0f);
	velocity->SetVelocity(velocity_value);
	grenade->transform->SetPosition(point.x - 8.0f, point.y - 8.0f);
	sa.entities.Add(grenade);
	sa.turnSystem.EndTurn();
	if (!m_infiniteAmmo)
		m_ammo--;
	sa.soundManager.Get(sa.soundManager.Add("assets/sounds/throw.wav"))->Play();
};