#pragma once
#include "../Weapon.h"

class Dynamite : public Weapon {
	private:
		int _fuse = 5;
	public:
		Dynamite(int ammo, bool infinite);
		void Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa)override;
};