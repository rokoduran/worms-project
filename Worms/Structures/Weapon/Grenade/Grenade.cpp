#include "Grenade.h"
#include "../Worms/Components/SharedAccess.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/PhysicsComponent/Projectile/MortarPhysicsComponent/MortarPhysicsComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../Worms/Components/PhysicsComponent/Thrown/ThrownPhysicsComponent.h"
#include "../Worms/Components/TimerComponent/TimerComponent.h"

Grenade::Grenade(int ammo, bool infinite) : Weapon(eGrenade,"Grenade", ammo, infinite), _fuse(3) {};
void Grenade::Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa) {
	std::string path = "assets/Weapons/Grenade/Grenade.png";
	std::shared_ptr<Entity> grenade = std::make_shared<Entity>(sa);
	auto sprite = grenade->AddComponent<SpriteComponent>();
	sprite->Load(path);
	sprite->SetDrawLayer(DrawLayer::Entity);
	auto phys = grenade->AddComponent<ThrownPhysicsComponent>();
	phys->SetDamage(55);
	phys->SetRect({ 14.0f,14.0f });
	phys->SetOffset({ 6.0f,6.0f });
	phys->SetBounce(0.3f);
	phys->SetDestructionDimensions({ 128.0f,128.0f });
	const glm::vec2 point = { source_center.x + radius * cos(angle),source_center.y + radius * sin(angle) };
	auto velocity = grenade->AddComponent<VelocityComponent>();
	auto timer = grenade->AddComponent<TimerComponent>();
	timer->SetMaxTime((float)_fuse);
	glm::vec2 velocity_value;
	angle = angle * 180 / M_PI;
	if (angle < 0)
		angle = 360 + angle;
	SetupVelocityForAngle(velocity_value, angle, 800.0f);
	velocity->SetVelocity(velocity_value);
	grenade->transform->SetPosition(point.x - 8.0f, point.y - 8.0f);
	sa.entities.Add(grenade);
	sa.turnSystem.EndTurn();
	if (!m_infiniteAmmo)
		m_ammo--;
	sa.soundManager.Get(sa.soundManager.Add("assets/sounds/throw.wav"))->Play();
};