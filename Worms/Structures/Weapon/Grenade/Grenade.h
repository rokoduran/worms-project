#pragma once
#include "../Weapon.h"

class Grenade : public Weapon {
	private:
		int _fuse;
	public:	
		Grenade(int ammo, bool infinite);
		void Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa)override;
		void Set_Fuse(int fuse) {
			_fuse = fuse;
		}
};