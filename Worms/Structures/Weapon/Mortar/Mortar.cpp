#include "Mortar.h"
#include "../Worms/Components/SharedAccess.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/PhysicsComponent/Projectile/MortarPhysicsComponent/MortarPhysicsComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityController/VelocityControllerComponent.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"

Mortar::Mortar(int ammo, bool infinite) : Weapon(eMortar, "Mortar", ammo, infinite) {};
void Mortar::Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa) {
	std::string path = "assets/Weapons/Mortar/Mortar_shell.png";
	std::shared_ptr<Entity> mortar = std::make_shared<Entity>(sa);
	auto sprite = mortar->AddComponent<SpriteComponent>();
	sprite->Load(path);
	sprite->SetDrawLayer(DrawLayer::Entity);
	auto phys = mortar->AddComponent<MortarPhysicsComponent>();
	phys->SetDamage(30);
	phys->SetRect({ 8.0f,8.0f });
	phys->SetOffset({ 8.0f,8.0f });
	phys->SetDestructionDimensions({ 128.0f,128.0f });
	const glm::vec2 point = { source_center.x + radius * cos(angle),source_center.y + radius * sin(angle) };
	auto velocity = mortar->AddComponent<VelocityComponent>();
	auto vel_control = mortar->AddComponent<VelocityControllerComponent>();
	glm::vec2 velocity_value;
	angle = angle * 180 / M_PI;
	if (angle < 0)
		angle = 360 + angle;
	SetupVelocityForAngle(velocity_value, angle, 1000.0f);
	velocity->SetVelocity(velocity_value);
	mortar->transform->SetPosition(point.x - 8.0f, point.y - 8.0f);
	mortar->transform->SetRotation(angle);
	sa.entities.Add(mortar);
	sa.turnSystem.EndTurn();
	if (!m_infiniteAmmo)
		m_ammo--;
	sa.soundManager.Get(sa.soundManager.Add("assets/sounds/shoot.wav"))->Play();
};