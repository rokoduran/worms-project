#pragma once
#include "../Weapon.h"

class Mortar : public Weapon {
	public:
		Mortar(int ammo, bool infinite);
		void Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa)override;
};