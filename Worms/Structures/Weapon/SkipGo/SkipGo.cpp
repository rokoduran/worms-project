#include "SkipGo.h"
#include "../Worms/Systems/TurnSystem/TurnSystem.h"
#include "../Worms/Components/SharedAccess.h"

SkipGo::SkipGo() : Weapon(eSkipGo,"SkipGo", 0, true) {};
void SkipGo::Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa) {
	sa.turnSystem.EndTurn();
	if (!m_infiniteAmmo)
		m_ammo--;
}