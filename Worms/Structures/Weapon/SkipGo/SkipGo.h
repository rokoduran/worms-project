#pragma once
#include "../Weapon.h"

class SkipGo : public Weapon {
	public:
		SkipGo();
		void Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa)override;
};