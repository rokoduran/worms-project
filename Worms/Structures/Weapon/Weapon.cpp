#include "Weapon.h"

Weapon::Weapon(WeaponTypes type, std::string name ,int ammo, bool infinite) : m_type(type),m_name(name), m_ammo(ammo), m_infiniteAmmo(infinite) {};
void Weapon::AddAmmo(unsigned int ammo) {
	if (m_infiniteAmmo)
		return;
	m_ammo += ammo;
}
int Weapon::GetAmmo() {
	if (m_infiniteAmmo)
		return -1;
	return m_ammo;
}
WeaponTypes Weapon::GetWeaponType() {
	return m_type;
}

void Weapon::SetupVelocityForAngle(glm::vec2& velocity, float angle, float multiplier) {
	if (angle < 0)
		angle = 360 + angle;

	if (angle <= 90.0f) {
		velocity.x = (1 - (angle / 90.0f)) * multiplier;
		velocity.y = (angle / 90.0f)* multiplier;
	}
	else if (angle >= 270.0f) {
		velocity.x = ((angle - 270.0f) / 90.0f) * multiplier;
		velocity.y = -(1 - ((angle - 270.0f) / 90.0f))* multiplier;
	}
	else if (angle > 90.0f && angle <= 180.0f) {
		velocity.x = -((angle - 90.0f) / 90.0f) * multiplier;
		velocity.y = (1 - ((angle - 90.0f) / 90.0f))* multiplier;
	}
	else if (angle > 180.0f && angle < 270.0f) {
		velocity.x = -(1 - ((angle - 180.0f) / 90.0f)) * multiplier;
		velocity.y = -((angle - 180.0f) / 90.0f)* multiplier;
	}
}
