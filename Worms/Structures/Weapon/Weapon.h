#pragma once
#include "../engine/util/Shapes/Rectangle.h"
#include <string>
struct SharedAccess;

enum WeaponTypes {
	eNone = 0,
	eBazooka,
	eGrenade,
	eDynamite,
	eMortar,
	eSkipGo
};


class Weapon {
	protected:
		WeaponTypes m_type;
		std::string m_name;
		int m_ammo;
		bool m_infiniteAmmo;
	public:
		Weapon(WeaponTypes type, std::string name, int ammo=1, bool infinite = false);
		void AddAmmo(unsigned int ammo);
		int GetAmmo();
		WeaponTypes GetWeaponType();
		const std::string& GetWeaponName() {
			return m_name;
		}
		//float shoot_angle, Rect& source_body, SharedAccess& entity_creation
		virtual void Fire(float angle, float radius, const glm::vec2& source_center, SharedAccess& sa) = 0;
	protected:
		void SetupVelocityForAngle(glm::vec2& velocity, float angle, float multiplier);


};