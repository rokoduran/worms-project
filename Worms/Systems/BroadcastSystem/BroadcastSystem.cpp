#include "BroadcastSystem.h"
#include <memory>
#include <iterator>

BroadcastSystem::BroadcastSystem(Font& font, float max_time, const glm::vec2& size) : m_font(font), m_timeLeft(0.0f), m_maxTime(max_time), m_screenCoords(size) {

};
void BroadcastSystem::PlayAnnouncement(const std::string& text) {
	ClearCurrentAnnouncements();
	Label label(m_font);
	label.UpdateText(text);
	unsigned int number_of_repeats = ceil(label.GetAdvance()/m_screenCoords.x);
	unsigned int starting_y = m_screenCoords.y - m_font.GetFontSize();
	unsigned int string_offset = text.size() / number_of_repeats;

	for (unsigned int i = 0; i < number_of_repeats; i++) {
		BoxLabel* boxlabel = new BoxLabel(m_font, m_screenCoords, 0x000000ff);
		boxlabel->SetCameraAffected(false);
		boxlabel->SetLabelColor(0xffffffff);
		std::string substr = text.substr(i*string_offset, string_offset);
		boxlabel->UpdateText(substr);
		boxlabel->SetPosition({ m_screenCoords.x,starting_y });
		float center_diff = m_screenCoords.x/2 - boxlabel->GetBoxCenter().x;
		boxlabel->Move({ center_diff ,0.0f});
		m_announcements.push_back(boxlabel);
		starting_y -= m_font.GetFontSize();
	}

};
void BroadcastSystem::Update(float dt) {
	if (!m_announcements.empty()) {
		m_timeLeft - dt <= 0.0f ? m_timeLeft = 0.0f : m_timeLeft -= dt;
		if (m_timeLeft == 0.0f) 
			ClearCurrentAnnouncements();
	}
	
};

void BroadcastSystem::ClearCurrentAnnouncements() {
	for (auto it = m_announcements.begin(); it != m_announcements.end();) {
		delete *it;
		it = m_announcements.erase(it);
	}
	m_timeLeft = m_maxTime;
}

void BroadcastSystem::Draw(Window& window) {
	for (unsigned int i = 0; i < m_announcements.size(); i++)
		m_announcements[i]->Draw(window);
};