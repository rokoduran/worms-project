#pragma once
#include "../Worms/Structures/BoxLabel/BoxLabel.h"

class BroadcastSystem {
	private:
		Font& m_font;
		std::vector<BoxLabel*> m_announcements;
		float m_timeLeft;
		float m_maxTime;
		glm::vec2 m_screenCoords;
	public:
		BroadcastSystem(Font& font, float max_time, const glm::vec2& screen_coords);
		void PlayAnnouncement(const std::string& text);
		void Update(float dt);
		void Draw(Window& window);
	private:
		void ClearCurrentAnnouncements();
};