#include "CrateDropSystem.h"
#include "../Worms/Structures/Weapon/Bazooka/Bazooka.h"
#include "../Worms/Structures/Weapon/Grenade/Grenade.h"
#include "../Worms/Structures/Weapon/Mortar/Mortar.h"
#include "../Worms/Structures/Weapon/Dynamite/Dynamite.h"
#include "../Worms/Components/SpriteComponent/SpriteComponent.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"
#include "../Worms/Components/AnimationComponent/AnimationComponent.h"
#include "../Worms/Components/PhysicsComponent/Crate/HealthCratePhysicsComponent/HealthCratePhysicsComponent.h"
#include "../Worms/Components/PhysicsComponent/Crate/WeaponCratePhysicsComponent/WeaponCratePhysicsComponent.h"
#include "../Worms/Managers/ObjectManager/EntityManager.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"

CrateDropSystem::CrateDropSystem(float weapon_chance, float hp_chance, SharedAccess& sa, int hp_value) : m_sa(sa), m_weaponChance(weapon_chance), m_hpChance(hp_chance), m_hpValue(hp_value), m_mapLimits(0.0f, 0.0f) {};

std::shared_ptr<Weapon> CrateDropSystem::RollWeapon() {
	WeaponTypes type = (WeaponTypes)glm::linearRand((int)WeaponTypes::eBazooka, (int)WeaponTypes::eDynamite);
	switch (type) {
		case(WeaponTypes::eBazooka): {
			std::shared_ptr<Bazooka> zooka = std::make_shared<Bazooka>(3, false);
			return zooka;
		}
		case(WeaponTypes::eGrenade): {
			std::shared_ptr<Grenade> grenade = std::make_shared<Grenade>(3, false);
			return grenade;
		}
		case(WeaponTypes::eMortar): {
			std::shared_ptr<Mortar> mortar = std::make_shared<Mortar>(1, false);
			return mortar;
		}
		case(WeaponTypes::eDynamite): {
			std::shared_ptr<Dynamite> dynamite = std::make_shared<Dynamite>(1, false);
			return dynamite;
		}
	}
}

void CrateDropSystem::DropCrate() {
	float roll1 = glm::linearRand(0.0f, 1.0f);
	std::string path = "assets/Crates/crates.png";
	if (roll1 <= m_weaponChance) {
		std::shared_ptr<Entity> entity = std::make_shared<Entity>(m_sa);
		auto sprite = entity->AddComponent<SpriteComponent>();
		sprite->SetDrawLayer(DrawLayer::Entity);
		auto velocity = entity->AddComponent<VelocityComponent>();
		velocity->SetGrounded(false);
		auto phys = entity->AddComponent<WeaponCratePhysicsComponent>();
		phys->SetOffset({ 13.0f, 16.0f });
		phys->SetRect({ 36.0f, 38.0f });
		phys->SetWeapon(RollWeapon());
		glm::vec2 spawn_pos{ glm::linearRand(0.0f, m_mapLimits.x) , m_mapLimits.y + 100.0f };
		std::shared_ptr<Texture> tex = m_sa.texManager.Get(m_sa.texManager.Add(path));
		glm::vec2 tex_size = glm::vec2(tex->GetWidth(), tex->GetHeight());
		auto anime = entity->AddComponent<AnimationComponent>();
		auto animation = Animation::CreateAnimation(7, m_sa.texManager.Add(path), tex_size, { 0.0f,1.0f }, true, { 64.0f,64.0f }, 0.15f);
		animation->SetLooped(true);
		anime->SetAnimation(animation);
		entity->transform->SetPosition(spawn_pos.x, spawn_pos.y);
		m_sa.entities.Add(entity);
		return;
	}
	float roll2 = glm::linearRand(0.0f, 1.0f);
	if (roll2 <= m_hpChance) {
		std::shared_ptr<Entity> entity = std::make_shared<Entity>(m_sa);
		auto sprite = entity->AddComponent<SpriteComponent>();
		sprite->Load(path);
		sprite->SetDrawLayer(DrawLayer::Entity);
		auto velocity = entity->AddComponent<VelocityComponent>();
		velocity->SetGrounded(false);
		auto phys = entity->AddComponent<HealthCratePhysicsComponent>();
		phys->SetOffset({ 11.0f, 15.0f });
		phys->SetRect({ 45.0f, 35.0f });
		phys->SetHP(m_hpValue);
		glm::vec2 spawn_pos{ glm::linearRand(0.0f, m_mapLimits.x) , m_mapLimits.y + 100.0f };
		std::shared_ptr<Texture> tex = m_sa.texManager.Get(m_sa.texManager.Add(path));
		glm::vec2 tex_size = glm::vec2(tex->GetWidth(), tex->GetHeight());
		auto anime = entity->AddComponent<AnimationComponent>();
		auto animation = Animation::CreateAnimation(7, m_sa.texManager.Add(path), tex_size, { 0.0f,0.0f }, true, { 64.0f,64.0f }, 0.15f);
		animation->SetLooped(true);
		anime->SetAnimation(animation);
		entity->transform->SetPosition(spawn_pos.x, spawn_pos.y);
		m_sa.entities.Add(entity);
		return;
	}
};