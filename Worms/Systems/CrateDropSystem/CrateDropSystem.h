#pragma once
#include "../Worms/Objects/Entity.h"
#include "../Worms/Structures/Weapon/Weapon.h"
class CrateDropSystem {
	private:
		SharedAccess& m_sa;
		glm::vec2 m_mapLimits;
		float m_weaponChance, m_hpChance;
		int m_hpValue;
	public:
		CrateDropSystem(float weapon_chance, float hp_chance, SharedAccess& _sa, int hp_value = 25);
		void Set_Map_Limits(const glm::vec2& map_limits) {
			m_mapLimits = map_limits;
		}
		void DropCrate();
		std::shared_ptr<Weapon> RollWeapon();
};