#include "DrawSystem.h"
#include <algorithm>

void DrawSystem::Add(const std::vector<std::shared_ptr<Entity>>& entities) {
	for (auto& e : entities)
		Add(e);
}

void DrawSystem::Deactivate() {
	m_drawableEntities.clear();
}

void DrawSystem::Add(std::shared_ptr<Entity> entity){

	std::shared_ptr<DrawComponent> draw = entity->GetComponent<DrawComponent>();

	if (draw){
		DrawLayer layer = draw->GetDrawLayer();

		auto itr = m_drawableEntities.find(layer);

		if (itr != m_drawableEntities.end())
			m_drawableEntities[layer].push_back(draw);
		else{
			std::vector<std::shared_ptr<DrawComponent>> draws;
			draws.push_back(draw);

			m_drawableEntities.insert(std::make_pair(layer, draws));
		}
	}
}

void DrawSystem::Draw(Window& win) {
	Sort();
	for (auto& layer : m_drawableEntities)
		for (auto& drawable : layer.second)
			drawable->Draw(win);
}

bool DrawSystem::LayerSort(std::shared_ptr<DrawComponent> a, std::shared_ptr<DrawComponent> b)
{
	return a->GetOrder() < b->GetOrder();
}

void DrawSystem::Sort(){
	for (auto& layer : m_drawableEntities) {
		if (!std::is_sorted(layer.second.begin(), layer.second.end(), LayerSort))
			std::sort(layer.second.begin(), layer.second.end(), LayerSort);
	}
}

//Method from EntityManager
void DrawSystem::HandleDeletion() {
	for (auto& layer : m_drawableEntities){
		auto objIterator = layer.second.begin();
		while (objIterator != layer.second.end()){
			auto obj = *objIterator;
			objIterator = obj->ShouldStopDrawing()? layer.second.erase(objIterator) : ++objIterator;
		}
	}
}