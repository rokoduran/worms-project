#pragma once
#include "../Worms/Components/DrawComponent/DrawComponent.h"
#include "../Worms/Objects/Entity.h"
#include <map>

class DrawSystem{
	private:
		std::map<DrawLayer,std::vector<std::shared_ptr<DrawComponent>>> m_drawableEntities;
	public:
		void Add(const std::vector <std::shared_ptr<Entity>>&);
		void Add(std::shared_ptr<Entity> object);

		void HandleDeletion();

		void Draw(Window& window);

		void Deactivate();
	private:
		//Static to avoid the first parameter being this
		static bool LayerSort(std::shared_ptr<DrawComponent> a, std::shared_ptr<DrawComponent> b);
		void Sort();
};