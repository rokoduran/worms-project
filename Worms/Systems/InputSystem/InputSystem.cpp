#include "InputSystem.h"


std::shared_ptr<Entity> InputSystem::activeWorm = nullptr;

void InputSystem::AttachCallback(KeyboardKeys key, Callback fun) {
	if (m_keyActionMap.find(key) != m_keyActionMap.end())
		RemoveKeyAction(key);
	m_keyActionMap.insert(std::make_pair(key, fun));
};
void InputSystem::RemoveKeyAction(KeyboardKeys key) {
	if(m_keyActionMap.find(key)!= m_keyActionMap.end())
		m_keyActionMap.erase(key);
};

void InputSystem::RemoveAllActions() {
	m_keyActionMap.clear();
	activeWorm = nullptr;
}
void InputSystem::Update(float dt) {
	if(!m_inputDisabled){
		for (auto it = m_keyActionMap.begin(); it != m_keyActionMap.end(); ++it) {
			if (Input::IsKeyPressed(it->first)) {
				it->second(dt);
			}
		}
	}
};

std::shared_ptr<Entity>& InputSystem::GetActiveWorm() {
	return activeWorm;
}