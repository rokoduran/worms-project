#pragma once
#include "../engine/graphics/window/Input.h"
#include "../Worms/Objects/Entity.h"
#include <map>
#include <functional>

typedef std::function<void(float)> Callback;
class InputSystem {
	private:
		friend class TurnSystem;
		bool m_inputDisabled = true;
		std::map<KeyboardKeys, Callback> m_keyActionMap;
		static std::shared_ptr<Entity> activeWorm;
	public:
		void AttachCallback(KeyboardKeys, Callback);
		void RemoveKeyAction(KeyboardKeys);
		void Update(float dt);
		static std::shared_ptr<Entity>& GetActiveWorm();
	private:
		void RemoveAllActions();
};