#include "PhysicsSystem.h"

PhysicsSystem::PhysicsSystem() {
	Bitmask defaultcollision;
	m_collisionLayers.insert(std::make_pair(CollisionLayer::None, defaultcollision));

	Bitmask player_collisions;
	player_collisions.Set_Bit((int)CollisionLayer::Map);
	player_collisions.Set_Bit((int)CollisionLayer::Entity);
	player_collisions.Set_Bit((int)CollisionLayer::Water);
	player_collisions.Set_Bit((int)CollisionLayer::Destruction);
	player_collisions.Set_Bit((int)CollisionLayer::Crate);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Entity, player_collisions));
	Bitmask destruction_collision;
	destruction_collision.Set_Bit((int)CollisionLayer::Map);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Destruction,destruction_collision));
	Bitmask crate_collision;
	crate_collision.Set_Bit((int)CollisionLayer::Map);
	crate_collision.Set_Bit((int)CollisionLayer::Entity);
	crate_collision.Set_Bit((int)CollisionLayer::Destruction);
	crate_collision.Set_Bit((int)CollisionLayer::Water);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Crate, crate_collision));
	Bitmask map_collision;
	map_collision.Set_Bit((int)CollisionLayer::None);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Map,map_collision));
	Bitmask water_collision;
	water_collision.Set_Bit((int)CollisionLayer::None);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Water, water_collision));
	Bitmask projectile_collision;
	projectile_collision.Set_Bit((int)CollisionLayer::Map);
	projectile_collision.Set_Bit((int)CollisionLayer::Entity);
	projectile_collision.Set_Bit((int)CollisionLayer::Crate);
	projectile_collision.Set_Bit((int)CollisionLayer::Water);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Projectile, projectile_collision));

	Bitmask thrown_collision;
	thrown_collision.Set_Bit((int)CollisionLayer::Entity);
	thrown_collision.Set_Bit((int)CollisionLayer::Map);
	thrown_collision.Set_Bit((int)CollisionLayer::Crate);
	thrown_collision.Set_Bit((int)CollisionLayer::Water);
	m_collisionLayers.insert(std::make_pair(CollisionLayer::Thrown, thrown_collision));

}

void PhysicsSystem::Add(std::vector<std::shared_ptr<Entity>>& objects) {
	for (auto& o : objects) {
		auto collider = o->GetComponent<PhysicsComponent>();
		if (collider) {
			CollisionLayer layer = collider->GetLayer();

			auto itr = m_collidables.find(layer);

			if (itr != m_collidables.end())
				m_collidables[layer].push_back(collider);

			else {
				std::vector<std::shared_ptr<PhysicsComponent>> objs;
				objs.push_back(collider);
				m_collidables.insert(std::make_pair(layer, objs));
			}
		}
	}
}

void PhysicsSystem::Deactivate() {
	m_collisionTree.Clear();
	m_collidables.clear();
}

void PhysicsSystem::ProcessRemovals() {
	for (auto& layer : m_collidables) {
		auto itr = layer.second.begin();
		while (itr != layer.second.end())
			(*itr)->GetMaster()->ShouldBeRemoved() ? itr = layer.second.erase(itr) : ++itr;
	}
}

void PhysicsSystem::Update() {
#ifdef DEBUG
	m_collisionTree.DrawDebug();
#endif
	m_collisionTree.Clear();
	for (auto maps = m_collidables.begin(); maps != m_collidables.end(); ++maps) {
		for (auto& collidable : maps->second)
			m_collisionTree.Insert(collidable);
	}
	Resolve();
}

void PhysicsSystem::Resolve() {
	for (auto maps = m_collidables.begin(); maps != m_collidables.end(); ++maps) {
		// If this layer collides with nothing then there is no need to perform any further checks.
		if (m_collisionLayers[maps->first].Get_Mask() == 0)
			continue;

		for (auto& collidable : maps->second) {

			collidable->ResolveActive();
			if (collidable->GetLayer() == CollisionLayer::Map) 
				continue;

			std::vector<std::shared_ptr<PhysicsComponent>> collisions
				= m_collisionTree.FindColliders(collidable->Get_Rect()); 

			for (auto& collision : collisions) {
				// Make sure we do not resolve collisions with the same object.
				if (collidable->GetMaster()->GetID() == collision->GetMaster()->GetID())
					continue;	

				bool layersCollide = m_collisionLayers[collidable->GetLayer()].Get_Bit(((int)collision->GetLayer()));

				if (layersCollide) {

					if (collidable->Intersects(collision)) {
#ifdef DEBUG
							Debug::DrawRect((const Rect&)collision->Get_Rect(), 2, 0x0000ffff);
							Debug::DrawRect((const Rect&)collidable->Get_Rect(), 2, 0x00ff00ff);
#endif
						if (collision->GetLayer() == CollisionLayer::Map)
							collision->Intersects(collidable);
						else
							collidable->ResolveOverlap(collision); 
					}
				}
			}

		}
	}
}

bool PhysicsSystem::NothingActive() {
	for (auto maps = m_collidables.begin(); maps != m_collidables.end(); ++maps) {
		for (auto& collidable : maps->second) {
			if (collidable->GetActivity())
				return 0;
		}
	}
	return true;
}