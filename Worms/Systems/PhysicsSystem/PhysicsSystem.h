#pragma once
#include "../Worms/Objects/Entity.h"
#include "../Worms/Structures/Quadtree.h"
#include <vector>
#include <memory>
#include <map>
#include "util/defines.h"

class PhysicsSystem {
private:
	// This is used to store collision layer data i.e. which layers can collide.
	std::map<CollisionLayer, Bitmask> m_collisionLayers;

	// The collision system stores all collidables along with their layer.
	std::map<CollisionLayer, std::vector<std::shared_ptr<PhysicsComponent>>> m_collidables;

	Quadtree m_collisionTree;
public:
	PhysicsSystem();
	void Add(std::vector<std::shared_ptr<Entity>>& objects);
	void ProcessRemovals();
	void Update();
	void Deactivate();
	bool NothingActive();
private:
	void Resolve();
};