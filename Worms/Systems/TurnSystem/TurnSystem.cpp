#include "TurnSystem.h"
#undef min
#undef max
#include "math/glm/gtc/random.hpp"
#include "util/defines.h"
#include "../Worms/Components/VelocityComponent/VelocityComponent.h"

TurnSystem::TurnSystem(bool& game_end,float turn_time, float time_between_turns, Font& font, const glm::vec2& window_dimensions, unsigned int color) : m_gameEnd(game_end), m_turnTime(turn_time), m_currentTurnTime(turn_time),
m_maxTimeBetweenTurns(time_between_turns), m_currentTimeBetweenTurns(0.0f), m_broadcastSystem(font, time_between_turns, window_dimensions),
m_timeBox(font, { 50.0f, 50.0f }, color), m_windowDimensions(window_dimensions), m_windBox(font, { window_dimensions.x - 200.0f, 100.0f }, color), m_turnOver(false) {
	font.SetSize(48);

}

void TurnSystem::AddTeam(std::shared_ptr<Team> new_team) {
	m_teams.push_back(new_team);
}

void TurnSystem::Start() {
	m_currentTimeBetweenTurns = 0.0f;
	m_windBox.SetCameraAffected(false);
	m_windBox.SetBoxColor(0x000000ff);
	m_windBox.SetLabelColor(0xffffffff);
	m_timeBox.SetCameraAffected(false);
	m_timeBox.SetBoxColor(0x000000ff);
	m_timeBox.SetLabelColor(0xffffffff);
	m_currentTeamIndex = glm::linearRand((unsigned int)0, (unsigned int)m_teams.size() - 1);
	NewTurn();
}

void TurnSystem::Deactivate() {
	m_input.RemoveAllActions();
	m_teams.clear();
}

void TurnSystem::Update(float dt) {
	m_broadcastSystem.Update(dt);
	if(!m_turnOver){
		m_currentTurnTime -= dt;
		if (m_currentTurnTime <= 0.0f){
			m_currentTurnTime = 0.0f;
			EndTurn();
			return;
		}
		m_timeBox.UpdateText(NumberToString<int>(m_currentTurnTime));
		m_input.Update(dt);
	}
}

void TurnSystem::WormDrowned(unsigned int id) {
	RemoveWorm(id);
}

void TurnSystem::RemoveWorm(unsigned int dead_worm_id) {
	if (m_input.activeWorm!=nullptr && m_input.activeWorm->GetID() == dead_worm_id) {
		EndTurn();
		m_input.activeWorm = nullptr;
	}
	for (unsigned int i = 0; i < m_teams.size(); i++) {
		if (m_teams[i]->WormDied(dead_worm_id)) {
			if(m_teams[i]->GetTeamSize()==0){
				std::string teamDeadMsg = "Team " + m_teams[i]->GetTeamName() + " bites the dust!";
				m_broadcastSystem.PlayAnnouncement(teamDeadMsg);
				if (i < m_currentTeamIndex)
					--m_currentTeamIndex;
				m_teams.erase(m_teams.begin() + i);
			}
			return;
		}
	}
	return;
}

void TurnSystem::WormDied(unsigned int id) {
	m_deadWorms.insert(id);
}

std::shared_ptr<Entity> TurnSystem::NextWorm() {

	m_currentTeamIndex + 1 >= m_teams.size() ? m_currentTeamIndex = 0 : ++m_currentTeamIndex;
	return m_teams[m_currentTeamIndex]->NextWorm();
}

void TurnSystem::Draw(Window& win) {
	m_timeBox.Draw(win);
	m_windBox.Draw(win);
	m_broadcastSystem.Draw(win);
}

bool TurnSystem::UpdateTimeBetween(float dt) {
	m_currentTimeBetweenTurns -= dt;
	if (m_currentTimeBetweenTurns <= 0.0f) {
		m_currentTimeBetweenTurns = 0.0f;
		return true;
	}
	return false;
}

void TurnSystem::NewTurn() {
	for (auto it : m_deadWorms) 
		RemoveWorm(it);
	m_deadWorms.clear();

	if(m_teams.size() < 2){
		m_gameEnd = true;

		std::string announcement;
		if(m_teams.size() == 1){
			auto& worms = m_teams[0]->GetAllWormsInTeam();
			for (auto& i : worms)
				i->RaiseEvent(Event::Victory);
			announcement = "Team " + m_teams[0]->GetTeamName() + " wins!";
		}
		else {
			announcement = "Draw! How boring.";
		}
		m_broadcastSystem.PlayAnnouncement(announcement);
		return;
	}
	m_windBox.SetBoxColor(0x000000ff);
	m_windBox.SetLabelColor(0xffffffff);
	m_timeBox.SetBoxColor(0x000000ff);
	m_timeBox.SetLabelColor(0xffffffff);
	m_turnOver = false;
	m_currentTurnTime = m_turnTime;
	m_currentTimeBetweenTurns = m_maxTimeBetweenTurns;
	m_input.activeWorm = NextWorm();
	m_input.activeWorm->RaiseEvent(Event::NewTurn);
	m_input.m_inputDisabled = false;
	VelocityComponent::wind = { glm::linearRand<float>(-100.0f,100.0f),0.0f };
	std::string wind_str = "Wind: " + NumberToString<int>(VelocityComponent::wind.x);
	m_windBox.UpdateText(wind_str);
	m_windBox.SetPosition({ m_windowDimensions.x - m_windBox.GetBoxSize().x - 50.0f, 50.0f });
}

void TurnSystem::EndTurn() {
	m_input.m_inputDisabled = true;
	m_input.activeWorm->RaiseEvent(Event::TurnOver);
//	_input.Remove_Key_Action(KeyboardKeys::KEYBOARD_SPACE);
	m_turnOver = true;
	m_currentTurnTime = 0.0f,
	m_timeBox.UpdateText(NumberToString<int>(m_currentTurnTime));
	m_windBox.SetBoxColor(0x00000000);
	m_windBox.SetLabelColor(0xffffff00);
	m_timeBox.SetBoxColor(0x00000000);
	m_timeBox.SetLabelColor(0xffffff00);
}