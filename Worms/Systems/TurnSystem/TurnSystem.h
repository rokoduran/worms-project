#pragma once
#include "../Worms/Structures/Team/Team.h"
#include "../Worms/Systems/InputSystem/InputSystem.h"
#include "../Worms/Structures/BoxLabel/BoxLabel.h"
#include "../Worms/Systems/BroadcastSystem/BroadcastSystem.h"

class TurnSystem {
	private:
		bool& m_gameEnd;
		BroadcastSystem m_broadcastSystem;
		InputSystem m_input;
		std::vector<std::shared_ptr<Team>> m_teams;
		std::set <unsigned int> m_deadWorms;
		unsigned int m_currentTeamIndex;
		glm::vec2 m_windowDimensions;
		float m_turnTime, m_currentTurnTime, m_maxTimeBetweenTurns, m_currentTimeBetweenTurns;
		BoxLabel m_timeBox, m_windBox;
		bool m_turnOver;
	public:
		TurnSystem(bool& game_end,float turn_time, float time_between_turns, Font&, const glm::vec2& window_dimensions, unsigned int color = 0xffffffff);
		void Start();
		void Deactivate();
		void Update(float dt);
		void Draw(Window& win);
		bool UpdateTimeBetween(float dt);
		void NewTurn();
		void AddTeam(std::shared_ptr<Team> new_team);
		void WormDrowned(unsigned int id);
		void WormDied(unsigned int id);
		bool IsTurnOver() {
			return m_turnOver;
		};
		void EndTurn();
		InputSystem& GetInputSys() {
			return m_input;
		}
		BroadcastSystem& GetBroadcastSystem() {
			return m_broadcastSystem;
		}
	private:
		void RemoveWorm(unsigned int id);
		std::shared_ptr<Entity> NextWorm();
};