#include "Game/Game.h"

int main(void){
#ifndef DEBUG
	FreeConsole();
#endif
	std::unique_ptr<Game> game = std::make_unique<Game>();
	while (game->IsRunning()) {
		game->ProcessInput();
		game->Update();
		game->LateUpdate();
		game->Draw();
		game->CalculateDeltaTime();
	}
	game->End();

	return 0;
}
