#version 330 core

layout (location=0) out vec4 color;
in vec4 v_color;
in vec2 v_tex_coord;
in float v_tex_id;
uniform sampler2D u_textures[32];

void main(){
		int index = int(v_tex_id);
		color = texture(u_textures[index], v_tex_coord)*v_color;
}