#version 330 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;
layout (location = 2) in unsigned int tex_id;
layout (location = 3) in unsigned int camera_affected;
out vec4 v_color;
out vec2 v_tex_coord;
out float v_tex_id;
uniform mat4 u_view_matrix;
uniform mat4 u_proj_matrix;

void main()
{
	if (bool(camera_affected)) {
		gl_Position = u_proj_matrix*u_view_matrix*vec4(position.x,position.y, 0.0f, 1.0f);
	}
	else {
		gl_Position = u_proj_matrix*vec4(position.x,position.y, 0.0f, 1.0f);
	}
	v_color = color;
	v_tex_coord = vec2(position.z,position.w);
	v_tex_id = float(tex_id);
}