#include "Sound.h"
#ifdef DEBUG
#include "util/Log.h"
#endif

gau_Manager* Sound::mngr = nullptr;
ga_Mixer* Sound::mixer = nullptr;

static void LoopOnEnd(ga_Handle* in_handle, void* in_context){
	gc_int32* flag = (gc_int32*)(in_context);
	*flag = 1;
	ga_handle_destroy(in_handle);
}

void Sound::Init(){
	gc_initialize(0);
	mngr = gau_manager_create();
	mixer = gau_manager_mixer(mngr);
}

void Sound::ShutDown() {
	gau_manager_destroy(mngr);
	gc_shutdown();
}

void Sound::Update() {
	gau_manager_update(mngr);
}
Sound::Sound() : m_filePath(""), m_handle(nullptr), m_volume(0.1f), m_looped(false), m_bufferPos(NULL) {};

Sound::Sound(const std::string& filepath, float volume): m_handle(nullptr), m_volume(volume), m_looped(false), m_bufferPos(NULL) {
	Load(filepath);
}

void Sound::Load(const std::string& filepath){
	m_filePath = filepath;
	std::string fileType = m_filePath.substr(m_filePath.find('.') + 1, m_filePath.back());
#ifdef DEBUG
	if (fileType.compare("ogg") && fileType.compare("wav")){
		std::cout << "Invalid sound file!" << std::endl;
		ASSERT(0);
	}
#endif
	m_sound = gau_load_sound_file(m_filePath.c_str(), fileType.c_str());
}

Sound::~Sound() {
	if (m_handle->state == GA_HANDLE_STATE_PLAYING)
		Stop();
	ga_sound_release(m_sound);
}

void Sound::Play() {
	gc_int32 quit = 0;
	m_handle = gau_create_handle_sound(mixer, m_sound, &LoopOnEnd, &quit, NULL);
	ga_handle_play(m_handle);
	ga_handle_setParamf(m_handle, GA_HANDLE_PARAM_GAIN, m_volume);
}

void Sound::Loop() {
	m_looped = true;
	gau_SampleSourceLoop* loopSrc = 0;
	gau_SampleSourceLoop** pLoopSrc = &loopSrc;
	gc_int32 quit = 0;
	m_handle = gau_create_handle_sound(mixer, m_sound, &LoopOnEnd, &quit, pLoopSrc);
	ga_handle_play(m_handle);
	ga_handle_setParamf(m_handle, GA_HANDLE_PARAM_GAIN, m_volume);
}

void Sound::SetVolume(float gain) {
	if(m_handle != nullptr && m_handle->state == GA_HANDLE_STATE_PLAYING)
		ga_handle_setParamf(m_handle, GA_HANDLE_PARAM_GAIN, m_volume);
	m_volume = gain;
}

void Sound::Resume() {
	gau_SampleSourceLoop* loopSrc = 0;
	gau_SampleSourceLoop** pLoopSrc = &loopSrc;
	gc_int32 quit = 0;
	if (!m_looped)
		pLoopSrc = NULL;
	m_handle = gau_create_handle_sound(mixer, m_sound, &LoopOnEnd, &quit, pLoopSrc);
	ga_handle_play(m_handle);
	ga_handle_seek(m_handle, m_bufferPos);
	m_bufferPos = NULL;
	ga_handle_setParamf(m_handle, GA_HANDLE_PARAM_GAIN, m_volume);
}

void Sound::Pause() {
	if(m_handle != nullptr && m_handle->state == GA_HANDLE_STATE_PLAYING){
		m_bufferPos = ga_handle_tell(m_handle, GA_TELL_PARAM_CURRENT);
		Stop();
	}
}

void Sound::Stop() {
	if(m_handle != nullptr && m_handle->state == GA_HANDLE_STATE_PLAYING)
		ga_handle_stop(m_handle);
}