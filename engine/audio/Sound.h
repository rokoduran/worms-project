#pragma once
#include <string>
#include "graphics/window/Window.h"
#include "util/Gorilla-Audio/ga.h"
#include "util/Gorilla-Audio/gau.h"

class Sound {
	protected:
		friend class Window;
		std::string m_filePath;
		ga_Sound* m_sound;
		ga_Handle* m_handle;
		gc_int32 m_bufferPos;
		float m_volume;
		bool m_looped;
		static gau_Manager* mngr;
		static ga_Mixer* mixer;
		static void Init();
		static void ShutDown();
	public:
		Sound();
		Sound(const std::string& filepath, float volume = 0.1f);
		~Sound();
		void Load(const std::string& filepath);
		void Play();
		void SetVolume(float gain);
		void Loop();
		void Resume();
		void Pause();
		void Stop();
		static void Update();
		inline const std::string& Get_Filepath()const { return m_filePath; }
};