#include "View.h"
#include "util/defines.h"

View::View() : m_halvedCoords({ 0.0f,0.0f }), m_position(0.0f, 0.0f), m_scale(1.0f, 1.0f, 1.0f), m_rotation(0) {
	Recalculate_View();
};

View::View(float w, float h) : m_halvedCoords({ w/2,h/2 }), m_position(0.0f, 0.0f), m_scale(1.0f, 1.0f, 1.0f), m_rotation(0) {
	Recalculate_View();
}

void View::Move(float x, float y) {
	m_position.x -= x;
	m_position.y -= y;
	Recalculate_View();
}

void View::UpdateCoords(float w, float h) {
	m_halvedCoords.x = w / 2;
	m_halvedCoords.y = h / 2;
	Recalculate_View();
}

void View::Move(const glm::vec2& other) {
	m_position.x -= other.x;
	m_position.y -= other.y;
	Recalculate_View();
}
void View::SetPosition(float x, float y) {
	m_position.x = x;
	m_position.y = y;
	Recalculate_View();
}
void View::SetPosition(const glm::vec2& pos) {
	m_position.x = pos.x;
	m_position.y = pos.y;
	Recalculate_View();
}
void View::Scale(float x, float y) {
	m_scale.x *= x;
	m_scale.y *= y;
	Recalculate_View();
}
void View::Scale(const glm::vec2& other) {
	m_scale.x *= other.x;
	m_scale.y *= other.y;
	Recalculate_View();
}
void View::Rotate(float angle) {
	if (angle < 0)
		angle += 360.0f;
	m_rotation += angle;
	Recalculate_View();
}
void View::SetScale(float x, float y) {
	m_scale.x = x;
	m_scale.y = y;
	Recalculate_View();
}
void View::SetScale(const glm::vec2& other) {
	m_scale.x = other.x;
	m_scale.y = other.y;
	Recalculate_View();
}
void View::SetRotation(float angle) {
	m_rotation =(float)(angle * M_PI / 180.0f);
	Recalculate_View();
}

void View::CenterTo(const glm::vec2& pos, const glm::vec2& offset) {
	m_position.x = m_scale.x*(-pos.x - offset.x + m_halvedCoords.x);
	m_position.y = m_scale.y*(-pos.y - offset.y + m_halvedCoords.y);
	Recalculate_View();
}

const glm::mat4& View::GetView()const{
	return m_view;
}

void View::Recalculate_View() {
	m_view = glm::mat4(1.0f);
	m_view = glm::translate(m_view, glm::vec3(m_position, 1.0f));
	m_view = glm::translate(m_view, glm::vec3(m_halvedCoords.x, m_halvedCoords.y, 0.0f));
	m_view = glm::rotate(m_view, glm::degrees(m_rotation), glm::vec3(0.0f, 0.0f, 1.0f));
	m_view = glm::scale(m_view, m_scale);
	m_view = glm::translate(m_view, glm::vec3(-m_halvedCoords.x, -m_halvedCoords.y, 0.0f));
}