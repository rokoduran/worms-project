#pragma once
#include "math/glm/gtc/matrix_transform.hpp"

//Class that represents the view matrix(camera)
class View  {
	private:
		glm::vec2 m_position;
		glm::vec3 m_scale;
		float m_rotation;
		glm::mat4 m_view;
		glm::vec2 m_halvedCoords;
	public:
		View();
		View(float width, float height);
		void Move(float x, float y);
		void Move(const glm::vec2& other);
		void SetPosition(float x, float y);
		void SetPosition(const glm::vec2& pos);
		void Scale(float x, float y);
		void Scale(const glm::vec2& other);
		void SetScale(float x, float y);
		void SetScale(const glm::vec2& other);
		void SetRotation(float angle);
		void UpdateCoords(float width, float height);
		void Rotate(float angle);
		const glm::mat4& GetView()const;
		inline const glm::vec2& GetPosition()const { return m_position; }
		inline const glm::vec3& GetScale()const { return m_scale; }
		inline const float GetRotation()const { return m_rotation; }
		void CenterTo(const glm::vec2&, const glm::vec2& = { 0.0f,0.0f });
	private:
		//The view matrix has to be recalculated after every change to pos/rot/scale.
		void Recalculate_View();
};