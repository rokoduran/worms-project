#include "PerlinNoise.h"
#include <iterator>
#include <numeric>
#include <random>
#include "util/Log.h"
#include "util/defines.h"
#include "util/stb_image/stb_image.h"

PerlinNoise::~PerlinNoise() {
	delete[] m_maskData;
	delete[] m_data;
}

PerlinNoise::PerlinNoise(){
	int w, h, c;
	m_maskData = stbi_load("assets/map/Mask.png", &w, &h, &c, 4);
}

void PerlinNoise::CalculateNoise(unsigned int seed,double x_modif= 5.0, double y_modif=7.0,double threshold =0.45) {
	// Generate a new permutation vector based on the value of seed
	for (unsigned int i = 0; i < 256; i++)
		p[i] = i;

	std::default_random_engine engine(seed);
	std::shuffle(&p[0], &p[255], engine);
	for (unsigned int i = 256; i < 512; i++)
		p[i] = p[i - 256];

	if (m_data != NULL)
		delete[] m_data;
	m_data = new unsigned char[m_width*m_height]();
	unsigned int kk = 0;
	// Visit every pixel of the image and assign a color generated with Perlin noise
	for (unsigned int i = 0; i < m_height; i++) {      // y
		for (unsigned int j = 0; j < m_width; j++) {  // x
			double x = (double)j / ((double)m_width);
			double y = (double)i / ((double)m_height);
			double n = Noise(x_modif * x, y_modif * y, 0.0);
			m_data[kk] = n < threshold ? 1 : 0;
			kk++;
		}
	}
	//Remove noise piles smaller than the pixel amount given as parameter
	RemoveBubbles(40);
		for (unsigned int i = 0; i < m_width*m_height; i++) 
			m_data[i] *= m_maskData[i*4];
}

void PerlinNoise::RemoveBubbles(unsigned int delimiter) {
	std::vector<unsigned int> coordsForDeletion;
	//Clear the left bubbles
	for (unsigned int y = 0 ; y < m_height; y++) {
		if (m_data[m_width*y] == 1) {
			unsigned int yOffset = 1;
			//Find how far the bubble goes along Y
			unsigned int fakeY = y;
			while (true) {
				if (m_data[m_width*(++fakeY)] == 1)
					yOffset++;
				else
					break;
			}
			unsigned int maxX = 0;
			for (unsigned int yy = y; yy < y + yOffset; yy++) {
				unsigned int xCounter = 0;
				for (unsigned int x = 0; x < m_width; x++) {
					if (m_data[x+ (yy*m_width)] == 1) {
						coordsForDeletion.push_back(x + (yy*m_width));
						xCounter++;
					}
					else break;
				}
				if (xCounter > maxX)
					maxX = xCounter;
				if (maxX > delimiter) {
					coordsForDeletion.clear();
					break;
				}
			}
			if (maxX < delimiter) {
				for (unsigned int& i : coordsForDeletion)
					m_data[i] = 0;
				coordsForDeletion.clear();
			}
			y += yOffset;
		}
	}
	//Clear the right bubbles
	for (unsigned int y = 0; y < m_height; y++) {
		if (m_data[((m_width)*y)+(m_width)] == 1) {
			unsigned int yOffset = 1;
			//Find how far the bubble goes along Y
			unsigned int fakeY = y;
			while (true) {
				if (m_data[(m_width)*(++fakeY)+(m_width)] == 1)
					yOffset++;
				else
					break;
			}
			unsigned int maxX = 0;
			for (unsigned int yy = y; yy < y + yOffset; yy++) {
				unsigned int xCounter = 0;
				for (unsigned int x = 0; x < m_width; x++) {
					if (m_data[(yy*(m_width))+ m_width - x] == 1) {
						coordsForDeletion.push_back((yy*(m_width)) + m_width  - x);
						xCounter++;
					}
					else break;
				}
				if (xCounter > maxX)
					maxX = xCounter;
				if (maxX > delimiter) {
					coordsForDeletion.clear();
					break;
				}
			}
			if (maxX < delimiter) {
				for (unsigned int& i : coordsForDeletion)
					m_data[i] = 0;
				coordsForDeletion.clear();
			}
			y += yOffset;
		}
	}
	//Clear the top bubbles
	
	for (unsigned int x = 0; x < m_width; x++) {
		if (m_data[((m_height-1)*(m_width))+x] == 1) {
			unsigned int xOffset = 1;
			//Find how far the bubble goes along X
			unsigned int fakeX = x;
			while (true) {
				if (m_data[((m_height-1)*(m_width)) + (++fakeX)] == 1)
					xOffset++;
				else
					break;
			}
			unsigned int maxY = 0;
			for (unsigned int xx = x; xx < x + xOffset; xx++) {
				unsigned int yCounter = 0;
				for (unsigned int y = 0; y < m_height; y++) {
					if (m_data[((m_height-1-y)*m_width) + xx] == 1) {
						coordsForDeletion.push_back(((m_height - 1 - y)*m_width) + xx);
						yCounter++;
					}
					else break;
				}
				if (yCounter > maxY)
					maxY = yCounter;
				if (maxY > delimiter) {
					coordsForDeletion.clear();
					break;
				}
			}
			if (maxY < delimiter) {
				for (unsigned int& i : coordsForDeletion)
					m_data[i] = 0;
				coordsForDeletion.clear();
			}
			x += xOffset;
		}
	}
}

double PerlinNoise::Noise(double x, double y, double z) {
	// Find the unit cube that contains the point
	int X = (int)floor(x) & 255;
	int Y = (int)floor(y) & 255;
	int Z = (int)floor(z) & 255;

	// Find relative x, y,z of point in cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// Compute fade curves for each of x, y, z
	double u = Fade(x);
	double v = Fade(y);
	double w = Fade(z);

	// Hash coordinates of the 8 cube corners
	int A = p[X] + Y;
	int AA = p[A] + Z;
	int AB = p[A + 1] + Z;
	int B = p[X + 1] + Y;
	int BA = p[B] + Z;
	int BB = p[B + 1] + Z;

	// Add blended results from 8 corners of cube
	double res = Lerp(w, Lerp(v, Lerp(u, Grad(p[AA], x, y, z), Grad(p[BA], x - 1, y, z)), Lerp(u, Grad(p[AB], x, y - 1, z), Grad(p[BB], x - 1, y - 1, z))), Lerp(v, Lerp(u, Grad(p[AA + 1], x, y, z - 1), Grad(p[BA + 1], x - 1, y, z - 1)), Lerp(u, Grad(p[AB + 1], x, y - 1, z - 1), Grad(p[BB + 1], x - 1, y - 1, z - 1))));
	return (res + 1.0) / 2.0;
}

double PerlinNoise::Fade(double t) {
	return t * t * t * (t * (t * 6 - 15) + 10);
}

//Linear interpolation
double PerlinNoise::Lerp(double t, double a, double b) {
	return a + t * (b - a);
}

double PerlinNoise::Grad(int hash, double x, double y, double z) {
	int h = hash & 15;
	// Convert lower 4 bits of hash into 12 gradient directions
	double u = h < 8 ? x : y,
		v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}