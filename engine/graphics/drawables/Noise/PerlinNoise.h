#pragma once
#include "../sprite/Texture.h"


class PerlinNoise {
	private:
		unsigned int m_width = 1920;
		unsigned int m_height = 696;
		int p[512];
		unsigned char* m_data = NULL;
		unsigned char* m_maskData= NULL;
	public:
		PerlinNoise();
		~PerlinNoise();
		// Generate a noise value,2D images z can have any value
		void CalculateNoise(unsigned int seed, double xmodif , double ymodif , double threshold);
		unsigned char* FlushData() {
			return m_data;
		};
private:
		void RemoveBubbles(unsigned int delimiter);
		double Noise(double x, double y, double z);
		double Fade(double t);
		double Lerp(double t, double a, double b);
		double Grad(int hash, double x, double y, double z);
};