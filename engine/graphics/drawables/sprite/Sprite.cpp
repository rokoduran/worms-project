#include "Sprite.h"
#include "math/glm/gtc/matrix_transform.hpp"


Sprite::Sprite() : m_color(0xffffffff), m_texId(0), m_innerRect(), m_cameraAffected(true),m_angle(0.0f), m_rotatingAxes(0.0f,0.0f,1.0f)  {
	NormalizeTexCoords();
	RecalculateModel();
};

Sprite::Sprite(unsigned int color) : m_texId(0),m_color(SwapHex(color)), m_innerRect(), m_cameraAffected(true), m_angle(0.0f), m_rotatingAxes(0.0f, 0.0f, 1.0f){
	NormalizeTexCoords();
	RecalculateModel();
};

Sprite::Sprite(const Texture& tex) : m_texId(tex.GetTexID()), m_cameraAffected(true), m_color(0xffffffff), m_innerRect({0.0f,0.0f}, { tex.GetWidth(), tex.GetHeight() }), m_angle(0.0f), m_rotatingAxes(0.0f, 0.0f, 1.0f) {
	NormalizeTexCoords();
	RecalculateModel();
};

void Sprite::NormalizeTexCoords() {
	m_texCoordinates[0] = { 0.0f,0.0f };
	m_texCoordinates[1] = { 1.0f,0.0f };
	m_texCoordinates[2] = { 1.0f,1.0f };
	m_texCoordinates[3] = { 0.0f,1.0f };
}

void Sprite::SetTexCoords(const glm::vec2* tex_coords, const glm::vec2& sprite_size) {
	m_innerRect.SetSize(sprite_size);
	m_texCoordinates[0] = tex_coords[0];
	m_texCoordinates[1] = tex_coords[1];
	m_texCoordinates[2] = tex_coords[2];
	m_texCoordinates[3] = tex_coords[3];
	RecalculateModel();
}

void Sprite::Move(float x, float y) {
	const glm::vec2& origin = m_innerRect.GetOrigin();
	m_innerRect.SetOrigin(origin.x + x, origin.y + y);
	RecalculateModel();
}
void Sprite::Move(const glm::vec2& move) { 
	const glm::vec2& origin = m_innerRect.GetOrigin();
	m_innerRect.SetOrigin(origin.x + move.x, origin.y + move.y);
	RecalculateModel();
}
void Sprite::SetPosition(float x, float y) {
	m_innerRect.SetOrigin(x, y);
	RecalculateModel();
}
void Sprite::SetPosition(const glm::vec2& pos) {
	m_innerRect.SetOrigin(pos);
	RecalculateModel();
}

const glm::vec2& Sprite::GetPosition()const{
	return m_innerRect.GetOrigin();
}

void Sprite::SetCenter(float x, float y) {
	const glm::vec2& size = m_innerRect.GetSize();
	m_innerRect.SetOrigin(x - 0.5f * size.x, y - 0.5f*size.y);
	RecalculateModel();
};

void Sprite::SetCenter(const glm::vec2& center) {
	const glm::vec2& size = m_innerRect.GetSize();
	m_innerRect.SetOrigin(center.x - 0.5f * size.x, center.y - 0.5f* size.y);
};

const glm::vec2& Sprite::GetCenter()const {
	return m_innerRect.GetCenter();
};

void Sprite::SetRotatingAxes(const glm::vec3& axes) {
	m_rotatingAxes = axes;
}

void Sprite::Rotate(float angle) {
	m_angle += (float)(angle*M_PI/180.0f); 
	RecalculateModel();
}
void Sprite::SetRotation(float angle) { 
	m_angle = (float)(angle*M_PI/180.0f); 
	RecalculateModel();
}

void Sprite::SetSize(float x, float y) {
	m_innerRect.SetSize(x, y);
	RecalculateModel();
};

void Sprite::SetSize(const glm::vec2& size) {
	m_innerRect.SetSize(size);
	RecalculateModel();
};

void Sprite::SetColor(unsigned int color) {
	m_color = SwapHex(color);
}

void Sprite::SetTexture(const Texture& tex) {
	m_texId = tex.GetTexID();
	m_innerRect.SetSize((float)tex.GetWidth(), (float)tex.GetHeight());
	RecalculateModel();
}

void Sprite::RecalculateModel() {
	const glm::vec2& position = m_innerRect.GetOrigin();
	const glm::vec2& size =m_innerRect.GetSize();
	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, glm::vec3(m_innerRect.GetCenter(), 0.0f));
	m_model = glm::rotate(m_model, m_angle, m_rotatingAxes);
	m_model = glm::translate(m_model, glm::vec3(-position.x-0.5f * size.x, -position.y -0.5f * size.y, 0.0f));
}
