#pragma once
#include "../../renderer/Render2D.h"
#include "Texture.h"
#include "util/defines.h"
#include "util/Shapes/Rectangle.h"
class Sprite{	
	private:
		//Model matrix
		glm::mat4 m_model;
		//The axes around which the sprite will rotate. Default is z
		glm::vec3 m_rotatingAxes;
		//Id of the texture
		unsigned int m_texId;
		//The rectangle area of the sprite around which the texture will wrap
		Rect m_innerRect;
		//The color of the sprite
		unsigned int m_color;
		//Texture coordinates of the sprite
		glm::vec2 m_texCoordinates[4];
		//Will the vertexs of the sprite multiply with the view matrix in the shader
		bool m_cameraAffected;
		//The angle of the sprite
		float m_angle;
	public:
		Sprite(const Texture&);
		Sprite(unsigned int);
		Sprite();
		//Reset the texture coordinates to the default
		void NormalizeTexCoords();
		void SetTexCoords(const glm::vec2* tex_coords, const glm::vec2& size);
		void Move(float x, float y);
		void Move(const glm::vec2& other);
		void SetPosition(float x, float y);
		void SetPosition(const glm::vec2& pos);
		const glm::vec2& GetPosition()const;
		void SetCenter(float x, float y);
		void SetCenter(const glm::vec2& center);
		const glm::vec2& GetCenter()const;
		void SetRotatingAxes(const glm::vec3&);
		void SetRotation(float angle);
		void Rotate(float angle);
		void SetTexture(const Texture& tex);
		void SetColor(unsigned int color);
		void SetSize(float x, float y);
		void SetSize(const glm::vec2& size);
		inline const glm::vec2* GetTexCoords()const { return m_texCoordinates; }
		inline const bool GetCameraAffected()const { return m_cameraAffected; }
		inline void SetCameraAffected(bool value) { m_cameraAffected = value; }
		const glm::vec2& GetSize()const { return m_innerRect.GetSize(); }
		inline const unsigned int GetColor()const { return m_color;}
		inline const unsigned int GetTexID()const { return m_texId; }
		inline const glm::mat4& GetModel()const { return m_model; }
		float GetRotation() {
			return m_angle;
		}
	private:
		//The model matrix of the sprites has to be recalculated after every change to any of the sprites spacial properties
		void RecalculateModel();
};