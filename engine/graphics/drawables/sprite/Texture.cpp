#include "Texture.h"
#include "util/stb_image\stb_image.h"
#include "util/Log.h"


Texture::Texture() : m_textureId(0), m_texturePath(""), m_width(0), m_height(0) {};

Texture::Texture(const std::string& path) :m_textureId(0), m_texturePath(path),m_width(0), m_height(0){
	Load(path);
};

Texture::Texture(unsigned char* data, unsigned int width, unsigned int height, GLint internalformat, GLenum format): m_width ((int)width),m_height ((int)height){
	GLCall(glGenTextures(1, &m_textureId));
	GLCall(glBindTexture(GL_TEXTURE_2D, m_textureId));
	//min_filter if the image is too large and has to be shrunk
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	//mag(nifying) filter if the image is too small and needs upscaling
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	//Clamp to stop it from expanding outside its bounds
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, internalformat, m_width, m_height, 0, format, GL_UNSIGNED_BYTE, data));
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
}

Texture::~Texture() {
	GLCall(glDeleteTextures(1, &m_textureId));
}

void Texture::Load(const std::string& path) {
	stbi_set_flip_vertically_on_load(1);
	int channels;
	unsigned char* buffer = stbi_load(path.c_str(), &m_width, &m_height, &channels, 4);
	GLCall(glGenTextures(1, &m_textureId));
	GLCall(glBindTexture(GL_TEXTURE_2D, m_textureId));
	//Min_Filter, if the image is too large and needs to be shrinked
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	//Mag(nifying) filter if the image is too small and needs upscaling
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	//Clamp to prevent expansion
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer));
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
	delete[] buffer;
}

void Texture::Bind(unsigned int texSlot)const{
	GLCall(glActiveTexture(GL_TEXTURE0 + texSlot));
	//Bind this textureId to the bound slot
	GLCall(glBindTexture(GL_TEXTURE_2D, m_textureId));
}

void Texture::Unbind()const {
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
}
