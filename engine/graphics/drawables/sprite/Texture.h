#pragma once
#include <string>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
class Texture{
	private: 
		unsigned int m_textureId;
		std::string m_texturePath;
		int m_width, m_height;
	public:
		Texture();
		Texture(const std::string& path);
		Texture(unsigned char* data, unsigned int width, unsigned int height, GLint internalformat, GLenum format);
		~Texture();
		void Load(const std::string& path);
		void Bind(unsigned int texture_slot = 0)const;
		void Unbind()const;
		inline const unsigned int GetWidth()const  { return m_width; }
		inline const unsigned int GetHeight()const{ return m_height; }
		inline const unsigned int GetTexID()const { return m_textureId; }
};