#include "Font.h"
#include "util/Log.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>


Font::Font(): m_path(""), m_texAtlas(ftgl::texture_atlas_new(512, 512, 2)), m_size(32) {

}
Font::Font(const std::string& filename, unsigned int size) :m_path(filename),m_texAtlas(ftgl::texture_atlas_new(512, 512, 2)), m_texFont(ftgl::texture_font_new_from_file(m_texAtlas, (float)size, filename.c_str())), m_size(size) {
	GLCall(glGenTextures(1, &m_texAtlas->id));
}

Font::~Font() {
	delete m_texFont;
	GLCall(glDeleteTextures(1, &m_texAtlas->id));
	delete m_texAtlas;
}

void Font::Load(const std::string& path) {
	m_path = path;
	m_texFont = ftgl::texture_font_new_from_file(m_texAtlas, (float)m_size, m_path.c_str());
}

const ftgl::texture_glyph_t* Font::Get_Glyph(const char& c)const {
		ftgl::texture_glyph_t* glyph = texture_font_get_glyph(m_texFont, &c);
		return glyph;
}

void Font::SetSize(unsigned int size) {
	m_size = size;
	if(m_texFont!=nullptr)
		m_texFont->size = (float)size;
}

void Font::BindFont()const {
	GLCall(glBindTexture(GL_TEXTURE_2D, m_texAtlas->id));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE_ALPHA, m_texAtlas->width, m_texAtlas->height,
		0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, m_texAtlas->data));
}