#pragma once
#include <util/freetype-GL/freetype-gl.h>
#include <string>
#include <math/glm/glm.hpp>

class Font {
	private:
		ftgl::texture_atlas_t* m_texAtlas = nullptr;
		ftgl::texture_font_t* m_texFont = nullptr;
		unsigned int m_size;
		std::string m_path;
	public:
		Font();
		Font(const std::string&, unsigned int= 16);
		~Font();
		void Load(const std::string& path);
		void SetSize(unsigned int);
		const ftgl::texture_glyph_t* Get_Glyph(const char&)const;
		void BindFont()const;
		inline const std::string&  GetFilePath()const { return m_path; }
		inline unsigned int GetAtlasID()const { return m_texAtlas->id; }
		inline unsigned int GetFontSize()const { return m_size; }
};