#pragma once
#include "util/defines.h"
#include "Font.h"
#include <string>

class Label{
	private:
		glm::vec2 m_position;
		Font& m_font;
		unsigned int m_color;
		bool m_cameraAffected;
		std::string m_text;
		float m_advance;
	public:
		Label(Font& font, unsigned int color = 0x000000ff, std::string text = "")
			: m_color(SwapHex(color)), m_font(font), m_cameraAffected(0), m_position(0.0f,0.0f) {
			UpdateText(text);
		};;
		void UpdateText(const std::string& text) {
			m_text = text;
			float advance = 0;
			for (unsigned int i = 0; i < m_text.length(); i++) {
				const char& c = m_text.at(i);
				const ftgl::texture_glyph_t* glyph = m_font.Get_Glyph(c);
				if (i > 0)
					advance += ftgl::texture_glyph_get_kerning(glyph, &m_text.at(i - 1));
				if (glyph != NULL) {
					advance += glyph->advance_x;
				}
			};
			m_advance = advance;
		}
		const std::string& GetText()const {
			return m_text;
		}
		float GetAdvance()const {
			return m_advance;
		}
		void Move(float x, float y) { m_position.x += x; m_position.y += y; }
		void Move(const glm::vec2& other) { m_position.x += other.x; m_position.y += other.y; }
		void SetPosition(float x, float y) { m_position.x = x; m_position.y = y; }
		void SetPosition(const glm::vec2& pos) { m_position.x = pos.x; m_position.y = pos.y; }
		const glm::vec2& GetPosition()const { return m_position; }
		void SetCameraAffected(bool value) { m_cameraAffected = value; }
		const bool GetCameraAffected()const { return m_cameraAffected; }
		void SetColor(unsigned int color) { m_color = SwapHex(color); }
		const unsigned int GetColor()const{ return m_color; }
		const Font& GetFont()const { return m_font; }
		//Labels don't use model matrices like sprites do.
};

