#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "util/Log.h"
#include "IndexBuffer.h"

void IndexBuffer::SetIB(const unsigned int size){
	//We use unsigned short because we dont plan to have nearly close to 10000+ sprites in the game at any point,
	//so we preserve some memory this way
	unsigned short* indices = new unsigned short[size];

	unsigned short offset = 0;
	for (unsigned int i = 0; i < size; i+=6) {
		indices[i + 0] = 0 + offset;
		indices[i + 1] = 1 + offset	;
		indices[i + 2] = 2 + offset	;
		indices[i + 3] = 2 + offset	;
		indices[i + 4] = 3 + offset	;
		indices[i + 5] = 0 + offset	;
		offset+=4;
	}

	GLCall(glGenBuffers(1, &m_iBufferId));
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBufferId));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, size*sizeof(unsigned short), indices, GL_STATIC_DRAW));
	delete[] indices;
}
IndexBuffer::~IndexBuffer() {
	GLCall(glDeleteBuffers(1, &m_iBufferId));
}
void IndexBuffer::Bind() {
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBufferId));
}
void IndexBuffer::Unbind(){
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

}
