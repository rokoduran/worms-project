#pragma once

class IndexBuffer {
private:
	unsigned int m_iBufferId;
public:
	void SetIB(const unsigned int size);
	void Bind();
	void Unbind();
	~IndexBuffer();
	inline unsigned int GetIBID() { return m_iBufferId; }
};