#include "Render2D.h"
#include "../drawables/text/Label.h"
#include "../drawables//sprite/Sprite.h"

Render2D* Render2D::renderer = nullptr;

Render2D::Render2D(): m_shader("assets/shaders/basic_vert.shader", "assets/shaders/basic_frag.shader"){
	Init();
	m_shader.Bind();
}

void Render2D::Init() {
#ifdef DEBUG	
	    if (m_data.buffer) {
			std::cout << " Batch renderer was already initialized! " << std::endl;
			ASSERT(0);
		}
#endif
	m_data.VBO = new VertexBuffer();
	m_data.IBO = new IndexBuffer();
	m_data.VAO = new VertexArrayObject();

	m_data.VBO->SetVBO(MAXVERTICES * sizeof(VertexArrayElements));
	m_data.VAO->SetVAO();
	m_data.IBO->SetIB(MAXINDICES);
	m_data.VBO->Bind();
	m_data.VAO->Bind();
	m_data.IBO->Bind();

	MAXTEXTURESLOTS = GLSlots();
	m_samplerArr = new int[MAXTEXTURESLOTS];
	for (unsigned int i = 0; i < MAXTEXTURESLOTS; ++i)
		m_samplerArr[i] = i;
	InitTextures();
	m_data.textureSlots = new int[MAXTEXTURESLOTS];
	SetTexArray();

	m_data.buffer = new VertexArrayElements[MAXVERTICES];
}

void Render2D::BeginBatch() {
	m_data.bufferPointer = m_data.buffer;
	m_shader.SetUniform1ia("u_textures", MAXTEXTURESLOTS, m_samplerArr);
}

void Render2D::SetProjection(const glm::mat4& proj) {
	m_shader.SetUniformMat4f("u_proj_matrix", proj);
}

void Render2D::SetView(const glm::mat4& view) {
	m_shader.SetUniformMat4f("u_view_matrix", view);
}

void Render2D::EndBatch() {
	const unsigned int size = (unsigned int*)m_data.bufferPointer - (unsigned int*)m_data.buffer;
	m_data.VBO->Bind();
	GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, size*4, m_data.buffer));
}

void Render2D::Flush() {
	for (unsigned int i = 0; i < m_data.currentTexture;i++) {
		GLCall(glActiveTexture(GL_TEXTURE0 + i));
		GLCall(glBindTexture(GL_TEXTURE_2D, m_data.textureSlots[i]));
	}
	GLCall(glDrawElements(GL_TRIANGLES,m_data.indexCounter, GL_UNSIGNED_SHORT, nullptr));
	m_data.indexCounter = 0;
}

Render2D::~Render2D() {
	delete m_data.VBO;
	delete m_data.VAO;
	delete m_data.IBO;
	delete[] m_data.buffer;
}

void Render2D::DrawString(const Label& label) {
	unsigned int ts = 0;
	FindTexID(label.GetFont().GetAtlasID(), ts);;
	FillBufferFont(label,ts);
	label.GetFont().BindFont();
}

void Render2D::InitTextures() {
	GLCall(glGenTextures(1, &m_data.whiteTexture));
	GLCall(glBindTexture(GL_TEXTURE_2D, m_data.whiteTexture))
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	unsigned int white = 0xffffffff;
	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, 1,
		0, GL_RGBA, GL_UNSIGNED_BYTE, &white));
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
}

void Render2D::SetTexArray() {
	m_data.currentTexture = 1;
	m_data.textureSlots[0] = m_data.whiteTexture;
	for (unsigned int i = 1; i < MAXTEXTURESLOTS; ++i)
		m_data.textureSlots[i] = 0;
}

unsigned int Render2D::GLSlots() {
	int slots;
	GLCall(glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &slots));
	return slots;
}

void Render2D::FindTexID(unsigned int texid, unsigned int& texslot) {
	if(texid){
		if (m_texSlots.find(texid) == m_texSlots.end()) {
			if (m_data.currentTexture == MAXTEXTURESLOTS) {
				EndBatch();
				Flush();
				BeginBatch();
				SetTexArray();
				m_texSlots.clear();
			}
			texslot = m_data.currentTexture;
			m_data.textureSlots[texslot] = texid;
			m_texSlots[texid] = m_data.currentTexture;
			m_data.currentTexture++;
		}

		else {
			texslot = m_texSlots[texid];
		}
	}
}

void Render2D::InitializeRenderer() {
	renderer = new Render2D();
}

void Render2D::ShutdownRenderer() {
	renderer->~Render2D();
}

void Render2D::FillBufferFont(const Label& label,const unsigned int& texslot) {
	const glm::vec2& position = label.GetPosition();
	unsigned int color = label.GetColor();
	unsigned int ts = 0;
	const Font& font = label.GetFont();
	float advance = 0;
	unsigned int affected = label.GetCameraAffected() ? 1 : 0;
	const std::string& label_text = label.GetText();
	for (unsigned int i = 0; i < label_text.length(); i++) {
		const char& c = label_text.at(i);
		const ftgl::texture_glyph_t* glyph = font.Get_Glyph(c);
		if (i > 0)
			advance += ftgl::texture_glyph_get_kerning(glyph, &label_text.at(i - 1));
		if (glyph != NULL) {
			float x0 = position.x + glyph->offset_x + advance;
			float x1 = x0 + glyph->width;
			float y0 = position.y + glyph->offset_y;
			float y1 = y0 - glyph->height;

			float s0 = glyph->s0;
			float s1 = glyph->s1;
			float t0 = glyph->t0;
			float t1 = glyph->t1;

			m_data.bufferPointer->position = glm::vec4(x0, y0, s0, t0);
			m_data.bufferPointer->color = color;
			m_data.bufferPointer->textureId = texslot;
			m_data.bufferPointer->cameraAffected = affected;
			m_data.bufferPointer++;
				   
			m_data.bufferPointer->position = glm::vec4(x0, y1, s0, t1);
			m_data.bufferPointer->color = color;
			m_data.bufferPointer->textureId = texslot;
			m_data.bufferPointer->cameraAffected = affected;
			m_data.bufferPointer++;
				   
			m_data.bufferPointer->position = glm::vec4(x1, y1, s1, t1);
			m_data.bufferPointer->color = color;
			m_data.bufferPointer->textureId = texslot;
			m_data.bufferPointer->cameraAffected = affected;
			m_data.bufferPointer++;
				   
			m_data.bufferPointer->position = glm::vec4(x1, y0,s1,t0);
			m_data.bufferPointer->color = color;
			m_data.bufferPointer->textureId = texslot;
			m_data.bufferPointer->cameraAffected = affected;
			m_data.bufferPointer++;
			
			m_data.indexCounter += 6;
			advance += glyph->advance_x;
		}
	}
}

void Render2D::DrawQuad(const Rect& rect, const unsigned int color, bool cam_affected) {
	unsigned int affected = cam_affected ? 1 : 0;
	const glm::vec2& position = rect.GetOrigin();
	const glm::vec2& size = rect.GetSize();
	m_data.bufferPointer->position = glm::vec4(position, 0.0f, 0.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = 0;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = glm::vec4(position.x + size.x, position.y, 0.0f, 1.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = 0;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = glm::vec4(position.x + size.x, position.y + size.y, 1.0f, 1.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = 0;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = glm::vec4(position.x, position.y + size.y, 0.0f, 1.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = 0;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.indexCounter += 6;
}

void Render2D::DrawQuad(const Sprite& sprite) {
	const glm::vec2& position = sprite.GetPosition();
	const glm::mat4& model = sprite.GetModel();
	const glm::vec2& size = sprite.GetSize();
	const unsigned int color = sprite.GetColor();
	const unsigned int texid = sprite.GetTexID();
	const glm::vec2* coords = sprite.GetTexCoords();
	unsigned int affected = sprite.GetCameraAffected() ? 1 : 0;
	unsigned int ts = 0;
	FindTexID(texid, ts);
	m_data.bufferPointer->position = model*glm::vec4(position, 0.0f, 1.0f);
	m_data.bufferPointer->position.z =coords[0].x; m_data.bufferPointer->position.w = coords[0].y;
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = model*glm::vec4(position.x + size.x, position.y, 0.0f, 1.0f);
	m_data.bufferPointer->position.z = coords[1].x; m_data.bufferPointer->position.w = coords[1].y;
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = model*glm::vec4(position.x + size.x, position.y + size.y, 0.0f, 1.0f);
	m_data.bufferPointer->position.z = coords[2].x; m_data.bufferPointer->position.w = coords[2].y;
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = model*glm::vec4(position.x, position.y + size.y, 0.0f, 1.0f);
	m_data.bufferPointer->position.z =coords[3].x; m_data.bufferPointer->position.w = coords[3].y;
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.indexCounter += 6;
};

void Render2D::DrawQuad(const Rect& rect, const unsigned int color, const unsigned int texid, bool cam_affected) {
	const glm::vec2& position = rect.GetOrigin();
	const glm::vec2& size = rect.GetSize();
	unsigned int affected = cam_affected ? 1 : 0;
	unsigned int ts = 0;
	FindTexID(texid, ts);
	m_data.bufferPointer->position = glm::vec4(position, 0.0f, 0.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = glm::vec4(position.x + size.x, position.y, 1.0f, 0.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = glm::vec4(position.x + size.x, position.y + size.y, 1.0f, 1.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.bufferPointer->position = glm::vec4(position.x, position.y + size.y, 0.0f, 1.0f);
	m_data.bufferPointer->color = color;
	m_data.bufferPointer->textureId = ts;
	m_data.bufferPointer->cameraAffected = affected;
	m_data.bufferPointer++;

	m_data.indexCounter += 6;
}