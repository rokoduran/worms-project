#pragma once
#include "../drawables/text/Font.h"
#include "../shader/Shader.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexArrayObject.h"
#include "../drawables/sprite/Texture.h"
#include "util/Shapes/Rectangle.h"
#include <string>
#include <vector>
#include <unordered_map>

static const unsigned int MAXSPRITES = 10000;
static const unsigned int MAXINDICES = MAXSPRITES * 6;
static const unsigned int MAXVERTICES = MAXSPRITES * 4;

class Sprite;

class Label;

struct RenderData {
	VertexBuffer* VBO;
	VertexArrayObject* VAO;
	IndexBuffer* IBO;
	VertexArrayElements* buffer = nullptr;
	VertexArrayElements* bufferPointer= nullptr;
	unsigned int indexCounter=0;
	int* textureSlots;
	unsigned int currentTexture;
	unsigned int whiteTexture;
};


class Render2D {
	private:
		friend class Window;
		Shader m_shader;
		RenderData m_data;
		std::unordered_map<unsigned int, unsigned int> m_texSlots;
		int* m_samplerArr;
		unsigned int MAXTEXTURESLOTS;
		Render2D();
		~Render2D();
		static Render2D* renderer;
		void DrawString(const Label&);
		void SetProjection(const glm::mat4&);
		void SetView(const glm::mat4&);
		void DrawQuad(const Sprite& sprite);
		void DrawQuad(const Rect&, const unsigned int color, bool);
		void DrawQuad(const Rect& , const unsigned int color, const unsigned int texid, bool);
		inline unsigned int GetMAXTEXTURESLOTS()const { return MAXTEXTURESLOTS; }
		inline int* GetTextureSlots()const { return m_data.textureSlots; }
		inline unsigned int GetCurrentTex()const { return m_data.currentTexture; }
		void FindTexID(unsigned int texid, unsigned int& texslot);
		void SetTexArray();
		void InitTextures();
		void Init();
		unsigned int GLSlots();
		void FillBufferFont(const Label& label, const unsigned int& texslot);
		static void InitializeRenderer();
		static void ShutdownRenderer();
		void BeginBatch();
		void EndBatch();
		void Flush();
};