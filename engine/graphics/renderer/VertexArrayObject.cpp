#include "VertexArrayObject.h"

VertexArrayObject::VertexArrayObject() {
	GLCall(glGenVertexArrays(1, &m_VaoId));
	GLCall(glBindVertexArray(m_VaoId));
}
void VertexArrayObject::SetVAO() {
	GLCall(glEnableVertexAttribArray(0));
	GLCall(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(VertexArrayElements), (const void*)offsetof(VertexArrayElements, position)));
	GLCall(glEnableVertexAttribArray(1));
	GLCall(glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexArrayElements), (const void*)offsetof(VertexArrayElements, color)));
	GLCall(glEnableVertexAttribArray(2));
	GLCall(glVertexAttribIPointer(2, 1, GL_UNSIGNED_INT,sizeof(VertexArrayElements), (const void*)offsetof(VertexArrayElements, textureId)));
	GLCall(glEnableVertexAttribArray(3));
	GLCall(glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, sizeof(VertexArrayElements), (const void*)offsetof(VertexArrayElements, cameraAffected)));
}

VertexArrayObject::~VertexArrayObject() {
	GLCall(glDeleteVertexArrays(1,&m_VaoId);)
};

void VertexArrayObject::Bind(){
	GLCall(glBindVertexArray(m_VaoId));
};
void VertexArrayObject::Unbind(){
	GLCall(glBindVertexArray(0));
};