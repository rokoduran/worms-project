#pragma once
#include "math/glm/glm.hpp"
#include "util/Log.h"

struct VertexArrayElements {
	glm::vec4 position;
	unsigned int color;
	unsigned int textureId;
	unsigned int cameraAffected;
};

class VertexArrayObject {
	private:
		unsigned int m_VaoId;
	public:
		void SetVAO();
		VertexArrayObject();
		~VertexArrayObject();
		void Bind();
		void Unbind();
		inline unsigned int GetVAOID() { return m_VaoId; }
};