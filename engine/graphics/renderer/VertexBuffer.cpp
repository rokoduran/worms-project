#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "util/Log.h"
#include "VertexBuffer.h"

void VertexBuffer::SetVBO(const unsigned int size) {
	GLCall(glGenBuffers(1, &m_vBufferId));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_vBufferId));
	GLCall(glBufferData(GL_ARRAY_BUFFER, size, NULL,GL_DYNAMIC_DRAW));
}
VertexBuffer::~VertexBuffer() { 
	GLCall(glDeleteBuffers(1,&m_vBufferId));
}
void VertexBuffer::Bind() {
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_vBufferId));
};
void VertexBuffer::Unbind() {
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
};