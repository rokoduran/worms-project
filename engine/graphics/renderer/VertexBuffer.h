#pragma once

class VertexBuffer {
	private:
		unsigned int m_vBufferId;
	public:
		void SetVBO(const unsigned int size);
		~VertexBuffer();
		void Bind();
		void Unbind();
		inline unsigned int GetVBID() { return m_vBufferId; }
};