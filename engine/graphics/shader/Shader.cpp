#include "Shader.h"
#include "util/Fileread.h"
#include <string>
#include <vector>
#include <iostream>
#include "util/Log.h"


Shader::Shader(std::string vertexPath, std::string fragmentPath) : m_vertexPath(vertexPath), m_fragmentPath(fragmentPath) {
	m_shaderId = Load();
	if (!m_shaderId) {
		std::cout << " Shader failed to load. " << std::endl;
	}
}

Shader::~Shader() {
	GLCall(glDeleteProgram(m_shaderId));
}

GLuint Shader::Load() {
	GLuint program;
	GLCall(program=glCreateProgram());
	GLuint vertex;
	GLCall(vertex= glCreateShader(GL_VERTEX_SHADER));
	GLuint fragment;
	GLCall(fragment= glCreateShader(GL_FRAGMENT_SHADER));

	//Need to initialize vertex and fragment source because the & operator needs an lvalue
	std::string strVertSrc = File::ReadFile(m_vertexPath);
	std::string strFragSrc = File::ReadFile(m_fragmentPath);
	const char* vertsrc =strVertSrc.c_str();
	const char* fragsrc =strFragSrc.c_str();

	GLCall(glShaderSource(vertex, 1, &vertsrc, NULL));
	GLCall(glShaderSource(fragment, 1, &fragsrc, NULL));

	bool vertexResult = Compile(vertex);
	bool fragmentResult = Compile(fragment);
	if (!vertexResult && !fragmentResult) {
		GLCall(glDeleteShader(vertex));
		GLCall(glDeleteShader(fragment));
		return NULL;
	}

	GLCall(glAttachShader(program, vertex));
	GLCall(glAttachShader(program, fragment));

	GLCall(glLinkProgram(program));
	GLCall(glValidateProgram(program));
		
	//Shaders no longer necessary, program is linked and loaded
	GLCall(glDeleteShader(vertex));
	GLCall(glDeleteShader(fragment));

	return program;
}
bool Shader::Compile(GLuint shader){
	GLCall(glCompileShader(shader));
	int result;
	GLCall(glGetShaderiv(shader, GL_COMPILE_STATUS, &result));
	if (result == GL_FALSE) {
		//Error logging and handling...
		GLint len;
		GLCall(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len));
		std::vector<char> log(len);
		GLCall(glGetShaderInfoLog(shader, len, &len, &log[0]));
		std::cout << &log[0] << std::endl;
		return 0;
	}
	return 1;
}

void Shader::Bind()const{
	GLCall(glUseProgram(m_shaderId));
}

void Shader::Unbind()const{
	GLCall(glUseProgram(0));
}

void Shader::SetUniform1ia(const std::string& name,int count,int* value) {
	GLCall(glUniform1iv(GetUniformLocation(name),count,value))
}

void Shader::SetUniform1i(const std::string& name, int value) {
	GLCall(glUniform1i(GetUniformLocation(name), value));
}

void Shader::SetUniform1f(const std::string& name, float value) {
	GLCall(glUniform1f(GetUniformLocation(name), value));
}
void Shader::SetUniform2f(const std::string& name, const glm::vec2& vec) {
	GLCall(glUniform2f(GetUniformLocation(name), vec.x, vec.y));
}
void Shader::SetUniform3f(const std::string& name, const glm::vec3& vec) {
	GLCall(glUniform3f(GetUniformLocation(name), vec.x, vec.y, vec.z));
}

void Shader::SetUniform4f(const std::string& name, const glm::vec4& vec){
	GLCall(glUniform4f(GetUniformLocation(name), vec.x, vec.y, vec.z, vec.w));
}

void Shader::SetUniformMat4f(const std::string& name, const glm::mat4& matrix) {
	GLCall(glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, &matrix[0][0]));
}

int Shader::GetUniformLocation(const std::string& uniform_name)  {
	auto locationSearch = m_locations.find(uniform_name);
	if (locationSearch != m_locations.end()) 
		return locationSearch->second;

	GLCall(int location = glGetUniformLocation(m_shaderId, uniform_name.c_str()));
	m_locations[uniform_name] = location;
	return location;

}