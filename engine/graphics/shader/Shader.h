#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include "math/glm/glm.hpp"
#include "math/glm/gtc/matrix_transform.hpp"
#include <unordered_map>

	class Shader {
		private:
			std::unordered_map<std::string, int> m_locations;
			GLuint m_shaderId;
			std::string m_vertexPath, m_fragmentPath;
		public:
			inline GLuint GetShaderID() const{ return m_shaderId; }
			Shader(std::string vertexpath, std::string fragmentpath);
			~Shader();
			void Bind()const;
			void Unbind()const;
			void SetUniform1ia(const std::string& uniform_name,int count, int* value);
			void SetUniform1i(const std::string& uniform_name, int value);
			void SetUniform1f(const std::string& uniform_name, float value);
			void SetUniform2f(const std::string& uniform_name, const glm::vec2& vec);
			void SetUniform3f(const std::string& uniform_name, const glm::vec3& vec);
			void SetUniform4f(const std::string& uniform_name,const glm::vec4& vec);
			void SetUniformMat4f(const std::string& uniform_name,const glm::mat4& matrix);
		private:
			int GetUniformLocation(const std::string& uniform_name);
			GLuint Load();
			bool Compile(GLuint shader);
	};