#include "Input.h"

bool Input::keyboardKeys[MAX_KEYBOARD_KEYS];
bool Input::mouseKeys[MAX_MOUSE_KEYS];
glm::vec2 Input::mousePos;
float Input::scrollValue;
GLFWwindow* Input::currentWindow = nullptr;


void Input::SetCurrentWindow(GLFWwindow* window) {
	currentWindow = window;
//	delete current_window;

	glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
		keyboardKeys[key] = action != GLFW_RELEASE ;
	});

	glfwSetMouseButtonCallback(window, [](GLFWwindow* window, int button, int action, int mods) {
		mouseKeys[button] = action != GLFW_RELEASE;
	});

	glfwSetCursorPosCallback(window, [](GLFWwindow* window, double xpos, double ypos) {
		mousePos.x =(float) xpos;
		mousePos.y =(float) ypos;

	});

	glfwSetScrollCallback(window, [](GLFWwindow* window, double xoffset, double yoffset) {
		scrollValue = float(yoffset);
	});
};

void Input::Update() {
	glfwPollEvents();
}

bool Input::IsKeyPressed(int key) {
	return keyboardKeys[key];
};

bool Input::IsMouseButtonPressed(int button) {
	return mouseKeys[button];
};

glm::vec2& Input::GetMousePos() {
	return mousePos;
};

bool Input::IsScrollPressed() {
	if (scrollValue)
		return true;
	return false;
}

float Input::GetScrollValue() {
	float value = scrollValue > 0? 1.05f : 0.95f;
	scrollValue = 0;
	return value;
}

void Input::SetScrollValue(float value) {
	scrollValue = value;
}