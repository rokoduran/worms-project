#pragma once
#include "Window.h"

enum KeyboardKeys{
	KEYBOARD_UP = GLFW_KEY_UP,
	KEYBOARD_LEFT = GLFW_KEY_LEFT,
	KEYBOARD_RIGHT =  GLFW_KEY_RIGHT,
	KEYBOARD_DOWN = GLFW_KEY_DOWN,
	KEYBOARD_ESC = GLFW_KEY_ESCAPE,
	KEYBOARD_ENTER = GLFW_KEY_ENTER,
	KEYBOARD_BACKSPACE = GLFW_KEY_BACKSPACE,
	KEYBOARD_SPACE = GLFW_KEY_SPACE,
	KEYBOARD_CAPS = GLFW_KEY_CAPS_LOCK,
	KEYBOARD_1 = GLFW_KEY_1,
	KEYBOARD_2 = GLFW_KEY_2,
	KEYBOARD_3 = GLFW_KEY_3,
	KEYBOARD_4 = GLFW_KEY_4,
	KEYBOARD_5 = GLFW_KEY_5,
	KEYBOARD_F1 = GLFW_KEY_F1,
	KEYBOARD_F2 = GLFW_KEY_F2,
	KEYBOARD_F3 = GLFW_KEY_F3,
	KEYBOARD_F4 = GLFW_KEY_F4,
	KEYBOARD_F5 = GLFW_KEY_F5
};

class Input {
	friend class Window;
	private:
		static bool keyboardKeys[MAX_KEYBOARD_KEYS];
		static bool mouseKeys[MAX_MOUSE_KEYS];
		static glm::vec2 mousePos;
		static float scrollValue;
		static GLFWwindow* currentWindow;
	public:
		//Set the current window. The first time a window is created this is automatically called.
		static void SetCurrentWindow(GLFWwindow*);
		static void Update();
		static bool IsKeyPressed(int);
		static bool IsMouseButtonPressed(int);
		static bool IsScrollPressed();
		static float GetScrollValue();
		static void SetScrollValue(float);
		static glm::vec2& GetMousePos();
};

