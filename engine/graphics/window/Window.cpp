#include "Window.h"
#ifdef DEBUG
#include "util/Log.h"
#include <sstream>
#endif
#include "util/stb_image/stb_image.h"
#include "Input.h"
#include "graphics/renderer/Render2D.h"

bool Window::GLEWGLFWInitialized = false;

Window* Window::CreateNewWindow(const WindowData& data) {
	return new Window(Window(data));
}

Window::Window(const WindowData& data): m_data(data){
	Init();
}

Window::~Window() {
	if (Input::currentWindow == m_window)
		Input::currentWindow = nullptr;
	glfwDestroyWindow(m_window);
}

void Window::ShutDown() {
	Input::SetCurrentWindow(nullptr);
	glfwTerminate();
}

void Window::Init() {
	if (!GLEWGLFWInitialized) {
#ifdef DEBUG
		if (!glfwInit()) {
			std::cout << "GLFW failed to initiliaze!" << std::endl;
			ASSERT(0);
		}
		glfwSetErrorCallback([](int errorcode, const char* msg) {
			std::stringstream ss;
			ss << "GLFW ERROR: " << errorcode << " " << msg << std::endl;
			std::cout << ss.str();
		});
#else
		glfwInit();
#endif
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	m_window = glfwCreateWindow((int)m_data.width, (int)m_data.height, m_data.windowTitle.c_str(), nullptr, nullptr);
	glfwMakeContextCurrent(m_window);
	if (!GLEWGLFWInitialized) {
		glewExperimental = true;
#ifdef DEBUG
		if (glewInit() != GLEW_OK) {
			std::cout << "GLEW failed to initialize!" << std::endl;
			ASSERT(0);
		}
#else 
		glewInit();
#endif
		GLEWGLFWInitialized = true;
		Input::SetCurrentWindow(m_window);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	Sound::Init();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glfwSetWindowUserPointer(m_window, &m_data);
	SetVSync(m_data.vsync);

	 glfwSetFramebufferSizeCallback(m_window,[](GLFWwindow* window, int width, int height)
	 {
		GLCall(glViewport(0, 0, width, height));
	 });

	glfwSetWindowSizeCallback(m_window, [](GLFWwindow* window, int width, int height) {
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		data.width = width;
		data.height = height;
		data.view.UpdateCoords((float)width,(float) height);
		data.Set_Projection((float)width,(float) height);
		
	});

	glfwSetWindowCloseCallback(m_window, [](GLFWwindow* window) {
		glfwDestroyWindow(window);
		Sound::ShutDown();
	});
}

void Window::SetFullscreen(bool fullscreen) {
	if (fullscreen) {
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		glfwSetWindowMonitor(m_window, glfwGetPrimaryMonitor(), 0, 0, 1600, 900, 60);
	}
	else
		glfwSetWindowMonitor(m_window, NULL, 0,0,640,480,60);
}

void Window::Set_Icon(const std::string& path) {
	GLFWimage* img = new GLFWimage;
	int width=0, height=0,bytes_per_pixel=0;
	img->pixels = stbi_load(path.c_str(), &img->width, &img->height,&bytes_per_pixel, 4);
	glfwSetWindowIcon(m_window, 1, img);
}

void Window::SetVSync(bool enabled) {
	enabled ? glfwSwapInterval(1) : glfwSwapInterval(0);
	m_data.vsync = enabled;
}

bool Window::CheckVSync()const {
	return m_data.vsync;
}

void Window::Clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::OnUpdate() {
	glfwSwapBuffers(m_window);
}