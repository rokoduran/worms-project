#pragma once

#include "util/defines.h"
#include "math/glm/glm.hpp"
#include "./graphics/renderer/Render2D.h"
#include "graphics/camera/View.h"
#include "audio/Sound.h"
#include "GL/glew.h"
#include "GLFW/glfw3.h"
struct WindowData {
	//Title of the window.
	std::string windowTitle;
	//Width and height of the window.
	unsigned int width, height;
	//Camera/view matrix.
	View view;
	//Projection matrix
	glm::mat4 projection;
	//Vsync flag
	bool vsync;

	WindowData(const std::string& title = "Worms", unsigned int width = 1280, unsigned int height = 720, bool vsync = false) : windowTitle(title), width(width), height(height),
		vsync(vsync), projection(glm::ortho(0.0f, (float)width, 0.0f, (float)height, -1.0f, 1.0f)), view((float)width,(float)height) {};
	WindowData(const WindowData& data) : windowTitle(data.windowTitle), width(data.width), height(data.height), vsync(data.vsync),
		projection(glm::ortho(0.0f, (float)data.width, 0.0f, (float)data.height, -1.0f, 1.0f)), view((float)data.width, (float)data.height) {};
	void Set_Projection(const glm::vec2& res) {
		projection = { glm::ortho(0.0f, res.x, 0.0f, res.y, -1.0f, 1.0f) };
	}
	void Set_Projection(float w, float h) {
		projection = { glm::ortho(0.0f, w, 0.0f, h, -1.0f, 1.0f) };
	}
};

class Window {
	private:
		friend class Game;
		static bool GLEWGLFWInitialized;
		GLFWwindow* m_window;
		WindowData m_data;
		Window(const WindowData& data);

	public:
		~Window();
		//Swap the buffers
		void OnUpdate();
		//Clear the buffer bit
		void Clear();
		View& Get_View(){
			return m_data.view;
		}
		unsigned int GetWidth() const { return m_data.width; };
		unsigned int GetHeight() const { return m_data.height; };
		glm::vec2 GetCenter() const { return glm::vec2(m_data.width / 2, m_data.height / 2); }
		void SetVSync(bool enabled);
		void SetFullscreen(bool fullscreen);
		bool CheckVSync()const;
		void DrawString(const Label& label) {
			Render2D::renderer->DrawString(label);
		}
		void DrawQuad(const Rect& rect, const unsigned int color, bool b = true) {
			Render2D::renderer->DrawQuad(rect, color, b);
		};
		void DrawQuad(const Sprite& sprite) {
			Render2D::renderer->DrawQuad(sprite);
		};
		void DrawQuad(Rect& rect, const unsigned int color, const unsigned int texid, bool b = true) {
			Render2D::renderer->DrawQuad(rect, color,texid, b);
		};
		//Set the window icon
		void Set_Icon(const std::string&);
		//Create a new window, if using input, set the current window to this one if necessary.
		static Window* CreateNewWindow(const WindowData& data = WindowData());
		static void ShutDown();
	private:
		void Init();
		void InitRenderer()const {
			Render2D::InitializeRenderer();
		}
		void BeginBatch()const {
			Render2D::renderer->BeginBatch();
			Render2D::renderer->SetProjection(m_data.projection);
			Render2D::renderer->SetView(m_data.view.GetView());
		}
		void EndBatch()const {
			Render2D::renderer->EndBatch();
		}
		void FlushRenderer()const {
			Render2D::renderer->Flush();
		}
};