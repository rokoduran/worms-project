#include "Debug.h"


std::vector <OutlineDebug> Debug::rects = {};

void Debug::Draw(Window& window) {
	for (auto& r : rects)
		r.Draw(window);
	rects.clear();
};

void Debug::Clear() {
	rects.clear();
}

void Debug::DrawRect(const Rect& rect, float thickness, unsigned int colour) {
	colour = SwapHex(colour);
	rects.push_back(OutlineDebug(rect, thickness, colour));
};