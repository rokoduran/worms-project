#pragma once
#include "graphics/window/Window.h"
#include "util/Shapes/Rectangle.h"
#include <vector>
#include <array>
#include <set>

struct OutlineDebug {
	std::array<Rect, 4> arr;
	unsigned int color;
	OutlineDebug(const Rect& rect, float thickness, unsigned int color) : color(color) {
		const glm::vec2& origin = rect.GetOrigin();
		const glm::vec2& size = rect.GetSize();
		arr = { Rect(origin.x, origin.y,size.x,thickness),
				Rect(origin.x, origin.y,thickness,size.y),
				Rect(origin.x, origin.y + size.y,size.x,thickness),
				Rect(origin.x + size.x, origin.y,thickness,size.y)};
	}

	void Draw(Window& window) {
		for (auto& r : arr) {
			window.DrawQuad(r, color);
		}
	}
};

class Debug{
	private:
		//No other way currently to implement sprite outlines, so it's done this way
		static std::vector<OutlineDebug> rects;
	public:
		static void Draw(Window& window);
		static void DrawRect(const Rect& rect, float thickness = 2.0f, unsigned int colour = 0xff0000ff);
		static void Clear();
};