#pragma once
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

class File{
	public:
		static std::string	ReadFile(std::string& path){
				std::ifstream file(path);
				if (file.is_open()) {
					file.seekg(0, file.end);
					int length =(int) file.tellg();
					file.seekg(0, file.beg);
					char* data = new char[length + 1];
					memset(data, 0, length);
					file.read(data, length);
					std::string str(data);
					delete[] data;
					return str;
				}
				return NULL;
		}
};