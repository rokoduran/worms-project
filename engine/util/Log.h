#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <iostream>
// #x fetches the function name in strong form, __ FILE console outs the files name, __LINE__, says at which line
#ifdef DEBUG
#define ASSERT(x) if(!(x)) __debugbreak();
#endif

#ifdef DEBUG
//GLCall is used in debug to check and clear all OpenGL errors that occur, in release mode, it's just a wrapper that does nothing
#define GLCall(x) GLClearError(); x; ASSERT(GLLogCall(#x,__FILE__ , __LINE__))
#else
#define GLCall(x) x;
#endif

//Clears OpenGL's error stream to the latest error
static void GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}
//Print the openglerror
static bool GLLogCall(const char* function, const char* file, int line) {
	std::string str;
	while (GLenum err = glGetError()) {
		switch (err)
		{
		case GL_INVALID_ENUM:
			str = "Invalid Enumerator";
			break;
		case GL_INVALID_VALUE:
			str = "Invalid Value";
			break;
		case GL_INVALID_OPERATION:
			str = "Invalid Operation";
			break;
		case GL_STACK_OVERFLOW:
			str = "Stack Overflow";
			break;
		case GL_STACK_UNDERFLOW:
			str = "Stack Underflow";
			break;
		case GL_OUT_OF_MEMORY:
			str = "Out of Memory";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			str = "Invalid Framebuffer Operation";
			break;
		case GL_CONTEXT_LOST:
			str = "Context Lost";
			break;
		default: 
			str = "File couldn't be found/opened ";
			break;
		}
		std::cout << "[OpenGL Error] " << str << " in function: " << function << " in file: " << file << " at line: "<< line << std::endl;
		return 0;
	}
	return 1;
}