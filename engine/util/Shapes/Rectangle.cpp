#include "Rectangle.h"

Rect::Rect() : m_origin(0.0f,0.0f), m_size(0.0f,0.0f), m_center(0.0f,0.0f){};
Rect::Rect(const glm::vec2& pos, const glm::vec2& size) : m_origin(pos), m_size(size), m_center(pos.x+0.5f*size.x, pos.y+0.5f*size.y) {};
Rect::Rect(float x, float y, float w, float h) : m_origin(x,y), m_size(w,h){
	RecalculateCenter();
};
bool Rect::Contains(const glm::vec2& point)const  {
	return (((point.x >= m_origin.x) && (point.x < (m_origin.x + m_size.x))) && ((point.y >= m_origin.y) && (point.y < (m_origin.y + m_size.y))));
}

glm::vec2 Rect::SolveOverlap(const Rect& other)const{
	glm::vec2 resolution = { 0.0f,0.0f };
	//Racunamo s kojih strana je collision
	const glm::vec2& otherOrigin = other.GetOrigin();
	const glm::vec2& otherSize = other.GetSize();
	const glm::vec2& otherCenter = other.GetCenter();
	float xdiff = m_center.x - otherCenter.x;
	float ydiff = m_center.y - otherCenter.y;
	//if (fabs(xdiff) > fabs(ydiff))
		resolution.x = xdiff > 0 ? (otherOrigin.x + otherSize.x) - m_origin.x : -((m_origin.x + m_size.x) - otherOrigin.x);
	//else
		resolution.y = ydiff > 0 ? (otherOrigin.y + otherSize.y) - m_origin.y : -((m_origin.y + m_size.y) - otherOrigin.y);
	return resolution;
}
/*
A's Left Edge to left of B's right edge, [RectA.Left < RectB.Right], and
A's right edge to right of B's left edge, [RectA.Right > RectB.Left], and
A's top above B's bottom, [RectA.Top > RectB.Bottom], and
A's bottom below B's Top [RectA.Bottom < RectB.Top]
*/
bool Rect::Intersects(const Rect& other)const{
	const glm::vec2& otherOrigin = other.GetOrigin();
	const glm::vec2& otherSize = other.GetSize();
	return (m_origin.x < (otherOrigin.x + otherSize.x)) && ((m_origin.x + m_size.x) > otherOrigin.x) && ((m_origin.y + m_size.y) > otherOrigin.y) && (m_origin.y < (otherOrigin.y + otherSize.y));	
}

void Rect::SetOrigin(float x, float y) {
	m_origin = { x,y };
	RecalculateCenter();
};
void Rect::SetSize(float w, float h) {
	m_size = { w, h };
	RecalculateCenter();
};

void Rect::SetOrigin(const glm::vec2& origin) {
	m_origin = origin;
	RecalculateCenter();
}

void Rect::SetSize(const glm::vec2& size) {
	m_size = size;
	RecalculateCenter();
}

const glm::vec2& Rect::GetOrigin()const {
	return m_origin;
};
const glm::vec2& Rect::GetSize()const {
	return m_size;
};
const glm::vec2& Rect::GetCenter()const {
	return m_center;
};

void Rect::RecalculateCenter() {
	m_center = { m_origin.x + 0.5f*m_size.x, m_origin.y + 0.5f*m_size.y };
}