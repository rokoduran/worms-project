#pragma once
#include "math/glm/glm.hpp"
class Rect{
	private:
		glm::vec2 m_origin,m_size,m_center;
	public:
		Rect();
		Rect(const glm::vec2& origin, const glm::vec2& size);
		Rect(float x, float y, float w, float h);
		void SetOrigin(const glm::vec2&);
		void SetSize(const glm::vec2&);
		void SetOrigin(float x, float y);
		void SetSize(float w, float h);
		const glm::vec2& GetOrigin()const;
		const glm::vec2& GetSize()const;
		const glm::vec2& GetCenter()const;
		bool Contains(const glm::vec2& point)const;

		glm::vec2 SolveOverlap(const Rect& othershape)const;
		bool Intersects(const Rect& othershape)const;
	private:
		void RecalculateCenter();
};

