#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <stdint.h>
#include <sstream>
#include "math/glm/glm.hpp"
#define MAX_KEYBOARD_KEYS  1024
#define MAX_MOUSE_KEYS     32
#define toRad(degrees) degrees * M_PI/180.0f
#define MAX_IMAGE_WIDTH = 12000
#define MAX_IMAGE_HEIGHT = 6000;
#define ShiftBit(x) 1 << x
static const float physics_dt = 1 / 60;
static const float gravityscale = 5.0f;
static const glm::vec2 gravity = { 0,10.0f*gravityscale };
static const float EPSILON = 0.0001f;

//Swap recieved hex values to match the wanted color
static void SwapHexByRef(unsigned int& color) {
	color = ((color << 24) & 0xFF000000) |
		((color << 8) & 0x00FF0000) |
		((color >> 8) & 0x0000FF00) |
		((color >> 24) & 0x000000FF);
};
//Swap recieved hex values to match the wanted color
static unsigned int SwapHex(unsigned int color) {
	color = ((color << 24) & 0xFF000000) |
		((color << 8) & 0x00FF0000) |
		((color >> 8) & 0x0000FF00) |
		((color >> 24) & 0x000000FF);
	return color;
};

template <typename T>
std::string NumberToString(T Number)
{
	std::ostringstream ss;
	ss << Number;
	return ss.str();
}



class Bitmask{
	private:
		uint32_t m_bits;
	public:
		Bitmask() : m_bits(0) {};

		void Set_Mask(Bitmask& other) {
			m_bits = other.Get_Mask();
		};
		uint32_t Get_Mask() const {
			return m_bits;
		};
		//Get bit at position
		bool Get_Bit(int pos) const {
			return (m_bits & (1 << pos)) != 0;
		};

		void Set_Bit(int pos, bool on) {
			on ? Set_Bit(pos) : Clear_Bit(pos);
		};
		//Set that bit to one
		void Set_Bit(int pos) {
			m_bits = m_bits | 1 << pos;
		};
		//Clear that bit location;
		void Clear_Bit(int pos) {
			m_bits = m_bits & ~(1 << pos);
		};
		//Clear all bits
		void Clear() {
			m_bits = 0;
		};

};
